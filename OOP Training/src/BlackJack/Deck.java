package BlackJack;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Deck {

    Map<String, Integer> values = Stream.of(
            new AbstractMap.SimpleEntry<>("Ace", 11),
            new AbstractMap.SimpleEntry<>("Two", 2),
            new AbstractMap.SimpleEntry<>("Three", 3),
            new AbstractMap.SimpleEntry<>("Four", 4),
            new AbstractMap.SimpleEntry<>("Five", 5),
            new AbstractMap.SimpleEntry<>("Six", 6),
            new AbstractMap.SimpleEntry<>("Seven", 7),
            new AbstractMap.SimpleEntry<>("Eight", 8),
            new AbstractMap.SimpleEntry<>("Nine", 9),
            new AbstractMap.SimpleEntry<>("Ten", 10),
            new AbstractMap.SimpleEntry<>("Jack", 10),
            new AbstractMap.SimpleEntry<>("Queen", 10),
            new AbstractMap.SimpleEntry<>("King", 10))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    private ArrayList<Card> deck = new ArrayList<>();
    private ArrayList<Card> cardGraveyard = new ArrayList<>();
    private String[] suits = {"Diamonds", "Clubs", "Hearts", "Spades"};
    private String[] faces = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};


    public Deck() {
        fill();
        shuffle();
    }

    public Deck(int numberOfDecks) {
        for (int i = 0; i < numberOfDecks; i++) {
            fill();
        }
        shuffle();
    }

    public int getCardAmmount() {
        return this.deck.size();
    }

    private void fill() {
        for (String suit : suits) {
            for (String face : faces) {
                Card card = new Card(suit, face, values.get(face));
                deck.add(card);
            }
        }
    }

    private void replaceCards() {
        for (Card card : this.cardGraveyard) {
            this.deck.add(card);
        }
        this.cardGraveyard.clear();
    }

    public void reshuffle() {
        replaceCards();
        shuffle();
    }

    public Card removeCard() {
        if (getCardAmmount() != 0) {
            Card card = this.deck.get(0);
            this.cardGraveyard.add(card);
            this.deck.remove(0);
            return card;
        }
        return null;
    }

    private void shuffle() {
        Collections.shuffle(this.deck);
    }


}
