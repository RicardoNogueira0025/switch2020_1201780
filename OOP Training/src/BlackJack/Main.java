package BlackJack;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Blackjack Ratado");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel betAmmount = new JLabel("Bet Amount: 0", JLabel.CENTER);
        betAmmount.setSize(50, 50);
        JLabel playerCredit = new JLabel("Credit: 50");
        playerCredit.setLayout(null);
        playerCredit.setLocation(50, 50);
        playerCredit.setSize(100, 50);
        frame.add(playerCredit);

        Game blackjack = new Game();

        JButton betOk = new JButton("Accept Bet");
        betOk.setSize(150, 50);
        betOk.setLayout(null);
        betOk.setLocation(165, 400);
        JButton betPlus = new JButton("Increase Bet");
        betPlus.setSize(150, 50);
        betPlus.setLayout(null);
        betPlus.setLocation(330, 400);
        JButton betMinus = new JButton("Decrease Bet");
        betMinus.setSize(150, 50);
        betMinus.setLayout(null);
        betMinus.setLocation(5, 400);


        betMinus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (blackjack.getTempBetAmmount() != 0) blackjack.addCredit(5);
                blackjack.decreaseBet();
                betAmmount.setText("Bet Amount: " + blackjack.getTempBetAmmount());
                playerCredit.setText("Credit: " + blackjack.getCredit());

            }
        });
        betPlus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                blackjack.increaseBet();
                betAmmount.setText("Bet Amount: " + blackjack.getTempBetAmmount());
                playerCredit.setText("Credit: " + blackjack.getCredit());
            }
        });
        betOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (blackjack.getTempBetAmmount() != 0) {
                    blackjack.setBet();
                    blackjack.startGame();
                    betPlus.setEnabled(false);
                    betMinus.setEnabled(false);
                    betOk.setEnabled(false);
                }

            }
        });

        // adicionar todos os botoes e so depois ativar
        frame.add(betOk);
        frame.add(betPlus);
        frame.add(betMinus);
        frame.add(betAmmount);

        frame.setSize(500, 500);
        frame.setVisible(true);

        while (!betOk.isEnabled()) {
            //
        }


    }
}
