package BlackJack;

public class Game {

    private final int DEALER = 0;
    private final int PLAYER = 1;
    private final Deck deck = new Deck();
    private final Player player1 = new Player();
    private final Dealer dealer = new Dealer();
    private final int[] score = new int[2];
    private int betAmmount = 0;
    private int tempBetAmmount = 0;

    public Game() {

    }


    public void startGame() {
        dealInitialCards();
    }

    public int getTempBetAmmount() {
        return this.tempBetAmmount;
    }

    public void setBet() {
        betAmmount = tempBetAmmount;
        tempBetAmmount = 0;
    }

    public int getCredit() {
        return player1.getCredit();
    }

    public void addCredit(int credit) {
        player1.addCredit(credit);
    }

    public void decreaseBet() {
        if (tempBetAmmount != 0) tempBetAmmount -= 5;
    }

    public void increaseBet() {
        if (player1.getCredit() >= 5) {
            tempBetAmmount += 5;
            player1.bet(5);
        }
    }


    private void dealerTurn() {
        while (score[DEALER] < 17) {
            dealer.addCard(deck.removeCard());
            updateScore();
        }
        if (score[DEALER] == 21) bust();
        if (score[DEALER] > 21) blackjack();
    }

    private void checkScoreAfterHit() {
        if (score[PLAYER] > 21) bust();
        if (score[PLAYER] == 21) blackjack();
    }

    private void blackjack() {
        player1.addCredit(betAmmount * 2);
        betAmmount = 0;
        restartGame();
    }

    private void bust() {
        restartGame();
    }

    private void restartGame() {
        player1.emptyHand();
        dealer.emptyHand();
        deck.reshuffle();
    }

    public void hit() {
        player1.addCard(deck.removeCard());
        updateScore();
        checkScoreAfterHit();
    }

    public int showScore(int person) {
        return score[person];
    }

    private void updateScore() {
        score[DEALER] = dealer.getTotalValue();
        score[PLAYER] = player1.getTotalValue();
    }

    private void dealInitialCards() {
        dealer.addCard(deck.removeCard());
        dealer.addCard(deck.removeCard());
        player1.addCard(deck.removeCard());
        player1.addCard(deck.removeCard());
    }
}
