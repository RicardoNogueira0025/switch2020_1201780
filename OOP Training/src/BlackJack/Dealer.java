package BlackJack;

import java.util.ArrayList;

public class Dealer {

    private ArrayList<Card> dealerHand = new ArrayList<>();


    public void addCard(Card card) {
        this.dealerHand.add(card);
    }

    public void emptyHand() {
        this.dealerHand.clear();
    }

    public int getValueOfHiddenCard() {
        return this.dealerHand.get(1).getValue();
    }

    public int getTotalValue() {
        int value = 0;
        for (Card card : this.dealerHand) {
            value += card.getValue();
        }
        return value;
    }


}
