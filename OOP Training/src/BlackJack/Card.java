package BlackJack;

public class Card {

    private String suit;
    private String face;
    private int value;

    public Card(String suit, String face, int value) {
        this.suit = suit;
        this.face = face;
        this.value = value;

    }

    public void displayCard() {
        System.out.println(this.face + " of " + this.suit);
    }

    public String getSuit() {
        return suit;
    }

    public String getFace() {
        return face;
    }

    public int getValue() {
        return value;
    }
}
