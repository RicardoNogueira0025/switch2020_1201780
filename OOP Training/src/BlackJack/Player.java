package BlackJack;

import java.util.ArrayList;

public class Player {
    private int credit;
    private ArrayList<Card> playerHand = new ArrayList<>();

    public Player() {
        this.credit = 50;
    }

    public void addCredit(double value) {
        this.credit += value;
    }

    public void addCard(Card card) {
        this.playerHand.add(card);
    }

    public int getTotalValue() {
        int value = 0;
        for (Card card : this.playerHand) {
            value += card.getValue();
        }
        return value;
    }

    public void emptyHand() {
        this.playerHand.clear();
    }

    public void bet(double value) {
        if (value > this.credit) {
            //Needs a message, not an error throw
            throw new IllegalStateException("Not enough money to bet");
        } else {
            this.credit -= value;
        }
    }

    public int getCredit() {
        return this.credit;
    }
}
