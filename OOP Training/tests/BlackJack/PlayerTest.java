package BlackJack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PlayerTest {

    @Test
    void addCredit() {
        Player player = new Player();
        int credit = 5;
        player.addCredit(credit);
        assertEquals(55, player.getCredit());
    }

    @Test
    void addCard() {
        Card ace = new Card("Diamonds", "Ace", 11);
        Player player = new Player();
        player.addCard(ace);
        assertEquals(11, player.getTotalValue());
    }

    @Test
    void getTotalValue() {
        Card ace = new Card("Diamonds", "Ace", 11);
        Card jack = new Card("Clubs", "Jack", 10);
        Player player = new Player();
        player.addCard(ace);
        player.addCard(jack);
        assertEquals(21, player.getTotalValue());
    }

    @Test
    void subtractCredit() {
        Player player = new Player();
        player.bet(5);
        assertEquals(45, player.getCredit());
    }
}