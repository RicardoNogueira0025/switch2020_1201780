package BlackJack;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class DeckTest {

    @Test
    public void checkNumberOfCards() {
        Deck testDeck = new Deck();
        int expected = 52;
        assertEquals(expected, testDeck.getCardAmmount());
    }

    //This test may fail in 3 out of 52 times, randomly
    @Test
    public void testRandom() {
        Deck testDeck = new Deck();
        Card firstCard = testDeck.removeCard();
        int expectedValue = 11;
        assertNotEquals(expectedValue, firstCard.getValue());

    }


}