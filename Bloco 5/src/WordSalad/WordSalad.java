package WordSalad;

public class WordSalad {

    private char[][] gameMatrix;
    private WordList solutions = new WordList();

    public WordSalad(char[][] wordSalad, WordList solutions) {
        this.gameMatrix = wordSalad;
        this.solutions = solutions;
    }

    /**
     * Method to check the direction to parse the word
     *
     * @param coord1 The coordinate of the first letter
     * @param coord2 The coordinate of the last letter
     * @return Integer representing the direction:
     * 1. Esquerda -> Direita
     * 2. Direita -> Esquerda
     * 3. Cima -> Baixo
     * 4. Baixo -> Cima
     * 5. Diagonal -> Descendente Direita
     * 6. Diagonal -> Ascendente Direita
     * 7. Diagonal Descendente Esquerda
     * 8. Diagonal Ascendente Esquerda
     */
    private int checkDirection(int[] coord1, int[] coord2) {
        /*
        1. Esquerda -> Direita
        2. Direita -> Esquerda
        3. Cima -> Baixo
        4. Baixo -> Cima
        5. Diagonal -> Descendente Direita
        6. Diagonal -> Ascendente Direita
        7. Diagonal Descendente Esquerda
        8. Diagonal Ascendente Esquerda
        */
        final int LINE = 0;
        final int COLUMN = 1;
        if (coord1[LINE] == coord2[LINE]) {
            //mesma fila
            if (coord1[COLUMN] < coord2[COLUMN]) {
                //Esquerda->Direita
                return 1;
            } else {
                //Direita->Esquerda
                return 2;
            }
        } else if (coord1[LINE] < coord2[LINE]) {
            //descendente
            if (coord1[COLUMN] == coord2[COLUMN]) {
                //mesma coluna, Cima->Baixo
                return 3;
            } else if (coord1[COLUMN] < coord2[COLUMN]) {
                //colunas diferentes, Diagonal -> Descendente Direita
                return 5;
            } else {
                //Diagonal -> Descendente Esquerda
                return 7;
            }
        } else {
            //ascendente
            if (coord1[COLUMN] == coord2[COLUMN]) {
                //mesma coluna, Baixo->Cima
                return 4;
            } else if (coord1[COLUMN] < coord2[COLUMN]) {
                //colunas diferentes, Diagonal -> Ascendente Direita
                return 6;
            } else {
                //Diagonal -> Ascendente Esquerda
                return 8;
            }
        }
    }

    /**
     * Creates a string from the characters present on the path from the first to the last coordinate
     *
     * @param coord1 The coordinate of the first letter
     * @param coord2 The coordinate of the last letter
     * @return String with the word formed from all the letters between the first and last coordinates
     */
    private String parseWord(int[] coord1, int[] coord2) {
        final int LINE = 0;
        final int COLUMN = 1;
        String chosenWord = "";
        switch (checkDirection(coord1, coord2)) {
            case 1:
                for (int i = coord1[COLUMN]; i <= coord2[COLUMN]; i++) {
                    chosenWord += this.gameMatrix[coord1[LINE]][i];
                }
                break;
            case 2:
                for (int i = coord1[COLUMN]; i >= coord2[COLUMN]; i--) {
                    chosenWord += this.gameMatrix[coord1[LINE]][i];
                }
                break;
            case 3:
                for (int i = coord1[LINE]; i <= coord2[LINE]; i++) {
                    chosenWord += this.gameMatrix[i][coord1[COLUMN]];
                }
                break;
            case 4:
                for (int i = coord1[LINE]; i >= coord2[LINE]; i--) {
                    chosenWord += this.gameMatrix[i][coord1[COLUMN]];
                }
                break;
            case 5:
                for (int i = coord1[LINE], j = coord1[COLUMN]; i <= coord2[LINE]; i++, j++) {
                    chosenWord += this.gameMatrix[i][j];
                }
                break;
            case 6:
                for (int i = coord1[LINE], j = coord1[COLUMN]; i >= coord2[LINE]; i--, j++) {
                    chosenWord += this.gameMatrix[i][j];
                }
                break;
            case 7:
                for (int i = coord1[LINE], j = coord1[COLUMN]; i <= coord2[LINE]; i++, j--) {
                    chosenWord += this.gameMatrix[i][j];
                }
                break;
            case 8:
                for (int i = coord1[LINE], j = coord1[COLUMN]; i >= coord2[LINE]; i--, j--) {
                    chosenWord += this.gameMatrix[i][j];
                }
                break;
        }
        return chosenWord;
    }

    /**
     * Removes the word formed from the chars between the first and last coordinates and removes it from the list if present
     *
     * @param coord1 The coordinate of the first letter
     * @param coord2 The coordinate of the last letter
     */
    public void chooseWord(int[] coord1, int[] coord2) {
        solutions.removeWordIfPresent(parseWord(coord1, coord2));
        solutions.checkIfComplete();
    }
}
