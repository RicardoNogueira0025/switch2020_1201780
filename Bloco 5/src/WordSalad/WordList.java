package WordSalad;

import java.util.ArrayList;

public class WordList {
    private String[] wordArray;

    public WordList(String[] wordArray) {
        this.wordArray = wordArray;
    }

    public WordList() {
    }

    public String[] getWordArray() {
        return wordArray;
    }

    /**
     * Removes a passed word if it is present in the list.
     *
     * @param chosenWord
     */
    public void removeWordIfPresent(String chosenWord) {
        if (isWordPresent(chosenWord)) {
            removeWord(chosenWord);
        }

    }

    /**
     * Checks if the array is empty, signifying the completion of the game
     *
     * @return True if the array is empty.
     */
    public boolean checkIfComplete() {
        return this.wordArray.length == 0;
    }

    /**
     * Removes the passed word from the array
     *
     * @param chosenWord
     */
    private void removeWord(String chosenWord) {
        int counter = 0;
        ArrayList<String> newList = new ArrayList<>();
        for (String word : this.wordArray) {
            if (!chosenWord.equalsIgnoreCase(word)) {
                newList.add(word);
                counter++;
            }
        }
        this.wordArray = newList.toArray(new String[counter]);
    }

    /**
     * Checks if the passes word is present in the array
     *
     * @param chosenWord
     * @return True if the word is present
     */
    private boolean isWordPresent(String chosenWord) {
        boolean wordExists = false;
        for (String word : this.wordArray) {
            if (!chosenWord.equalsIgnoreCase(word)) continue;
            wordExists = true;
        }
        return wordExists;
    }
}
