package Sudoku;

public class SudokuCell {
    private final boolean fixedNumber;
    private int value;

    public SudokuCell(int value, boolean fixedNumber) {
        this.value = value;
        this.fixedNumber = fixedNumber;
    }

    /**
     * Checks if the value is fixed (non-editable)
     *
     * @return True if value is fixed
     */
    public boolean isFixedNumber() {
        return fixedNumber;
    }

    /**
     * Changes the value of the object
     *
     * @param number
     */
    public void changeValue(int number) {
        checkIfNumberValid(number);
        this.value = number;
    }

    /**
     * Returns the value of the object
     *
     * @return Integer representing the value
     */
    public int getValue() {
        return this.value;
    }

    /**
     * Method that thows an exception if the number is negative or larger than 9
     *
     * @param number
     */
    private void checkIfNumberValid(int number) {
        if (number < 0) {
            throw new IllegalArgumentException("O número não pode ser negativo.");
        }
        if (number > 9) {
            throw new IllegalArgumentException("O número não pode ser maior que 9!");
        }

    }

}
