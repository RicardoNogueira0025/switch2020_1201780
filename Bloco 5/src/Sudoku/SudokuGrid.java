package Sudoku;

public class SudokuGrid {
    final private SudokuCell[][] gameGrid = new SudokuCell[9][9];

    public SudokuGrid(int[][] gameMap) {
        fillGameGrid(gameMap);
    }

    /**
     * Method that returns the value of the cell present in the passed coordinates
     *
     * @param lineIndex
     * @param columnIndex
     * @return Integer representing the value of the cell
     */
    public int getValue(int lineIndex, int columnIndex) {
        return this.gameGrid[lineIndex][columnIndex].getValue();
    }

    /**
     * The main game method, where the passed coordinates are verified to see if they contain an editable cell, the number is added, and the game completion is verified.
     *
     * @param lineIndex
     * @param columnIndex
     * @param number
     */
    public void selectCellAndAddNumber(int lineIndex, int columnIndex, int number) {
        if (!checkIfNumberFixed(lineIndex, columnIndex)) {
            addNumber(number, lineIndex, columnIndex);
            checkIfGameComplete();
        }
    }

    /**
     * Method that adds the number to the passes coordinates if it is valid
     *
     * @param number
     * @param lineIndex
     * @param columnIndex
     */
    private void addNumber(int number, int lineIndex, int columnIndex) {
        this.gameGrid[lineIndex][columnIndex].changeValue(number);
    }

    /**
     * Method that returns true if the game is completed correctly
     *
     * @return True is the game solution is valid
     */
    public boolean checkIfGameComplete() {
        return checkIfLinesValid() && checkIfColumnsValid() && checkIfSectorsValid();
    }

    /**
     * Method to calulate the product of the valus on an array of SudokuCell objects
     *
     * @param cellArray
     * @return Integer representing the product of the cell values.
     */
    private int productOfArray(SudokuCell[] cellArray) {
        int product = 1;
        for (SudokuCell cell : cellArray) {
            product *= cell.getValue();
        }
        return product;
    }

    /**
     * Method to check if the product of all columns equals 9!
     *
     * @return True if all columns are comprised of the numbers 1 through 9
     */
    private boolean checkIfColumnsValid() {
        final int NINE_FACTORIAL = 362880;
        for (int lineIndex = 0; lineIndex < 9; lineIndex++) {
            SudokuCell[] column = new SudokuCell[9];
            for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
                column[columnIndex] = this.gameGrid[columnIndex][lineIndex];
            }
            if (productOfArray(column) != NINE_FACTORIAL) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method to check if the product of all lines equals 9!
     *
     * @return True if all lines are comprised of the numbers 1 through 9
     */
    private boolean checkIfLinesValid() {
        final int NINE_FACTORIAL = 362880;
        for (SudokuCell[] line : this.gameGrid) if (productOfArray(line) != NINE_FACTORIAL) return false;
        return true;
    }

    /**
     * Method to gather all the values of the cells on the matrix diagonal.
     *
     * @return Integer Array of the diagonal values.
     */
    private int[] getArrayOfDiagonalValues() {
        int[] diagonal = new int[9];
        for (int index = 0; index < 9; index++) {
            diagonal[index] = this.gameGrid[index][index].getValue();
        }
        return diagonal;
    }

    /**
     * Method to check if an array is comprised of the same number.
     *
     * @param array
     * @return True if the array has the same number throughout.
     */
    private boolean checkIfArrayHasTheSameNumber(int[] array) {
        int numberToCheck = array[0];
        for (int number : array) {
            if (number != numberToCheck) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method to check if the sectors are correct. After line and column verification is passed, the only way for the 3x3 sectors to be valid is if they don't have a single-number diagonal
     *
     * @return True if the diagonal has different digits.
     */
    private boolean checkIfSectorsValid() {
        int[] diagonal = getArrayOfDiagonalValues();
        return !checkIfArrayHasTheSameNumber(diagonal); //se a diagonal for sempre o mesmo numero, é o unico caso que pode haver de 9! em linhas e colunas mas com setores inválidos
    }

/*
    private int[] getSectorIndex(int lineIndex, int columnIndex) {
        int[] sectorIndex = new int[2];
        sectorIndex[0]=lineIndex/3;
        sectorIndex[1]=columnIndex/3;
        return sectorIndex;
    }
*/

    /**
     * Method to check if the selected coordinates contain a cell with a fixed (non-editable) number.
     *
     * @param lineIndex
     * @param columnIndex
     * @return True if the number is fixed.
     */
    private boolean checkIfNumberFixed(int lineIndex, int columnIndex) {
        return this.gameGrid[lineIndex][columnIndex].isFixedNumber();
    }


    /**
     * Method that creates a Map Matrix, detailing the fixed number positions as 1s.
     *
     * @param gameMap
     * @return Integer Matrix of 0 (editable cells) and 1 (fixed cells)
     */
    private int[][] createMapMatrix(int[][] gameMap) {
        int[][] fixedNumberMatrix = new int[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (gameMap[i][j] != 0) {
                    fixedNumberMatrix[i][j] = 1;
                }
            }
        }
        return fixedNumberMatrix;
    }

    /**
     * Method to create the Sudoku Cell objects and place them in the SudokuGrid. The MapMatrix will determine if the object is editable, and the initial gamegrid will provide the values
     *
     * @param gameMap
     */
    private void fillGameGrid(int[][] gameMap) {
        int[][] mapMatrix = createMapMatrix(gameMap);
        for (int lineIndex = 0; lineIndex < 9; lineIndex++) {
            for (int columnIndex = 0; columnIndex < 9; columnIndex++) {
                boolean fixedNumber = mapMatrix[lineIndex][columnIndex] == 1;
                //int sector = getSectorNumber(lineIndex, columnIndex);
                this.gameGrid[lineIndex][columnIndex] = new SudokuCell(gameMap[lineIndex][columnIndex], fixedNumber);
            }
        }
    }
}
