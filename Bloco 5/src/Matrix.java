import Previous_Exercises.Exercicio_Dez;
import Previous_Exercises.Exercicio_Quinze;
import Previous_Exercises.Exercicio_Tres;

public class Matrix extends Vector {

    private int[][] biVector = new int[0][0];

    public Matrix() {
    }

    public Matrix(int[][] biVector) {
        this.biVector = biVector;
    }

    public void addValue(int value, int line) {
        addNewLine(line);
        addNewValueToFirstSlotInLine(value, line);

    }

    private void addNewValueToFirstSlotInLine(int value, int line) {

        int i = 0;
        int[] tempArray = new int[this.biVector[line].length + 1];
        for (int element : this.biVector[line]) {
            tempArray[i] = element;
            i++;
        }
        this.biVector[line] = tempArray;
        this.biVector[line][this.biVector[line].length - 1] = value;

    }

    private void addNewLine(int newLine) {
        if (this.biVector.length == 0) {
            this.biVector = new int[newLine + 1][0];
        } else if (this.biVector.length < newLine + 1) {
            int[][] newBiVector = new int[newLine + 1][0];
            for (int i = 0; i < this.biVector.length; i++) {
                System.arraycopy(this.biVector[i], 0, newBiVector[i], 0, this.biVector[i].length);
            }
            this.biVector = newBiVector;
        }

    }

    public int[][] toMatrix() {
        int[][] copy = this.biVector.clone();
        return copy;
    }

    public void removeFirst(int number) {
        checkEmpty();
        boolean flag = false;
        for (int lineIndex = 0; lineIndex < this.biVector.length && !flag; lineIndex++) {
            for (int columnIndex = 0; columnIndex < this.biVector[lineIndex].length && !flag; columnIndex++) {
                if (this.biVector[lineIndex][columnIndex] == number) {
                    elementRemoval(number, lineIndex);
                    flag = true;
                }
            }
        }
    }

    private void elementRemoval(int number, int index) {
        int newArrayIndex = 0;
        int[] tempArray = new int[this.biVector[index].length - 1];
        boolean flag = false;
        for (int j : this.biVector[index]) {
            if (j == number && !flag) {
                flag = true;
            } else {
                tempArray[newArrayIndex] = j;
                newArrayIndex++;
            }
        }
        this.biVector[index] = tempArray;
    }

    public boolean isEmpty() {
        return this.biVector.length == 0;
    }

    public boolean isNull() {
        return this.biVector == null;
    }

    private void addVector(int[] array) {
        if (isNull()) {
            int[][] storage = new int[1][array.length];
            System.arraycopy(array, 0, storage[0], 0, array.length);
            this.biVector = storage;
        } else {
            int[][] storage = new int[this.biVector.length + 1][];
            for (int i = 0; i < this.biVector.length; i++) {
                storage[i] = new int[this.biVector[i].length];
                System.arraycopy(this.biVector[i], 0, storage[i], 0, this.biVector[i].length);
            }
            storage[this.biVector.length] = new int[array.length];
            System.arraycopy(array, 0, storage[this.biVector.length], 0, array.length);
            this.biVector = storage;
        }
    }

    private void checkEmpty() {
        if (isEmpty()) throw new IllegalStateException("Matrix is empty");
    }

    public int getBiggestValue() {
        checkEmpty();
        return (int) Exercicio_Quinze.elementoMaiorValorMatriz(this.biVector);
    }

    public int getSmallestValue() {
        checkEmpty();
        return Exercicio_Quinze.elementoMenorValorMatriz(this.biVector);
    }

    public float getAverageValue() {
        checkEmpty();
        return Exercicio_Quinze.mediaElementosMatriz(this.biVector);
    }

    public int[] getArrayOfLineSums() {
        checkEmpty();
        int[] sumArray = new int[this.biVector.length];
        int index = 0;
        for (int[] line : this.biVector) {
            sumArray[index] = Exercicio_Tres.somaDeElementos(line);
            index++;
        }
        return sumArray;
    }

    private int calculateBiggestColumnLength() {
        int biggestColumn = 0;
        for (int[] line : this.biVector) {
            if (biggestColumn < line.length) {
                biggestColumn = line.length;
            }
        }
        return biggestColumn;
    }

    public Vector getVectorOfColumnSums() {
        checkEmpty();
        int[] sumArray = new int[calculateBiggestColumnLength()];
        for (int[] line : this.biVector) {
            for (int columnIndex = 0; columnIndex < line.length; columnIndex++) {
                sumArray[columnIndex] += line[columnIndex];
            }
        }

        return new Vector(sumArray);

    }

    public Number getLargestLineIndex() {
        checkEmpty();
        int[] arrayofLineSums = getArrayOfLineSums();
        for (int i = 0; i < arrayofLineSums.length; i++) {
            if (arrayofLineSums[i] == Exercicio_Dez.maiorValorDeArray(arrayofLineSums)) {
                return i;
            }
        }
        return null;
    }

    public boolean isSquare() {
        if (this.biVector.length == 0) return false;
        for (int[] line : this.biVector) {
            if (line.length != this.biVector.length) {
                return false;
            }
        }
        return true;
    }

    public boolean isSymmetricSquare() {
        checkEmpty();
        if (!isSquare()) return false;
        for (int lineIndex = 0; lineIndex < this.biVector.length; lineIndex++) {
            for (int columnIndex = 0; columnIndex < this.biVector.length; columnIndex++) {
                if (this.biVector[lineIndex][columnIndex] != this.biVector[columnIndex][lineIndex]) {
                    return false;
                }
            }
        }
        return true;
    }

    public int getAmountOfNonNullValuesInDiagonal() {
        checkEmpty();
        if (!isSquare()) return -1;
        int count = 0;
        int[] diagonal = Exercicio_Quinze.diagonalPrincipalMatriz(this.biVector);
        assert diagonal != null;
        for (int value : diagonal) {
            if (value != 0) count++;
        }
        return count;
    }

    public boolean checkIfDiagonalsAreTheSame() {
        checkEmpty();
        int[] mainDiagonal = Exercicio_Quinze.diagonalPrincipalMatriz(this.biVector);
        int[] secondaryDiagonal = Exercicio_Quinze.diagonalSecundariaMatriz(this.biVector);
        assert secondaryDiagonal != null;
        assert mainDiagonal != null;
        if (mainDiagonal.length != secondaryDiagonal.length) return false;
        for (int index = 0; index < mainDiagonal.length; index++) {
            if (mainDiagonal[index] != secondaryDiagonal[index]) return false;
        }
        return true;
    }

    private double getNumberOfElements() {
        double numberOfElements = 0;
        for (int[] line : this.biVector) {
            for (int ignored : line) {
                numberOfElements++;
            }
        }
        return numberOfElements;
    }

    private double digitAmmount(int number) {
        String numString = Integer.toString(number);
        if (numString.charAt(0) == '-') {
            return numString.length() - 1;
        } else {
            return numString.length();
        }

    }

    private double averageOfDigitAmmount() {
        double sumOfAmmounts = 0;
        for (int[] line : this.biVector) {
            for (int element : line) {
                sumOfAmmounts += digitAmmount(element);
            }
        }
        return sumOfAmmounts / getNumberOfElements();
    }

    public Vector getAllElementsOfMatrixWhoseNumberOfDigitsIsHigherThanAverage() {
        checkEmpty();
        Vector storage = new Vector();
        for (int[] line : this.biVector) {
            for (int element : line) {
                if (digitAmmount(element) > averageOfDigitAmmount()) storage.addValue(element);
            }
        }
        return storage;

    }

    private Vector allElementsToVector() {
        checkEmpty();
        Vector storage = new Vector();
        for (int[] line : this.biVector) {
            for (int element : line) {
                storage.addValue(element);
            }
        }
        return storage;
    }

    public Vector getallElementsWhosePercentageOfEvenNumbersIsHigherThanTheAverage() {
        return allElementsToVector().getElementsWhosePercentageOfEvenDigitsIsHigherThanTheAverage();
    }

    private int[] invertArray(int[] array) {
        int[] newArray = new int[array.length];
        for (int index = 0; index < array.length; index++) {
            newArray[array.length - 1 - index] = array[index];
        }
        return newArray;
    }

    public void invertLines() {
        checkEmpty();
        for (int index = 0; index < this.biVector.length; index++) {
            this.biVector[index] = invertArray(this.biVector[index]);
        }
    }

    public void invertColumns() {
        checkEmpty();
        Matrix storage = new Matrix();
        for (int i = this.biVector.length - 1; i >= 0; i--) storage.addVector(this.biVector[i]);
        this.biVector = storage.toMatrix();
    }

    private void switchColumnsForLines() {
        int[][] storage = new int[this.biVector.length][this.biVector.length];
        for (int lineIndex = 0; lineIndex < this.biVector.length; lineIndex++) {
            for (int columnIndex = 0; columnIndex < this.biVector[lineIndex].length; columnIndex++) {
                storage[lineIndex][columnIndex] = this.biVector[columnIndex][lineIndex];
            }
        }
        this.biVector = storage;
    }

    public void turn90() {
        checkEmpty();
        switchColumnsForLines();
        invertLines();
    }

    public void turn180() {
        checkEmpty();
        invertColumns();
        invertLines();
    }
    public void turnMinus90(){
        checkEmpty();
        switchColumnsForLines();
        invertColumns();
    }



}

