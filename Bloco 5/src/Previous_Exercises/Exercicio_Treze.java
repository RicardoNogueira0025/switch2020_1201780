package Previous_Exercises;

public class Exercicio_Treze {
    /**
     * Method to check if a given matrix is a square (same number of rows and columns)
     *
     * @param matriz Matrix of Float type to check.
     * @return boolean True if the matrix is a square.
     */
    public static boolean matrizEQuadrada(int[][] matriz) {
        int linhas = matriz.length;
        for (int[] floats : matriz) {
            if (floats.length != linhas) {
                return false;
            }
        }
        return true;
    }
}
