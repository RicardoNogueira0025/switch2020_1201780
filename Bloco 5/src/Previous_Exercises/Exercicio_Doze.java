package Previous_Exercises;

public class Exercicio_Doze {
    /**
     * Method to check if the passed matrix has a constant number of columns. Returns the number of colums, or -1 if not a constant value.
     *
     * @param matriz 2-dimensional Array of the type Float.
     * @return int representing the constant number of column on the matrix, or -1 if not-constant.
     */
    public static int mesmoNumeroColunas(int[][] matriz) {
        int numeroColunas = 0;
        for (int i = 1; i < matriz.length; i++) {
            numeroColunas = matriz[i].length;
            if (numeroColunas != matriz[i - 1].length) {
                return -1;
            }
        }
        return numeroColunas;
    }
}
