package Previous_Exercises;

public class Exercicio_Nove {
    /**
     * Method to check if a given number is a palindrome.
     *
     * @param numero Integer to check.
     * @return boolean True if the number is a palindrome.
     */
    public static boolean verificarCapicua(int numero) {
        int[] digitos = Exercicio_Dois.arrayDeDigitos(numero);
        int indiceArrayReversa = 1;
        boolean resultado = false;
        for (int digito : digitos) {
            if (digito == digitos[digitos.length - indiceArrayReversa]) {
                resultado = true;
                indiceArrayReversa++;
            } else {
                return false;
            }
        }
        return resultado;
    }
}
