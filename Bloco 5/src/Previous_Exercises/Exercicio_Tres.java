package Previous_Exercises;

public class Exercicio_Tres {
    /**
     * Method to return the sum of all the numbers in an array.
     *
     * @param arrayInicial Array of type Integer.
     * @return Integer representing the sum of the values in an array.
     */
    public static int somaDeElementos(int[] arrayInicial) {
        int somaDeDigitos = 0;
        for (int digito : arrayInicial) {
            somaDeDigitos += digito;

        }
        return somaDeDigitos;
    }
}
