package Previous_Exercises;

import java.util.*;

public abstract class BlocoTres {

    public static void main(String[] args) {
        /*
        exercicio1Process(2);
        exercicio2Logic(4, new double[]{12, 23, 2, 113});
        exercicio3(new float[]{2f,4f,67f,8f});
        exercicio4A(2,6);
        exercicio4B(3,7,5);
        exercicio4C(2,5);
        exercicio4D(2,6,3,4);
        exercicio4E(2,3,5,7);
        exercicio5A(3,7);
        exercicio5B(2,7);
        exercicio5C(3,8);
        exercicio5D(2,7);
        exercicio5E(2,3,6);
        ex6InverterNumero(223);
        ex6NumeroDeImpares(22345);
        ex6SomaDeImpares(1235);
        ex6NumeroDigitos(123);
        ex6SomaDigitos(123);
        ex6MediaDigitos(123);
        ex6MediaImpares(123);
        ex6MediaPares(234);
        ex7ACapicua(1221);
        ex7BArmstrongNumber(123);
        double[] salario ={300,400,500};
        double[] horas ={2,3,4,-1};
        System.out.println(exercicio9(horas,salario));
        exercicio13();
        ex14();
        ex15();
        */

    }

    /*
    public static void exercicio1() {
        Scanner ler = new Scanner(System.in);
        //int res =1;
        System.out.println("Introduza um numero inteiro positivo");
        int num = ler.nextInt();
        if (num <= 0) {
            System.out.println("Isso não é um número positivo");
        } else {
            int res = exercicio1Process(num);
            System.out.println("O resultado é: " + res);
        }

    }

    public static int exercicio1Process(int num) {
        int x, res = 1;
        for (x = num; x >= 1; x--) {
            res = res * x;
        }
        return res;
    }


    public static void exercicio2() {
        Scanner ler = new Scanner(System.in);
        double somaNegativos = 0, numNegativos = 0, numPositivos = 0;
        System.out.println("Qual o numero de alunos?");
        int alunos = ler.nextInt();

        for (int i = 1; i <= alunos; i++) {

            System.out.println("Qual a nota do " + i + "º aluno?");
            double nota = ler.nextDouble();
            if (nota >= 10) {
                numPositivos++;
            } else {
                numNegativos++;
                somaNegativos += nota;
            }

        }
        System.out.println("A percentagem de alunos positivos é de " + numPositivos / alunos * 100 + " e a média de alunos negativos é de " + somaNegativos / numNegativos);
    }

    public static String exercicio2Logic(int alunos, double[] notas) {
        double somaNegativos = 0, numNegativos = 0, numPositivos = 0;
        for (int i = 0; i < alunos; i++) {

            double nota = notas[i];
            if (nota >= 10) {
                numPositivos++;
            } else {
                numNegativos++;
                somaNegativos += nota;
            }

        }

        int resPos = (int) (numPositivos / alunos * 100);
        double resNeg = (somaNegativos / numNegativos);
        return "Positivos " + resPos + " Negativos " + String.format("%.1f", resNeg);

    }

    public static String exercicio3(float[] numeros) {
        float pares = 0f, impares = 0f;
        int i = 0, numerosImapares = 0;
        while (numeros[i] > 0) {
            if (numeros[i] % 2 == 0) {
                pares++;
            } else {
                impares++;
                numerosImapares += numeros[i];
            }
            i++;
        }
        return "Pares:" + pares / i * 100 + " Impares:" + numerosImapares / impares;
    }

    public static int exercicio4A(int numMin, int numMax) {
        int res = 0;
        for (int i = numMin; i <= numMax; i++) {
            if (i % 3 == 0) {
                res++;
            }
        }
        return res;
    }

    public static int exercicio4B(int numMin, int numMax, int num) {
        int res = 0;
        for (int i = numMin; i <= numMax; i++) {
            if (i % num == 0) {
                res++;
            }
        }
        return res;
    }

    public static int exercicio4C(int numMin, int numMax) {
        int res = 0;
        for (int i = numMin; i <= numMax; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                res++;
            }
        }
        return res;
    }

    public static int exercicio4D(int num1, int num2, int numMin, int numMax) {
        int res = 0;
        for (int i = numMin; i <= numMax; i++) {
            if (i % num1 == 0 && i % num2 == 0) {
                res++;
            }
        }
        return res;
    }

    public static int exercicio4E(int num1, int num2, int numMin, int numMax) {
        int res = 0;
        for (int i = numMin; i <= numMax; i++) {
            if (i % num1 == 0 && i % num2 == 0) {
                res = res + i;
            }
        }
        return res;
    }

    public static int exercicio5A(int numMin, int numMax) {
        int res = 0;
        for (int i = numMin; i <= numMax; i++) {
            if (ePar(i)) {
                res = res + i;
            }
        }
        return res;
    }

    public static int exercicio5B(int numMin, int numMax) {
        int res = 0;
        for (int i = numMin; i <= numMax; i++) {
            if (i % 2 == 0) {
                res++;
            }
        }
        return res;
    }

    public static int exercicio5C(int numMin, int numMax) {
        int res = 0;
        for (int i = numMin; i <= numMax; i++) {
            if (i % 2 != 0) {
                res = res + i;
            }
        }
        return res;
    }

    public static int exercicio5D(int numMin, int numMax) {
        int res = 0;
        for (int i = numMin; i <= numMax; i++) {
            if (i % 2 != 0) {
                res++;
            }
        }
        return res;
    }

    public static int exercicio5E(int num1, int num2, int numMultiplo) {
        int res = 0;
        if (num1 > num2) {
            for (int i = num2; i <= num1; i++) {
                if (i % numMultiplo == 0) {
                    res = res + i;
                }
            }
        } else if (num2 > num1) {
            for (int i = num1; i <= num2; i++) {
                if (i % numMultiplo == 0) {
                    res = res + i;
                }
            }
        }
        return res;
    }

    public static int exercicio5F(int num1, int num2, int numMultiplo) {
        int res = 1;
        if (num1 > num2) {
            for (int i = num2; i <= num1; i++) {
                if (i % numMultiplo == 0) {
                    res = res * i;
                }
            }
        } else if (num2 > num1) {
            for (int i = num1; i <= num2; i++) {
                if (i % numMultiplo == 0) {
                    res = res * i;
                }
            }
        }
        return res;
    }

    public static float exercicio5G(float num1, float num2, float numMultiplo) {
        float res = 0f, denominator = 0;
        if (num1 > num2) {
            for (float i = num2; i <= num1; i++) {
                if (i % numMultiplo == 0) {
                    res = res + i;
                    denominator++;
                }
            }
            return res / denominator;
        } else if (num2 > num1) {
            for (float i = num1; i <= num2; i++) {
                if (i % numMultiplo == 0) {
                    res = res + i;
                    denominator++;
                }
            }
            return res / denominator;
        } else
            return 0;

    }

    public static float exercicio5H(float num1, float num2, float numMultiplo1, float numMultiplo2) {
        float res = 0f, denominator = 0;
        if (num1 > num2) {
            for (float i = num2; i <= num1; i++)
                if (i % numMultiplo1 == 0 || i % numMultiplo2 == 0) {
                    res = res + i;
                    denominator++;
                }
            return res / denominator;
        } else if (num2 > num1) {
            for (float i = num1; i <= num2; i++)
                if (i % numMultiplo1 == 0 || i % numMultiplo2 == 0) {
                    res = res + i;
                    denominator++;
                }
            return res / denominator;
        } else
            return 0;

    }
*/
    public static int ex6NumeroDigitos(long num) {
        String numString = Long.toString(num);
        return numString.length();
    }

    public static double ex6NumeroDePares(int num) {
        double pares = 0;
        for (double i = ex6NumeroDigitos(num) - 1; i >= 0; i--) {
            int digito = (int) ((num / Math.pow(10, i)) % 10);
            if (ePar(digito)) {
                pares++;
            }
        }
        return pares;
    }

    public static boolean ePar(int digito) {
        return digito % 2 == 0;
    }

    public static double ex6NumeroDeImpares(int num) {
        int impares = 0;
        for (double i = ex6NumeroDigitos(num) - 1; i >= 0; i--) {
            int digito = (int) ((num / Math.pow(10, i)) % 10);
            if (eImpar(digito)) {
                impares++;
            }
        }
        return impares;
    }

    public static boolean eImpar(int digito) {
        return !ePar(digito);
    }

    public static double ex6SomaDigitos(int num) {
        int soma = 0;
        for (double i = ex6NumeroDigitos(num) - 1; i >= 0; i--) {
            int digito = (int) ((num / Math.pow(10, i)) % 10);
            soma = soma + digito;
        }
        return soma;
    }

    public static double ex6SomaDePares(int num) {
        double soma = 0;
        for (double i = ex6NumeroDigitos(num) - 1; i >= 0; i--) {
            int digito = (int) ((num / Math.pow(10, i)) % 10);
            if (ePar(digito)) {
                soma = soma + digito;
            }
        }
        return soma;
    }

    public static double ex6SomaDeImpares(int num) {
        double soma = 0;
        for (double i = ex6NumeroDigitos(num) - 1; i >= 0; i--) {
            int digito = (int) ((num / Math.pow(10, i)) % 10);
            if (eImpar(digito)) {
                soma = soma + digito;
            }
        }
        return soma;
    }
/*
    public static double ex6MediaDigitos(int num) {
        return ex6SomaDigitos(num) / ex6NumeroDigitos(num);
    }

    public static double ex6MediaPares(int num) {
        return ex6SomaDePares(num) / ex6NumeroDePares(num);
    }

    public static double ex6MediaImpares(int num) {
        return ex6SomaDeImpares(num) / ex6NumeroDeImpares(num);
    }
*/
    public static int ex6InverterNumero(int num) {
        int inverso = 0;
        while (num != 0) {
            inverso = inverso * 10;
            inverso = inverso + num % 10;
            num = num / 10;
        }
        return inverso;
    }

    public static boolean ex7ACapicua(int num) {
        return ex6InverterNumero(num) == num;
    }

    public static boolean ex7BArmstrongNumber(int num) {
        String numString = Integer.toString(num);
        double power = numString.length();
        int soma = 0;
        for (double i = power - 1; i >= 0; i--) {
            int digito = (int) ((num / Math.pow(10, i)) % 10);
            soma = (int) (soma + Math.pow(digito, 3));
        }
        return soma == num;
    }
/*
    public static int ex7CPrimeiraCapicua(int num1, int num2) {
        int capicua1 = -1;
        for (int i = num1; i <= num2; i++) {
            if (ex7ACapicua(i) && capicua1 == -1) {
                capicua1 = i;
            }
        }
        return capicua1;
    }

    public static int ex7DMaiorCapicua(int num1, int num2) {
        int capicua1 = -1;
        for (int i = num2; i >= num1; i--) {
            if (ex7ACapicua(i) && capicua1 == -1) {
                capicua1 = i;
            }
        }
        return capicua1;
    }

    public static int ex7ENumeroCapicuas(int num1, int num2) {
        int contador = 0;
        for (int i = num1; i <= num2; i++) {
            if (ex7ACapicua(i)) {
                contador++;
            }
        }
        return contador;
    }

    public static int ex7FPrimeiroArmstrong(int num1, int num2) {
        int armstrong1 = -1;
        for (int i = num1; i <= num2; i++) {
            if (ex7BArmstrongNumber(i) && armstrong1 == -1) {
                armstrong1 = i;

            }

        }
        return armstrong1;
    }

    public static int ex7GNumeroArmstrong(int num1, int num2) {
        int contador = 0;
        for (int i = num1; i <= num2; i++) {
            if (ex7BArmstrongNumber(i)) {
                contador++;
            }
        }
        return contador;
    }

    public static int ex8alt(int numeroAlvo) {
        int somaComulativa = 0, menorNumero = 0, numeroAtual;
        Scanner ler = new Scanner(System.in);
        while (somaComulativa <= numeroAlvo) {
            System.out.println("Introduza um número:");
            numeroAtual = ler.nextInt();
            somaComulativa += numeroAtual;
            if (numeroAtual < menorNumero || menorNumero == 0) {
                menorNumero = numeroAtual;
            }
        }
        return menorNumero;
    }

    public static int exercicio8(int[] set, int alvo) {
        int index = 0;
        int soma = 0;
        List<Integer> numerosUsados = new ArrayList<>();
        while (soma <= alvo) {
            soma = set[index] + soma;
            numerosUsados.add(set[index]);
            index++;

        }
        Collections.sort(numerosUsados);
        return numerosUsados.get(0);
    }

    public static String exercicio9(double[] horasExtra, double[] salarioBase) {
        int i = 0;
        double totalPagoEmSalarios = 0;
        while (horasExtra[i] >= 0) {
            double salarioExtra = horasExtra[i] * (salarioBase[i] * 0.02);
            System.out.println("O funcionário numero " + (i + 1) + " vai receber " + (salarioBase[i] + salarioExtra) + " no total. Base: " + salarioBase[i] + " Extra: " + salarioExtra);

            totalPagoEmSalarios += salarioExtra + salarioBase[i];
            i++;

        }
        return "A média dos salários pagos é " + totalPagoEmSalarios / i;
    }

    public static int exercicio10(int numeroLimite, int[] numeros) {
        int produto = 1;
        int j = 0;
        List<Integer> numerosUsados = new ArrayList<>();
        while (produto <= numeroLimite) {
            numerosUsados.add(numeros[j]);
            produto *= numeros[j];
            j++;


        }
        Collections.sort(numerosUsados);
        return numerosUsados.get(numerosUsados.size() - 1);
    }


    public static String ex11VerificarCombinacoesSoma1a10(int numero) {
        StringBuilder combinacoesPossiveis = new StringBuilder();
        int numeroDeCombinacoes = 0;
        if (numero < 1 || numero > 20) {
            return "Número tem de ser entre 1 a 20.";
        }
        for (int i = 0; i < 11; i++) {
            for (int j = 10; j >= 0 && j >= i; j--) {
                if (j + i == numero) {
                    combinacoesPossiveis.append(i).append("+").append(j).append(";");
                    numeroDeCombinacoes++;
                }
            }
        }
        return "O numero " + numero + " tem " + numeroDeCombinacoes + " combinações possíveis: " + combinacoesPossiveis;
    }

    public static List<Double> exercicio12(double a, double b, double c) {
        double raiz = Math.pow(b, 2) - 4 * a * c;
        double x1;
        double x2;
        List<Double> resultados = new ArrayList<>();
        if (raiz > 0) {
            x1 = (-b + Math.sqrt(b * b - 4 * a * c)) / (2 * a);
            x2 = (-b - Math.sqrt(b * b - 4 * a * c)) / (2 * a);
            resultados.add(x1);
            resultados.add(x2);
            Collections.sort(resultados);
            return resultados;

        } else if (raiz == 0) {
            x1 = (-b + Math.sqrt(b * b - 4 * a * c)) / (2 * a);
            resultados.add(x1);
            Collections.sort(resultados);
            return resultados;

        } else {

            return resultados;
        }

    }

    public static void exercicio13() {
        HashMap<Integer, String> codigos;
        codigos = new HashMap<>();
        codigos.put(1, "Alimento não perecível");
        codigos.put(2, "Alimento perecível");
        codigos.put(3, "Alimento perecível");
        codigos.put(4, "Alimento perecível");
        codigos.put(5, "Vestuário");
        codigos.put(6, "Vestuário");
        codigos.put(7, "Higiene pessoal");
        codigos.put(8, "Limpeza e utensílios domésticos");
        codigos.put(9, "Limpeza e utensílios domésticos");
        codigos.put(10, "Limpeza e utensílios domésticos");
        codigos.put(11, "Limpeza e utensílios domésticos");
        codigos.put(12, "Limpeza e utensílios domésticos");
        codigos.put(13, "Limpeza e utensílios domésticos");
        codigos.put(14, "Limpeza e utensílios domésticos");
        codigos.put(15, "Limpeza e utensílios domésticos");

        Scanner ler = new Scanner(System.in);

        int num = 1;
        while (num != 0) {
            System.out.println("Introduza o código do produto:");
            num = ler.nextInt();
            if (num > 15 || num < 0) {
                System.out.println("Código Inválido. Insira código válido ou prima 0 para terminar.");
                System.out.println("--------------");
            } else {
                System.out.println(codigos.get(num));
                System.out.println("--------------");
            }


        }
        System.out.println("Programa terminado.");

    }

    public static void ex14() {
        Scanner ler = new Scanner(System.in);
        float valor = 0f;
        while (valor >= 0) {
            System.out.println("Introduza o valor em € para converter:");
            valor = ler.nextFloat();
            if (valor < 0) {
                System.out.println("-------------");
                System.out.println("Programa terminado.");
                System.out.println("-------------");
                break;
            }
            System.out.println("Introduza a moeda para qual quer converter:");
            System.out.println("D (dólar), L (libra), I (Iene), C (Coroa Sueca) e F (Franco Suíço)");
            char moeda = ler.next().charAt(0);
            while (moeda != 'd' && moeda != 'D' && moeda != 'l' && moeda != 'L' && moeda != 'I' && moeda != 'i' && moeda != 'c' && moeda != 'C' && moeda != 'f' && moeda != 'F') {
                System.out.println("-------------");
                System.out.println("Código de moeda inválido!");
                System.out.println("-------------");
                System.out.println("Introduza a moeda para qual quer converter:");
                System.out.println("D (dólar), L (libra), I (Iene), C (Coroa Sueca) e F (Franco Suíço)");
                moeda = ler.next().charAt(0);
            }

            System.out.println("-------------");
            System.out.println(ex14process(moeda, valor));
            System.out.println("-------------");
        }


    }

    public static String ex14process(char moeda, float valor) {
        String moedaNova = null;
        float cambio = 0f;

        if (moeda == 'D' || moeda == 'd') {
            moedaNova = "Dólares";
            cambio = valor * 1.534f;
        } else if (moeda == 'L' || moeda == 'l') {
            moedaNova = "Libras";
            cambio = valor * 0.774f;
        } else if (moeda == 'I' || moeda == 'i') {
            moedaNova = "Ienes";
            cambio = valor * 161.480f;
        } else if (moeda == 'C' || moeda == 'c') {
            moedaNova = "Coroas";
            cambio = valor * 9.593f;
        } else if (moeda == 'F' || moeda == 'f') {
            moedaNova = "Franco Suíço";
            cambio = valor * 1.601f;
        } else {
            if (!(valor < 0)) {
                moedaNova = "=Moeda Introduzida Inválida!=";
            }
        }


        return valor + "€ equivale a " + cambio + " " + moedaNova;
    }

    public static void ex15() {
        Scanner ler = new Scanner(System.in);
        int nota;
        do {
            System.out.println("Insira a nota do aluno:");
            nota = ler.nextInt();
            System.out.println("---------------");
            System.out.println("O aluno é " + ex15process(nota));
            System.out.println("---------------");
        } while (nota >= 0);
        System.out.println("---------------");
        System.out.println("Programa terminado.");
        System.out.println("---------------");
    }

    public static String ex15process(int nota) {
        if (nota > 20) {
            return "Nota inválida. Insira notas entre 0 e 20. Nota negativa termina o programa.";
        } else if (nota >= 0 && nota <= 4) {
            return "Mau";
        } else if (nota >= 5 && nota <= 9) {
            return "Medíocre";
        } else if (nota >= 10 && nota <= 13) {
            return "Suficiente";
        } else if (nota >= 14 && nota <= 17) {
            return "Bom";
        } else if (nota >= 18) {
            return "Muito Bom";
        } else {
            return null;
        }

    }

    public static float ex16(float salarioBruto) {
        if (salarioBruto < 500) {
            return salarioBruto - (salarioBruto * 0.10f);
        } else if (salarioBruto >= 500 && salarioBruto < 1000) {
            return salarioBruto - (salarioBruto * 0.15f);
        } else {
            return salarioBruto - (salarioBruto * 0.20f);
        }
    }

    public static String ex17(int[] peso, int[] racao) {

        for (int i = 0; i < peso.length; i++)
            do {
                if (peso[i] >= 0 && peso[i] <= 10) {
                    //Pequenos
                    if (racao[i] == 100) {
                        return "A quantidade de ração é adequada.";
                    } else {
                        return "A quantidade é desadequada! Um cão pequeno deve comer 100g/dia!";
                    }
                } else if (peso[i] > 10 && peso[i] <= 25) {
                    //Médio
                    if (racao[i] == 250) {
                        return "A quantidade de ração é adequada.";
                    } else {
                        return "A quantidade é desadequada! Um cão médio deve comer 250g/dia!";
                    }
                } else if (peso[i] > 25 && peso[i] <= 45) {
                    //Grande
                    if (racao[i] == 300) {
                        return "A quantidade de ração é adequada.";
                    } else {
                        return "A quantidade é desadequada! Um cão grande deve comer 300g/dia!";
                    }
                } else if (peso[i] > 45) {
                    //Gigante
                    if (racao[i] == 500) {
                        return "A quantidade de ração é adequada.";
                    } else {
                        return "A quantidade é desadequada! Um cão gigante deve comer 500g/dia!";
                    }
                }


            } while (true);

        return null;
    }

    public static boolean ex18(int numCC, int numeroVerificacao) {
        int numValidar = 0;
        if (ex6NumeroDigitos(numCC) != 8 || ex6NumeroDigitos(numeroVerificacao) != 1) {
            return false;
        } else {
            int num9Digitos = numCC * 10 + numeroVerificacao;
            String numString = Integer.toString(num9Digitos);
            for (int i = 0; i < 9; i++) {
                numValidar += (int) numString.charAt(i) * (9 - i);
            }
        }
        return numValidar % 11 == 0;
    }

    public static int ex19(int numero) {
        StringBuilder numeroOrganizado2 = new StringBuilder();
        StringBuilder numeroOrganizado1 = new StringBuilder();
        String numString = Integer.toString(numero);
        for (int i = 0; i < ex6NumeroDigitos(numero); i++) {
            if ((int) numString.charAt(i) % 2 == 0) {
                numeroOrganizado2.append(numString.charAt(i));

            } else numeroOrganizado1.append(numString.charAt(i));
        }
        String numeroFinal = numeroOrganizado1.toString() + numeroOrganizado2;
        return Integer.parseInt(numeroFinal);
        
    }

    public static String ex20(int numero) {
        int soma = 0;
        for (int i = 1; i < numero; i++) {
            if (numero % i == 0) {
                soma += i;
            }
        }
        if (soma == numero) {
            return numero + " é um número Perfeito.";
        } else if (soma > numero) {
            return numero + " é um número Abundante.";
        } else {
            return numero + " é um número Reduzido.";
        }

    }

*/
}