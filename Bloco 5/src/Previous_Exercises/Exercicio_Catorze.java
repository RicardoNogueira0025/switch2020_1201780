package Previous_Exercises;

import java.util.ArrayList;
import java.util.Collections;

public class Exercicio_Catorze {
    /**
     * Method to check if a matrix is rectangular (Different number of rows and columns, with a constant number of columns)
     *
     * @param matriz A 2-dimensional Array of the type Float.
     * @return boolean True if matrix is rectangular.
     */
    public static boolean matrizERetangular(int[][] matriz) {
        return !Exercicio_Treze.matrizEQuadrada(matriz) && Exercicio_Doze.mesmoNumeroColunas(matriz) != -1;
    }
}
