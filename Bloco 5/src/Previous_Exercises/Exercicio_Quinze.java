package Previous_Exercises;

import java.util.ArrayList;
import java.util.List;

public class Exercicio_Quinze {
    /**
     * Method that returns the smallest value of a Matrix.
     *
     * @param matriz 2-dimension Array of Float type.
     * @return Float representing the smallest value of the given matrix.
     */
    public static int elementoMenorValorMatriz(int[][] matriz) {
        int elementoMenor = matriz[0][0];
        for (int[] floats : matriz) {
            for (int elemento : floats) {
                if (elemento < elementoMenor) {
                    elementoMenor = elemento;
                }
            }
        }
        return elementoMenor;
    }

    /**
     * Method that returns the largest value of a Matrix.
     *
     * @param matriz 2-dimension Array of Float type.
     * @return Float representing the largest value of the given matrix.
     */
    public static float elementoMaiorValorMatriz(int[][] matriz) {
        float elementoMenor = matriz[0][0];
        for (int[] floats : matriz) {
            for (float elemento : floats) {
                if (elemento > elementoMenor) {
                    elementoMenor = elemento;
                }
            }
        }
        return elementoMenor;
    }

    /**
     * Method that returns the average value of a Matrix.
     *
     * @param matriz 2-dimension Array of Float type.
     * @return Float representing the average value of the given matrix.
     */
    public static float mediaElementosMatriz(int[][] matriz) {
        int numeroElementos = 0, soma = 0;
        for (int[] floats : matriz) {
            for (float elemento : floats) {
                soma += elemento;
                numeroElementos++;
            }
        }
        return (float) soma / numeroElementos;
    }

    /**
     * Method that returns the product of the multiplication of all the values of a Matrix.
     *
     * @param matriz 2-dimension Array of Integer type.
     * @return Long representing the product of all the values of the given matrix.

    public static long produtoElementosMatriz(int[][] matriz) {
        int produtoElementos = 1;
        for (int[] ints : matriz) {
            for (int elemento : ints) {
                produtoElementos *= elemento;
            }
        }
        return produtoElementos;
    }

    /**
     * Method that returns all the values of a given matrix without repetitions.
     *
     * @param matriz 2-dimensional Array of Integer type.
     * @return Array of Integer type with all the values of the given Matrix without repetitions.

    public static int[] elementosNaoRepetidosMatriz(int[][] matriz) {
        List<Integer> elementosUnicosLista = new ArrayList<>();
        for (int[] ints : matriz) {
            int[] listaTemp = Exercicio_Dez.elementosNaoRepetidos(ints);
            for (int num : listaTemp) {
                elementosUnicosLista.add(num);
            }
        }
        int[] elementosUnicos = elementosUnicosLista.stream().mapToInt(i -> i).toArray();
        return Exercicio_Dez.elementosNaoRepetidos(elementosUnicos);
    }

    /**
     * Method that returns all the prime values of a given matrix.
     *
     * @param matriz 2-dimensional Array of Integer type.
     * @return Array of Integer type with all the prime values of the given Matrix.

    public static int[] elementosPrimosNumaMatriz(int[][] matriz) {
        List<Integer> elementosPrimosLista = new ArrayList<>();
        for (int[] ints : matriz) {
            int[] listaTemp = Exercicio_Dez.arrayDePrimos(ints);
            for (int num : listaTemp) {
                elementosPrimosLista.add(num);
            }
        }
        int[] elementosPrimos = elementosPrimosLista.stream().mapToInt(i -> i).toArray();
        return Exercicio_Dez.elementosNaoRepetidos(elementosPrimos);
    }

    /**
     * Method that returns all the values that represent the primary diagonal of a given matrix.
     *
     * @param matriz 2-dimensional Array of Float type.
     * @return Array of Float type with all the values of the given Matrix's primary diagonal.
     */
    public static int[] diagonalPrincipalMatriz(int[][] matriz) {
        if (Exercicio_Treze.matrizEQuadrada(matriz) || Exercicio_Catorze.matrizERetangular(matriz)) {
            int[] diagonal = new int[matriz.length];
            for (int i = 0; i < matriz.length; i++) {
                diagonal[i] = matriz[i][i];
            }
            return diagonal;
        }
        return null;
    }

    /**
     * Method that returns all the values that represent the secondary diagonal of a given matrix.
     *
     * @param matriz 2-dimensional Array of Float type.
     * @return Array of Float type with all the values of the given Matrix's secondary diagonal.
     */
    public static int[] diagonalSecundariaMatriz(int[][] matriz) {
        if (Exercicio_Treze.matrizEQuadrada(matriz) || Exercicio_Catorze.matrizERetangular(matriz)) {

            int index = 0;
            if (Exercicio_Treze.matrizEQuadrada(matriz)) {
                int[] diagonal = new int[matriz[0].length];
                for (int i = (matriz[0].length) - 1; i >= 0; i--) {
                    diagonal[index] = matriz[index][i];
                    index++;
                }
                return diagonal;
            } else if (Exercicio_Catorze.matrizERetangular(matriz)) {
                int[] diagonal = new int[matriz[0].length - 1];
                for (int i = (matriz[0].length) - 1; i > 0; i--) {
                    diagonal[index] = matriz[index][i];
                    index++;
                }
                return diagonal;
            }


        }
        return null;
    }
}
    /**
     * Method that checks if a given matrix is an Identity Matrix.
     *
     * @param matriz 2-dimesnional Array of Float type.
     * @return boolean True if the given matrix is an Identity Matrix.

    public static boolean matrizIdentidade(int[][] matriz) {
        if (!Exercicio_Treze.matrizEQuadrada(matriz)) {
            return false;
        }
        if (elementoMenorValorMatriz(matriz) != 0 || elementoMaiorValorMatriz(matriz) != 1) {
            return false;
        }
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                if (i != j) {
                    if (matriz[i][j] != 0) {
                        return false;
                    }
                } else {
                    if (matriz[i][j] != 1) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public static int[][] inversoMatriz(int[][] matriz) {
        if (!Exercicio_Treze.matrizEQuadrada(matriz)) {
            return null;
        }
        int[][] matrizInversa;
        if (matriz.length == 2) {
            if (((matriz[0][0] * matriz[1][1] - matriz[0][1] * matriz[1][0]) == 0)) {
                return null;
            }
            matrizInversa = matrizInversa2_2(matriz);
            return matrizInversa;//alterar para o fim, quando estiver tudo feito.
        } else if (matriz.length == 3) {
            matrizInversa = new int[matriz.length][matriz.length];
            matrizInversa = originarIdentidade(matrizInversa);

        }


        return null;
    }
    */
