import Previous_Exercises.BlocoTres;
import Previous_Exercises.Exercicio_Dez;
import Previous_Exercises.Exercicio_Dois;
import Previous_Exercises.Exercicio_Nove;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Vector {

    private int[] array = new int[0];

    public Vector() {
    }

    public Vector(int number) {
        addValue(number);
    }

    public Vector(int[] array) {
        this.array = array;
    }

    public int[] toArray() {
        return this.array;
    }

    public void addValue(int value) {
        int i = 0;
        int[] tempArray = new int[array.length + 1];
        for (int element : this.array) {
            tempArray[i] = element;
            i++;
        }
        this.array = Arrays.stream(tempArray).toArray();
        this.array[array.length - 1] = value;
    }

    public boolean isEmpty() {
        return this.array.length == 0;
    }

    private void checkIfEmpty() {
        if (this.array.length == 0) {
            throw new IllegalStateException("Array is empty.");
        }
    }

    public void removeFirst(int number) {
        checkIfEmpty();
        int newArrayIndex = 0;
        int[] tempArray = new int[array.length - 1];
        boolean flag = false;
        for (int j : this.array) {
            if (j == number && !flag) {
                flag = true;
            } else {
                tempArray[newArrayIndex] = j;
                newArrayIndex++;
            }
        }
        this.array = tempArray;
    }

    public int returnValueWithIndexOf(int index) {
        if (index >= this.array.length) {
            throw new IllegalArgumentException("Index inserted is out of bounds");
        }
        return this.array[index];
    }

    public int size() {
        return this.array.length;
    }

    public int biggestElement() {
        checkIfEmpty();
        int biggestElement = this.array[0];
        for (int element : this.array) {
            if (element > biggestElement) {
                biggestElement = element;
            }
        }
        return biggestElement;
    }

    public int smallestElement() {
        checkIfEmpty();
        int smallestElement = this.array[0];
        for (int element : this.array) {
            if (element < smallestElement) {
                smallestElement = element;
            }
        }
        return smallestElement;
    }

    public double average() {
        checkIfEmpty();
        double elementSum = 0;
        for (int element : this.array) {
            elementSum += element;
        }
        return elementSum / this.array.length;
    }

    public double evenAverage() {
        checkIfEmpty();
        double elementSum = 0;
        double elementCount = 0;
        for (int element : this.array) {
            if (element % 2 == 0) {
                elementSum += element;
                elementCount++;
            }
        }
        if (elementCount == 0) return 0;
        return elementSum / elementCount;
    }

    public double oddAverage() {
        checkIfEmpty();
        double elementSum = 0;
        double elementCount = 0;
        for (int element : this.array) {
            if (element % 2 != 0) {
                elementSum += element;
                elementCount++;
            }
        }
        if (elementCount == 0) return 0;
        return elementSum / elementCount;
    }

    public double averageOfMultiples(int multiple) {
        checkIfEmpty();
        double elementSum = 0;
        double elementCount = 0;
        for (int element : this.array) {
            if (element % multiple == 0) {
                elementSum += element;
                elementCount++;
            }
        }
        if (elementCount == 0) return 0;
        return elementSum / elementCount;
    }

    public void arrangeAscending() {
        checkIfEmpty();
        IntStream tempArray = Arrays.stream(this.array).sorted();
        this.array = tempArray.toArray();
    }

    public void arrangeDescending() {
        checkIfEmpty();
        int index = 0;
        IntStream tempArray = Arrays.stream(this.array).sorted();
        int[] sortedArray = tempArray.toArray();
        for (int i = sortedArray.length - 1; i >= 0; i--) {
            this.array[index] = sortedArray[i];
            index++;
        }
    }

    public boolean hasJustOneElement() {
        return this.array.length == 1;
    }

    public boolean hasJustEvens() {
        checkIfEmpty();
        for (int element : this.array) {
            if (element % 2 != 0) {
                return false;
            }
        }
        return true;
    }

    public boolean hasJustOdds() {
        checkIfEmpty();
        for (int element : this.array) {
            if (element % 2 == 0) {
                return false;
            }
        }
        return true;
    }

    public boolean hasDuplicates() {
        checkIfEmpty();
        int[] checkArray = Exercicio_Dez.elementosNaoRepetidos(this.array);
        return !(checkArray.length == this.array.length);
    }

    private int digitAmmount(int number) {
        String numString = Integer.toString(number);
        return numString.length();
    }


    private double averageOfDigitAmmount() {
        double sumOfAmmounts = 0;
        for (int element : this.array) {
            sumOfAmmounts += digitAmmount(element);
        }
        return sumOfAmmounts / (double) this.array.length;
    }

    public Vector getElementsWithNumberOfDigitsHigherThanTheAverage() {
        checkIfEmpty();
        Vector newVector = new Vector();
        int count = 0;
        int[] storageArray = new int[this.array.length];
        for (int element : this.array) {
            if (digitAmmount(element) > averageOfDigitAmmount()) {
                newVector.addValue(element);

            }
        }
        return newVector;
    }


    private double getPercentageOfEvenDigits(int element) {
        return BlocoTres.ex6NumeroDePares(element) / BlocoTres.ex6NumeroDigitos(element);
    }

    private double getAverageOfEvenDigitPercentage() {
        double percentageCount = 0;
        for (int element : this.array) {
            percentageCount += getPercentageOfEvenDigits(element);
        }
        return percentageCount / (double) this.array.length;
    }

    public Vector getElementsWhosePercentageOfEvenDigitsIsHigherThanTheAverage() {
        checkIfEmpty();
        Vector newVector = new Vector();
        for (int element : this.array) {
            if (getPercentageOfEvenDigits(element) > getAverageOfEvenDigitPercentage()) {
                newVector.addValue(element);
            }
        }
        return newVector;
    }

    public Vector getJustNumbersThatHaveAllEvenDigits() {
        checkIfEmpty();
        Vector newVector = new Vector();
        for (int element : this.array) {
            if (getPercentageOfEvenDigits(element) == 1) {
                newVector.addValue(element);
            }
        }
        newVector.checkIfEmpty();
        return newVector;
    }

    private boolean checkIfAscending(int element) {
        int[] digits = Exercicio_Dois.arrayDeDigitos(element);
        if (digits.length == 1) {
            return true;
        }
        for (int i = 1; i < digits.length; i++) {
            if (digits[i] < digits[i - 1]) {
                return false;
            }
        }
        return true;
    }

    private boolean checkIfAscendingLimited(int element, int limit) {
        int[] digits = Exercicio_Dois.arrayDeDigitos(element);
        if (digits.length < limit) {
            return false;
        }
        for (int i = 1; i < limit && i < digits.length; i++) {
            if (digits[i] < digits[i - 1]) {
                return false;
            }
        }
        return true;
    }

    public Vector getElementsThatAreAscendingSequences() {
        checkIfEmpty();
        Vector newVector = new Vector();
        for (int element : this.array) {
            if (checkIfAscending(element)) {
                newVector.addValue(element);
            }
        }
        newVector.checkIfEmpty();
        return newVector;
    }

    public Vector getAllPalindromes() {
        checkIfEmpty();
        Vector newVector = new Vector();
        for (int element : this.array) {
            if (Exercicio_Nove.verificarCapicua(element)) {
                newVector.addValue(element);
            }
        }
        newVector.checkIfEmpty();
        return newVector;
    }

    private boolean checkIfAllDigitsAreSame(int element) {
        int[] digits = Exercicio_Dois.arrayDeDigitos(element);
        int firstDigit = digits[0];
        for (int digit : digits) {
            if (digit != firstDigit) {
                return false;
            }
        }
        return true;
    }

    public Vector getAllElementsWithSameDigits() {
        checkIfEmpty();
        Vector newVector = new Vector();
        for (int element : this.array) {
            if (checkIfAllDigitsAreSame(element)) {
                newVector.addValue(element);
            }
        }
        newVector.checkIfEmpty();
        return newVector;
    }

    public Vector getAllNonArmstrongNumbers() {
        checkIfEmpty();
        Vector newVector = new Vector();
        for (int element : this.array) {
            if (!BlocoTres.ex7BArmstrongNumber(element)) {
                newVector.addValue(element);
            }
        }
        newVector.checkIfEmpty();
        return newVector;
    }

    public Vector getElementsThatAreAscendingSequences(int limit) {
        checkIfEmpty();
        Vector newVector = new Vector();
        for (int element : this.array) {
            if (checkIfAscendingLimited(element, limit)) {
                newVector.addValue(element);
            }
        }
        newVector.checkIfEmpty();
        return newVector;
    }

    public boolean isEquals(Vector vector) {
        vector.checkIfEmpty();
        int[] newArray = vector.toArray();
        if (this.array.length != newArray.length) {
            return false;
        }
        for (int i = 0; i < this.array.length; i++) {
            if (this.array[i] != newArray[i]) {
                return false;
            }
        }
        return true;
    }

}