import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MatrixTest {

    @Test
    void addValue_3ToFirstLine() {
        Matrix matrix = new Matrix();
        matrix.addValue(3, 0);
        int[][] expected = {{3}};
        assertArrayEquals(expected, matrix.toMatrix());
    }

    @Test
    void addValue_3ToSecondLine() {
        Matrix matrix = new Matrix();
        matrix.addValue(3, 1);
        int[][] expected = {{}, {3}};
        assertArrayEquals(expected, matrix.toMatrix());
    }

    @Test
    void addValue_3ToSecondLineWithPreviousValues() {
        int[][] biArray = {{2, 2}, {2, 2}};
        Matrix matrix = new Matrix(biArray);
        matrix.addValue(3, 1);
        int[][] expected = {{2, 2}, {2, 2, 3}};
        assertArrayEquals(expected, matrix.toMatrix());
    }

    @Test
    void toArray_Empty() {
        Matrix matrix = new Matrix();
        int[][] expected = new int[0][0];
        assertArrayEquals(expected, matrix.toMatrix());
    }

    @Test
    void toArray_2x2() {
        int[][] biArray = {{2, 2}, {2, 2}};
        Matrix matrix = new Matrix(biArray);
        int[][] expected = {{2, 2}, {2, 2}};
        assertArrayEquals(expected, matrix.toMatrix());
    }

    @Test
    void removeFirst_removeFirst_2() {
        int[][] biArray = {{2, 2}, {2, 2}};
        Matrix matrix = new Matrix(biArray);
        int[][] expected = {{2}, {2, 2}};
        matrix.removeFirst(2);
        assertArrayEquals(expected, matrix.toMatrix());
    }

    @Test
    void removeFirst_removeFirst_3() {
        int[][] biArray = {{2, 2, 3, 3}, {2, 3, 2}};
        Matrix matrix = new Matrix(biArray);
        int[][] expected = {{2, 2, 3}, {2, 3, 2}};
        matrix.removeFirst(3);
        assertArrayEquals(expected, matrix.toMatrix());
    }

    @Test
    void isEmpty_True() {
        Matrix matrix = new Matrix();
        assertTrue(matrix.isEmpty());
    }

    @Test
    void isEmpty_False() {
        int[][] biArray = {{2, 2, 3, 3}, {2, 3, 2}};
        Matrix matrix = new Matrix(biArray);
        assertFalse(matrix.isEmpty());
    }

    @Test
    void getBiggestValue_13() {
        int[][] biArray = {{2, 2, 3, 3}, {2, 13, 2}};
        Matrix matrix = new Matrix(biArray);
        int expected = 13;
        assertEquals(expected, matrix.getBiggestValue());
    }

    @Test
    void getBiggestValue_EmptyMatrix() {
        Matrix matrix = new Matrix();
        assertThrows(IllegalStateException.class, matrix::getBiggestValue);
    }

    @Test
    void getSmallestValue_EmptyMatrix() {
        Matrix matrix = new Matrix();
        assertThrows(IllegalStateException.class, matrix::getSmallestValue);
    }

    @Test
    void getSmallestValue_2() {
        int[][] biArray = {{2, 2, 3, 3}, {2, 13, 2}};
        Matrix matrix = new Matrix(biArray);
        int expected = 2;
        assertEquals(expected, matrix.getSmallestValue());
    }

    @Test
    void getAverageValue() {
        int[][] biArray = {{2, 2, 3, 3}, {2, 13, 2}};
        Matrix matrix = new Matrix(biArray);
        float expected = 3.857f;
        assertEquals(expected, matrix.getAverageValue(), 0.01);
    }

    @Test
    void getAverageValueAllSame() {
        int[][] biArray = {{2, 2, 2, 2}, {2, 2, 2}};
        Matrix matrix = new Matrix(biArray);
        float expected = 2f;
        assertEquals(expected, matrix.getAverageValue(), 0.01);
    }

    @Test
    void getArrayOfLineSums_TwoLines() {
        int[][] biArray = {{2, 2, 2, 2}, {2, 2, 2}};
        Matrix matrix = new Matrix(biArray);
        int[] expected = {8, 6};
        assertArrayEquals(expected, matrix.getArrayOfLineSums());
    }

    @Test
    void getArrayOfLineSums_4Lines() {
        int[][] biArray = {{2, 2, 2, 2}, {2, 2, 2}, {2, 4, 6, 2}, {2, 34, 33}};
        Matrix matrix = new Matrix(biArray);
        int[] expected = {8, 6, 14, 69};
        assertArrayEquals(expected, matrix.getArrayOfLineSums());
    }

    @Test
    void getArrayOfColumnSums_NoSameNumberOFColumns() {
        int[][] biArray = {{2, 2, 2, 2}, {2, 2, 2}, {2, 4, 6, 2}, {2, 34, 33}};
        Matrix matrix = new Matrix(biArray);
        int[] expected = {8, 42, 43, 4};
        assertArrayEquals(expected, matrix.getVectorOfColumnSums().toArray());
    }

    @Test
    void getArrayOfColumnSums_SameNumberOfColumns() {
        int[][] biArray = {{2, 2, 2, 2}, {2, 2, 2, 5}, {2, 4, 6, 2}, {2, 34, 33, 123}};
        Matrix matrix = new Matrix(biArray);
        int[] expected = {8, 42, 43, 132};
        assertArrayEquals(expected, matrix.getVectorOfColumnSums().toArray());
    }

    @Test
    void getLargestLineIndex_3() {
        int[][] biArray = {{2, 2, 2, 2}, {2, 2, 2}, {2, 4, 6, 2}, {2, 34, 33}};
        Matrix matrix = new Matrix(biArray);
        int expected = 3;
        assertEquals(expected, matrix.getLargestLineIndex());
    }

    @Test
    void getLargestLineIndex_0() {
        int[][] biArray = {{2, 2, 2, 2}, {2, 2, 2}};
        Matrix matrix = new Matrix(biArray);
        int expected = 0;
        assertEquals(expected, matrix.getLargestLineIndex());
    }

    @Test
    void isSquare_False() {
        int[][] biArray = {{2, 2, 2, 2}, {2, 2, 2, 2}};
        Matrix matrix = new Matrix(biArray);
        assertFalse(matrix.isSquare());
    }

    @Test
    void isSquare_False_Null() {
        Matrix matrix = new Matrix();
        assertFalse(matrix.isSquare());
    }

    @Test
    void isSquare_True() {
        int[][] biArray = {{2, 2}, {2, 2}};
        Matrix matrix = new Matrix(biArray);
        assertTrue(matrix.isSquare());
    }

    @Test
    void isSymmetricSquare_FalseNotSquare() {
        int[][] biArray = {{2, 2, 2, 2}, {2, 2, 2, 2}};
        Matrix matrix = new Matrix(biArray);
        assertFalse(matrix.isSymmetricSquare());
    }

    @Test
    void isSymmetricSquare_FalseNotSymmetric() {
        int[][] biArray = {{2, 3}, {2, 2}};
        Matrix matrix = new Matrix(biArray);
        assertFalse(matrix.isSymmetricSquare());
    }

    @Test
    void isSymmetricSquare_True() {
        int[][] biArray = {{3, -2, 4}, {-2, 6, 2}, {4, 2, 3}};
        Matrix matrix = new Matrix(biArray);
        assertTrue(matrix.isSymmetricSquare());
    }

    @Test
    void getAmountOfNonNullValuesInDiagonal_AllNonNull() {
        int[][] biArray = {{3, -2, 4}, {-2, 6, 2}, {4, 2, 3}};
        Matrix matrix = new Matrix(biArray);
        int expected = 3;
        assertEquals(expected, matrix.getAmountOfNonNullValuesInDiagonal());
    }

    @Test
    void getAmountOfNonNullValuesInDiagonal_NonSquare() {
        int[][] biArray = {{3, -2, 4}, {-2, 6, 2}, {4, 2}};
        Matrix matrix = new Matrix(biArray);
        int expected = -1;
        assertEquals(expected, matrix.getAmountOfNonNullValuesInDiagonal());
    }

    @Test
    void getAmountOfNonNullValuesInDiagonal_SomeNull() {
        int[][] biArray = {{3, -2, 4}, {-2, 6, 2}, {4, 2, 0}};
        Matrix matrix = new Matrix(biArray);
        int expected = 2;
        assertEquals(expected, matrix.getAmountOfNonNullValuesInDiagonal());
    }

    @Test
    void checkIfDiagonalsAreTheSame_False() {
        int[][] biArray = {{3, -2, 4}, {-2, 6, 2}, {4, 2, 3}};
        Matrix matrix = new Matrix(biArray);
        assertFalse(matrix.checkIfDiagonalsAreTheSame());
    }

    @Test
    void checkIfDiagonalsAreTheSame_True() {
        int[][] biArray = {{3, -2, 3}, {-2, 6, 2}, {3, 2, 3}};
        Matrix matrix = new Matrix(biArray);
        assertTrue(matrix.checkIfDiagonalsAreTheSame());
    }

    @Test
    void getAllElementsOfMatrixWhoseNumberOfDigitsIsHigherThanAverage() {
        int[][] biArray = {{343, -2, 3211}, {-224, 246, 244}, {322, 222, 313}};
        Matrix matrix = new Matrix(biArray);
        int[] expected = {343, 3211, -224, 246, 244, 322, 222, 313};
        assertArrayEquals(expected, matrix.getAllElementsOfMatrixWhoseNumberOfDigitsIsHigherThanAverage().toArray());
    }

    @Test
    void getAllElementsOfMatrixWhoseNumberOfDigitsIsHigherThanAverage_2() {
        int[][] biArray = {{34, -2555, 355}, {-224444, 6, 24533}, {334534, 5, 3465}};
        Matrix matrix = new Matrix(biArray);
        int[] expected = {-2555, -224444, 24533, 334534, 3465};
        assertArrayEquals(expected, matrix.getAllElementsOfMatrixWhoseNumberOfDigitsIsHigherThanAverage().toArray());
    }

    @Test
    void getallElementsWhosePercentageOfEvenNumbersIsHigherThanTheAverage() {
        int[][] biArray = {{34, -2555, 355}, {-224444, 6, 245393}, {334534, 5, 3465}};
        Matrix matrix = new Matrix(biArray);
        int[] expected = {34, -224444, 6, 3465};
        assertArrayEquals(expected, matrix.getallElementsWhosePercentageOfEvenNumbersIsHigherThanTheAverage().toArray());
    }

    @Test
    void invertLines() {
        int[][] biArray = {{34, -2555, 355}, {-224444, 6, 24533}, {334534, 5, 3465}};
        Matrix matrix = new Matrix(biArray);
        int[][] expected = {{355, -2555, 34}, {24533, 6, -224444}, {3465, 5, 334534}};
        matrix.invertLines();
        assertArrayEquals(expected, matrix.toMatrix());

    }

    @Test
    void invertLines_SingleElementArray() {
        int[][] biArray = {{34}, {-224444, 6, 24533}, {334534, 5, 3465}};
        Matrix matrix = new Matrix(biArray);
        int[][] expected = {{34}, {24533, 6, -224444}, {3465, 5, 334534}};
        matrix.invertLines();
        assertArrayEquals(expected, matrix.toMatrix());

    }

    @Test
    void invertColumns() {
        int[][] biArray = {{34, -2555, 355}, {-224444, 6, 24533}, {334534, 5, 3465}};
        Matrix matrix = new Matrix(biArray);
        int[][] expected = {{334534, 5, 3465}, {-224444, 6, 24533}, {34, -2555, 355}};
        matrix.invertColumns();
        assertArrayEquals(expected, matrix.toMatrix());
    }

    @Test
    void invertColumns_SingleElementArray() {
        int[][] biArray = {{34}, {-224444, 6, 24533}, {334534, 5, 3465}};
        Matrix matrix = new Matrix(biArray);
        int[][] expected = {{334534, 5, 3465}, {-224444, 6, 24533}, {34}};
        matrix.invertColumns();
        assertArrayEquals(expected, matrix.toMatrix());

    }

    @Test
    void turn180() {
        int[][] biArray = {
                {34, 344, 122},
                {-224444, 6, 24533},
                {334534, 5, 3465}};
        Matrix matrix = new Matrix(biArray);
        int[][] expected = {
                {3465, 5, 334534},
                {24533, 6, -224444},
                {122, 344, 34}};
        matrix.turn180();
        assertArrayEquals(expected, matrix.toMatrix());
    }

    @Test
    void turn90() {
        int[][] biArray = {
                {34, 344, 122},
                {-224444, 6, 24533},
                {334534, 5, 3465}};
        Matrix matrix = new Matrix(biArray);
        int[][] expected = {
                {334534, -224444, 34},
                {5, 6, 344},
                {3465, 24533, 122}};
        matrix.turn90();
        assertArrayEquals(expected, matrix.toMatrix());
    }

    @Test
    void turnMinus90() {
        int[][] biArray = {
                {34, 344, 122},
                {-224444, 6, 24533},
                {334534, 5, 3465}};
        Matrix matrix = new Matrix(biArray);
        int[][] expected = {
                {122, 24533, 3465},
                {344, 6, 5},
                {34, -224444, 334534}};
        matrix.turnMinus90();
        assertArrayEquals(expected, matrix.toMatrix());
    }

}