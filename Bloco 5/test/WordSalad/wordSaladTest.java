package WordSalad;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class wordSaladTest {
    char[][] sopa = {{'P', 'E', 'I', 'T', 'O', 'F', 'U', 'O'}, {'R', 'O', 'T', 'U', 'L', 'I', 'R', 'E'},
            {'E', 'S', 'O', 'G', 'R', 'A', 'L', 'O'}, {'F', 'U', 'A', 'R', 'I', 'N', 'E', 'I'},
            {'A', 'G', 'E', 'P', 'A', 'L', 'O', 'R'}, {'D', 'M', 'S', 'T', 'O', 'N', 'I', 'A'}, {'O', 'T', 'A', 'C', 'O', 'B', 'A', 'R'},
            {'V', 'I', 'F', 'E', 'U', 'T', 'P', 'X'}};


    String[] palavras = {"PEITO", "SOGRA", "FADO", "SAPO", "RIO", "RABO", "ARO", "ROLA", "RIA", "RALO", "OLEO", "RIO", "AMA", "ORA"};


    @Test
    void chooseWord_WordNotPresent() {
        WordList solutions = new WordList(palavras);
        WordSalad salad = new WordSalad(sopa, solutions);
        String[] expected = {"PEITO", "SOGRA", "FADO", "SAPO", "RIO", "RABO", "ARO", "ROLA", "RIA", "RALO", "OLEO", "RIO", "AMA", "ORA"};
        int[] cord1 = {0, 0};
        int[] cord2 = {0, 2};
        salad.chooseWord(cord1, cord2);
        Assertions.assertArrayEquals(expected, solutions.getWordArray());
    }

    @Test
    void chooseWord_Present() {
        WordList solutions = new WordList(palavras);
        WordSalad salad = new WordSalad(sopa, solutions);
        String[] expected = {"SOGRA", "FADO", "SAPO", "RIO", "RABO", "ARO", "ROLA", "RIA", "RALO", "OLEO", "RIO", "AMA", "ORA"};
        int[] cord1 = {0, 0};
        int[] cord2 = {0, 4};
        salad.chooseWord(cord1, cord2);
        Assertions.assertArrayEquals(expected, solutions.getWordArray());
    }

    @Test
    void chooseWord_ParseLeft() {
        WordList solutions = new WordList(palavras);
        WordSalad salad = new WordSalad(sopa, solutions);
        String[] expected = {"PEITO", "SOGRA", "FADO", "SAPO", "RIO", "RABO", "ARO", "RIA", "RALO", "OLEO", "RIO", "AMA", "ORA"};
        int[] cord1 = {4, 7};
        int[] cord2 = {4, 4};
        salad.chooseWord(cord1, cord2);
        Assertions.assertArrayEquals(expected, solutions.getWordArray());
    }

    @Test
    void chooseWord_ParseUp() {
        WordList solutions = new WordList(palavras);
        WordSalad salad = new WordSalad(sopa, solutions);
        String[] expected = {"PEITO", "SOGRA", "FADO", "SAPO", "RABO", "ARO", "ROLA", "RIA", "RALO", "OLEO", "AMA", "ORA"};
        int[] cord1 = {4, 7};
        int[] cord2 = {2, 7};
        salad.chooseWord(cord1, cord2);
        Assertions.assertArrayEquals(expected, solutions.getWordArray());
    }

    @Test
    void chooseWord_ParseDown() {
        WordList solutions = new WordList(palavras);
        WordSalad salad = new WordSalad(sopa, solutions);
        String[] expected = {"PEITO", "SOGRA", "SAPO", "RIO", "RABO", "ARO", "ROLA", "RIA", "RALO", "OLEO", "RIO", "AMA", "ORA"};
        int[] cord1 = {3, 0};
        int[] cord2 = {6, 0};
        salad.chooseWord(cord1, cord2);
        Assertions.assertArrayEquals(expected, solutions.getWordArray());
    }

    @Test
    void chooseWord_ParseDiagonalRightDown() {
        WordList solutions = new WordList(palavras);
        WordSalad salad = new WordSalad(sopa, solutions);
        String[] expected = {"PEITO", "SOGRA", "FADO", "RIO", "RABO", "ARO", "ROLA", "RIA", "RALO", "OLEO", "RIO", "AMA", "ORA"};
        int[] cord1 = {2, 1};
        int[] cord2 = {5, 4};
        salad.chooseWord(cord1, cord2);
        Assertions.assertArrayEquals(expected, solutions.getWordArray());
    }

    @Test
    void chooseWord_ParseDiagonalRightUp() {
        WordList solutions = new WordList(palavras);
        WordSalad salad = new WordSalad(sopa, solutions);
        String[] expected = {"PEITO", "SOGRA", "FADO", "SAPO", "RIO", "RABO", "ARO", "ROLA", "RIA", "RALO", "RIO", "AMA", "ORA"};
        int[] cord1 = {5, 4};
        int[] cord2 = {2, 7};
        salad.chooseWord(cord1, cord2);
        Assertions.assertArrayEquals(expected, solutions.getWordArray());
    }

    @Test
    void chooseWord_ParseDiagonalUpLeft() {
        WordList solutions = new WordList(palavras);
        WordSalad salad = new WordSalad(sopa, solutions);
        String[] expected = {"PEITO", "SOGRA", "FADO", "SAPO", "RIO", "RABO", "ARO", "ROLA", "RIA", "RALO", "OLEO", "RIO", "ORA"};
        int[] cord1 = {6, 2};
        int[] cord2 = {4, 0};
        salad.chooseWord(cord1, cord2);
        Assertions.assertArrayEquals(expected, solutions.getWordArray());
    }

    @Test
    void chooseWord_ParseDiagonalDownLeft() {
        WordList solutions = new WordList(palavras);
        WordSalad salad = new WordSalad(sopa, solutions);
        String[] expected = {"PEITO", "SOGRA", "FADO", "SAPO", "RIO", "RABO", "ARO", "ROLA", "RIA", "RALO", "OLEO", "RIO", "AMA"};
        int[] cord1 = {0, 7};
        int[] cord2 = {2, 5};
        salad.chooseWord(cord1, cord2);
        Assertions.assertArrayEquals(expected, solutions.getWordArray());
    }
}