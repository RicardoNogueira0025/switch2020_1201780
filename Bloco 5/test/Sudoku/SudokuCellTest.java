package Sudoku;

import Sudoku.SudokuCell;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SudokuCellTest {

    @Test
    void isFixedNumber_True() {
        SudokuCell cell = new SudokuCell(1, true);
        assertTrue(cell.isFixedNumber());
    }

    @Test
    void isFixedNumber_False() {
        SudokuCell cell = new SudokuCell(1, false);
        assertFalse(cell.isFixedNumber());
    }

    @Test
    void changeValue() {
        SudokuCell cell = new SudokuCell(1, false);
        int expected = 2;
        cell.changeValue(expected);
        assertEquals(expected, cell.getValue());
    }

    @Test
    void getValue() {
        SudokuCell cell = new SudokuCell(1, false);
        cell.changeValue(4);
        int expected = 4;
        int result = cell.getValue();
        assertEquals(expected, result);
    }
}