package Sudoku;

import Sudoku.SudokuGrid;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SudokuGridTest {
    int[][] matrizSudoku = {
            {0, 0, 4, 0, 0, 5, 3, 0, 9}, {0, 0, 0, 0, 0, 0, 2, 0, 4}, {0, 0, 0, 7, 0, 0, 0, 0, 0},
            {0, 7, 0, 6, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 7, 0, 0, 6, 0}, {0, 0, 3, 9, 0, 0, 0, 0, 0},
            {6, 5, 0, 0, 3, 0, 9, 0, 0}, {8, 0, 2, 0, 0, 4, 0, 5, 7}, {0, 0, 0, 0, 0, 0, 0, 0, 2}};

    SudokuGrid testGrid = new SudokuGrid(matrizSudoku);

    int[][] matrizSudoku_quaseFeita_0_0_sera_7 = {
            {0, 2, 4, 8, 6, 5, 3, 1, 9}, {5, 8, 6, 3, 1, 9, 2, 7, 4}, {3, 1, 9, 7, 4, 2, 5, 8, 6},
            {1, 7, 8, 6, 2, 3, 4, 9, 5}, {2, 9, 5, 4, 7, 1, 8, 6, 3}, {4, 6, 3, 9, 5, 8, 7, 2, 1},
            {6, 5, 1, 2, 3, 7, 9, 4, 8}, {8, 3, 2, 1, 9, 4, 6, 5, 7}, {9, 4, 7, 5, 8, 6, 1, 3, 2}};

    SudokuGrid testGrid_amostDone = new SudokuGrid(matrizSudoku_quaseFeita_0_0_sera_7);

    int[][] matrizSudoku_ColunasEFilas9FatorialMasSetoresNao = {
            {1, 2, 3, 4, 5, 6, 7, 8, 9}, {9, 1, 2, 3, 4, 5, 6, 7, 8}, {8, 9, 1, 2, 3, 4, 5, 6, 7},
            {7, 8, 9, 1, 2, 3, 4, 5, 6}, {6, 7, 8, 9, 1, 2, 3, 4, 5}, {5, 6, 7, 8, 9, 1, 2, 3, 4},
            {4, 5, 6, 7, 8, 9, 1, 2, 3}, {3, 4, 5, 6, 7, 8, 9, 1, 2}, {2, 3, 4, 5, 6, 7, 8, 9, 1}};

    SudokuGrid testGrid_MarciaMethod = new SudokuGrid(matrizSudoku_ColunasEFilas9FatorialMasSetoresNao);

    int[][] matrizSudoku_AllLines1to9 = {
            {1, 2, 3, 4, 5, 6, 7, 8, 9}, {1, 2, 3, 4, 5, 6, 7, 8, 9}, {1, 2, 3, 4, 5, 6, 7, 8, 9},
            {1, 2, 3, 4, 5, 6, 7, 8, 9}, {1, 2, 3, 4, 5, 6, 7, 8, 9}, {1, 2, 3, 4, 5, 6, 7, 8, 9},
            {1, 2, 3, 4, 5, 6, 7, 8, 9}, {1, 2, 3, 4, 5, 6, 7, 8, 9}, {1, 2, 3, 4, 5, 6, 7, 8, 9}};

    SudokuGrid testGrid_checkColumnError = new SudokuGrid(matrizSudoku_AllLines1to9);

    @Test
    void selectCellAndAddNumber_addValidNumber() {
        int lineIndex = 0;
        int columnIndex = 0;
        int number = 7;
        testGrid.selectCellAndAddNumber(lineIndex, columnIndex, number);
        assertEquals(number, testGrid.getValue(lineIndex, columnIndex));
    }

    @Test
    void selectCellAndAddNumber_addInValidNegativeNumber() {
        int lineIndex = 0;
        int columnIndex = 0;
        int number = -1;
        assertThrows(IllegalArgumentException.class, () -> testGrid.selectCellAndAddNumber(lineIndex, columnIndex, number));

    }

    @Test
    void selectCellAndAddNumber_addInValidPositiveNumber() {
        int lineIndex = 0;
        int columnIndex = 0;
        int number = 11;
        assertThrows(IllegalArgumentException.class, () -> testGrid.selectCellAndAddNumber(lineIndex, columnIndex, number));
    }

    @Test
    void selectCellAndAddNumber_addNumberOnANonEditablePosition() {
        int lineIndex = 0;
        int columnIndex = 2;
        int number = 7;
        int expected = 4;
        testGrid.selectCellAndAddNumber(lineIndex, columnIndex, number);
        assertEquals(expected, testGrid.getValue(lineIndex, columnIndex));
    }

    @Test
    void selectCellAndAddNumber_checkCorrectlyGameCompleted() {
        int lineIndex = 0;
        int columnIndex = 0;
        int number = 7;
        testGrid_amostDone.selectCellAndAddNumber(lineIndex, columnIndex, number);
        assertTrue(testGrid_amostDone.checkIfGameComplete());
    }

    @Test
    void selectCellAndAddNumber_checkIncorrectlyGameCompleted() {
        int lineIndex = 0;
        int columnIndex = 0;
        int number = 6;
        testGrid_amostDone.selectCellAndAddNumber(lineIndex, columnIndex, number);
        assertFalse(testGrid_amostDone.checkIfGameComplete());
    }

    @Test
    void selectCellAndAddNumber_checkIncorrectlyGameCompletedMarciaMethod() {
        assertFalse(testGrid_MarciaMethod.checkIfGameComplete());
    }

    @Test
    void selectCellAndAddNumber_checkIncorrectlyGameCompletedColumnError() {
        assertFalse(testGrid_checkColumnError.checkIfGameComplete());
    }
}