//import org.junit.Assert;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
//import org.junit.Test;

class VectorTest {

    @Test
    void addValueOneElement() {
        Vector vector = new Vector();
        int[] expected = {2};
        vector.addValue(2);
        assertArrayEquals(expected, vector.toArray());

    }

    @Test
    void addValueThreeElements() {
        Vector vector = new Vector();
        int[] expected = {2, 3, 5};
        vector.addValue(2);
        vector.addValue(3);
        vector.addValue(5);
        assertArrayEquals(expected, vector.toArray());

    }

    @Test
    void addValueNoElements() {
        Vector vector = new Vector();
        int[] expected = {};
        assertArrayEquals(expected, vector.toArray());

    }

    @Test
    void removeFirst_5() {
        Vector vector = new Vector();
        int[] expected = {2, 5, 3};
        vector.addValue(2);
        vector.addValue(5);
        vector.addValue(5);
        vector.addValue(3);
        vector.removeFirst(5);
        assertArrayEquals(expected, vector.toArray());
    }

    @Test
    void removeFirst_3() {
        Vector vector = new Vector();
        int[] expected = {2, 5, 3};
        vector.addValue(2);
        vector.addValue(3);
        vector.addValue(5);
        vector.addValue(3);
        vector.removeFirst(3);
        assertArrayEquals(expected, vector.toArray());
    }

    @Test
    void removeFirst_EmptyArray() {
        Vector vector = new Vector();
        assertThrows(IllegalStateException.class, () -> vector.removeFirst(2));
    }

    @Test
    void returnValueWithIndexOf_3() {
        Vector vector = new Vector();
        int expected = 7;
        int index = 3;
        vector.addValue(2);
        vector.addValue(3);
        vector.addValue(5);
        vector.addValue(7);
        assertEquals(expected, vector.returnValueWithIndexOf(index));
    }

    @Test
    void returnValueWithIndexOf_0() {
        Vector vector = new Vector(7);
        int expected = 7;
        int index = 0;
        assertEquals(expected, vector.returnValueWithIndexOf(index));
    }

    @Test
    void size_1() {
        Vector vector = new Vector(7);
        int expected = 1;
        assertEquals(expected, vector.size());

    }

    @Test
    void size_0() {
        Vector vector = new Vector();
        int expected = 0;
        assertEquals(expected, vector.size());

    }

    @Test
    void size_4() {
        Vector vector = new Vector();
        vector.addValue(2);
        vector.addValue(3);
        vector.addValue(5);
        vector.addValue(7);
        int expected = 4;
        assertEquals(expected, vector.size());

    }

    @Test
    void biggestElement_7() {
        Vector vector = new Vector();
        vector.addValue(2);
        vector.addValue(3);
        vector.addValue(5);
        vector.addValue(7);
        int expected = 7;
        assertEquals(expected, vector.biggestElement());
    }

    @Test
    void biggestElement_Minus3() {
        Vector vector = new Vector();
        vector.addValue(-12);
        vector.addValue(-3);
        vector.addValue(-5);
        vector.addValue(-7);
        int expected = -3;
        assertEquals(expected, vector.biggestElement());
    }

    @Test
    void returnValueWithIndexOf_OutOfBounds() {
        Vector vector = new Vector();
        vector.addValue(-12);
        vector.addValue(-3);
        vector.addValue(-5);
        vector.addValue(-7);
        int index = 4;
        assertThrows(IllegalArgumentException.class, () -> vector.returnValueWithIndexOf(index));
    }

    @Test
    void biggestElement_EmptyArray() {
        Vector vector = new Vector();
        assertThrows(IllegalStateException.class, vector::biggestElement);
    }

    @Test
    void smallestElement_EmptyArray() {
        Vector vector = new Vector();
        assertThrows(IllegalStateException.class, vector::smallestElement);
    }

    @Test
    void smallestElement_Minus12() {
        Vector vector = new Vector();
        vector.addValue(-3);
        vector.addValue(-5);
        vector.addValue(-12);
        vector.addValue(-7);
        int expected = -12;
        assertEquals(expected, vector.smallestElement());
    }

    @Test
    void smallestElement_2() {
        Vector vector = new Vector();
        vector.addValue(2);
        vector.addValue(3);
        vector.addValue(5);
        vector.addValue(7);
        int expected = 2;
        assertEquals(expected, vector.smallestElement());
    }

    @Test
    void smallestElement_2_OneElement() {
        Vector vector = new Vector(2);
        int expected = 2;
        assertEquals(expected, vector.smallestElement());
    }

    @Test
    void average_5Point33() {
        Vector vector = new Vector();
        vector.addValue(4);
        vector.addValue(5);
        vector.addValue(7);
        double expected = 5.33;
        assertEquals(expected, vector.average(), 0.01);
    }

    @Test
    void average_EmptyArray() {
        Vector vector = new Vector();
        assertThrows(IllegalStateException.class, vector::average);
    }

    @Test
    void evenAverage_EmptyArray() {
        Vector vector = new Vector();
        assertThrows(IllegalStateException.class, vector::evenAverage);
    }

    @Test
    void evenAverage_OneEvenElement() {
        Vector vector = new Vector();
        vector.addValue(4);
        vector.addValue(5);
        vector.addValue(7);
        double expected = 4.00;
        assertEquals(expected, vector.evenAverage(), 0.01);
    }

    @Test
    void evenAverage_NoEvenElements() {
        Vector vector = new Vector();
        vector.addValue(5);
        vector.addValue(5);
        vector.addValue(7);
        double expected = 0.00;
        assertEquals(expected, vector.evenAverage(), 0.01);
    }

    @Test
    void evenAverage_3() {
        Vector vector = new Vector();
        vector.addValue(4);
        vector.addValue(2);
        vector.addValue(7);
        double expected = 3.00;
        assertEquals(expected, vector.evenAverage(), 0.01);
    }

    @Test
    void oddAverage_EmptyArray() {
        Vector vector = new Vector();
        assertThrows(IllegalStateException.class, vector::oddAverage);
    }

    @Test
    void evenAverage_OneOddElement() {
        Vector vector = new Vector();
        vector.addValue(4);
        vector.addValue(6);
        vector.addValue(7);
        double expected = 7.00;
        assertEquals(expected, vector.oddAverage(), 0.01);
    }

    @Test
    void evenAverage_NoOddElements() {
        Vector vector = new Vector();
        vector.addValue(4);
        vector.addValue(6);
        vector.addValue(8);
        double expected = 0.00;
        assertEquals(expected, vector.oddAverage(), 0.01);
    }

    @Test
    void evenAverage_6() {
        Vector vector = new Vector();
        vector.addValue(4);
        vector.addValue(5);
        vector.addValue(7);
        double expected = 6.00;
        assertEquals(expected, vector.oddAverage(), 0.01);
    }

    @Test
    void averageOfMultiples_EmptyVector() {
        Vector vector = new Vector();
        assertThrows(IllegalStateException.class, () -> vector.averageOfMultiples(3));
    }

    @Test
    void averageOfMultiples_7Point5() {
        Vector vector = new Vector();
        vector.addValue(4);
        vector.addValue(6);
        vector.addValue(9);
        double expected = 7.5;
        int multiple = 3;
        assertEquals(expected, vector.averageOfMultiples(multiple), 0.01);
    }

    @Test
    void averageOfMultiples_noMultiples() {
        Vector vector = new Vector();
        vector.addValue(3);
        vector.addValue(6);
        vector.addValue(9);
        double expected = 0;
        int multiple = 4;
        assertEquals(expected, vector.averageOfMultiples(multiple), 0.01);
    }

    @Test
    void arrangeAscending_3Elements() {
        Vector vector = new Vector();
        vector.addValue(6);
        vector.addValue(4);
        vector.addValue(2);
        int[] expected = {2, 4, 6};
        vector.arrangeAscending();
        assertArrayEquals(expected, vector.toArray());
    }

    @Test
    void arrangeAscending_2Elements() {
        Vector vector = new Vector();
        vector.addValue(4);
        vector.addValue(2);
        int[] expected = {2, 4};
        vector.arrangeAscending();
        assertArrayEquals(expected, vector.toArray());
    }

    @Test
    void arrangeAscending_1Element() {
        Vector vector = new Vector(3);
        int[] expected = {3};
        vector.arrangeAscending();
        assertArrayEquals(expected, vector.toArray());
    }

    @Test
    void arrangeAscending_EmptyArray() {
        Vector vector = new Vector();
        assertThrows(IllegalStateException.class, vector::arrangeAscending);
    }

    @Test
    void arrangeDescending_EmptyArray() {
        Vector vector = new Vector();
        assertThrows(IllegalStateException.class, vector::arrangeDescending);
    }

    @Test
    void arrangeDescending_3Elements() {
        Vector vector = new Vector();
        vector.addValue(2);
        vector.addValue(4);
        vector.addValue(6);
        int[] expected = {6, 4, 2};
        vector.arrangeDescending();
        assertArrayEquals(expected, vector.toArray());
    }

    @Test
    void arrangeDescending_2Elements() {
        Vector vector = new Vector();
        vector.addValue(2);
        vector.addValue(4);
        int[] expected = {4, 2};
        vector.arrangeDescending();
        assertArrayEquals(expected, vector.toArray());
    }

    @Test
    void arrangeDescending_1Element() {
        Vector vector = new Vector(3);
        int[] expected = {3};
        vector.arrangeDescending();
        assertArrayEquals(expected, vector.toArray());
    }

    @Test
    void isEmpty_True() {
        Vector vector = new Vector();
        assertTrue(vector.isEmpty());
    }

    @Test
    void isEmpty_False() {
        Vector vector = new Vector(3);
        assertFalse(vector.isEmpty());
    }

    @Test
    void hasJustOneElement_True() {
        Vector vector = new Vector(3);
        assertTrue(vector.hasJustOneElement());
    }

    @Test
    void hasJustOneElement_False_EmptyArray() {
        Vector vector = new Vector();
        assertFalse(vector.hasJustOneElement());
    }

    @Test
    void hasJustOneElement_False_MultipleElements() {
        Vector vector = new Vector(3);
        vector.addValue(7);
        assertFalse(vector.hasJustOneElement());
    }

    @Test
    void hasJustEvens_EmptyArray() {
        Vector vector = new Vector();
        assertThrows(IllegalStateException.class, vector::hasJustEvens);
    }

    @Test
    void hasJustEvens_True_OneElement() {
        Vector vector = new Vector(2);
        assertTrue(vector.hasJustEvens());
    }

    @Test
    void hasJustEvens_True_MultipleElements() {
        Vector vector = new Vector(2);
        vector.addValue(4);
        assertTrue(vector.hasJustEvens());
    }

    @Test
    void hasJustEvens_False_MultipleElements() {
        Vector vector = new Vector(3);
        vector.addValue(1);
        assertFalse(vector.hasJustEvens());
    }

    @Test
    void hasJustEvens_False_OneElement() {
        Vector vector = new Vector(9);
        assertFalse(vector.hasJustEvens());
    }

    @Test
    void hasJustOdds_EmptyArray() {
        Vector vector = new Vector();
        assertThrows(IllegalStateException.class, vector::hasJustOdds);
    }

    @Test
    void hasJustOdds_True_OneElement() {
        Vector vector = new Vector(1);
        assertTrue(vector.hasJustOdds());
    }

    @Test
    void hasJustOdds_True_MultipleElements() {
        Vector vector = new Vector(1);
        vector.addValue(3);
        assertTrue(vector.hasJustOdds());
    }

    @Test
    void hasJustOdds_False_OneElement() {
        Vector vector = new Vector(4);
        assertFalse(vector.hasJustOdds());
    }

    @Test
    void hasJustOdds_False_MultipleElements() {
        Vector vector = new Vector(4);
        vector.addValue(8);
        assertFalse(vector.hasJustOdds());
    }

    @Test
    void hasDuplicates_EmptyArray() {
        Vector vector = new Vector();
        assertThrows(IllegalStateException.class, vector::hasDuplicates);
    }

    @Test
    void hasDuplicates_True_2Elements() {
        Vector vector = new Vector(3);
        vector.addValue(3);
        assertTrue(vector.hasDuplicates());
    }

    @Test
    void hasDuplicates_True_3Elements() {
        Vector vector = new Vector(3);
        vector.addValue(2);
        vector.addValue(3);
        assertTrue(vector.hasDuplicates());
    }

    @Test
    void hasDuplicates_False_1Element() {
        Vector vector = new Vector(3);
        assertFalse(vector.hasDuplicates());
    }

    @Test
    void hasDuplicates_False_2Elements() {
        Vector vector = new Vector(3);
        vector.addValue(4);
        assertFalse(vector.hasDuplicates());
    }

    @Test
    void hasDuplicates_False_3Elements() {
        Vector vector = new Vector(3);
        vector.addValue(4);
        vector.addValue(1);
        assertFalse(vector.hasDuplicates());
    }

    @Test
    void elementsWithNumberOfDigitsHigherThanTheAverage_EmptyArray() {
        Vector vector = new Vector();
        assertThrows(IllegalStateException.class, vector::getElementsWithNumberOfDigitsHigherThanTheAverage);
    }

    @Test
    void elementsWithNumberOfDigitsHigherThanTheAverage_1() {
        Vector vector = new Vector();
        vector.addValue(233);
        vector.addValue(6464567);
        vector.addValue(1232);
        vector.addValue(12);
        Vector expected = new Vector(6464567);
        Vector result = vector.getElementsWithNumberOfDigitsHigherThanTheAverage();
        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void elementsWithNumberOfDigitsHigherThanTheAverage_2() {
        Vector vector = new Vector();
        vector.addValue(233);
        vector.addValue(6464);
        vector.addValue(1232);
        vector.addValue(12);
        Vector expected = new Vector();
        expected.addValue(6464);
        expected.addValue(1232);
        Vector result = vector.getElementsWithNumberOfDigitsHigherThanTheAverage();
        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWhosePercentageOfEvenDigitsIsHigherThanTheAverage_EmptyArray() {
        Vector vector = new Vector();
        assertThrows(IllegalStateException.class, vector::getElementsWhosePercentageOfEvenDigitsIsHigherThanTheAverage);
    }

    @Test
    void getElementsWhosePercentageOfEvenDigitsIsHigherThanTheAverage_1() {
        Vector vector = new Vector();
        vector.addValue(233);
        vector.addValue(6464);
        vector.addValue(1232);
        vector.addValue(12);
        Vector expected = new Vector();
        expected.addValue(6464);
        Vector result = vector.getElementsWhosePercentageOfEvenDigitsIsHigherThanTheAverage();
        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsWhosePercentageOfEvenDigitsIsHigherThanTheAverage_2() {
        Vector vector = new Vector();
        vector.addValue(233);
        vector.addValue(22333333);
        vector.addValue(1232);
        vector.addValue(12);
        Vector expected = new Vector();
        expected.addValue(1232);
        expected.addValue(12);
        Vector result = vector.getElementsWhosePercentageOfEvenDigitsIsHigherThanTheAverage();
        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getJustNumbersThatHaveAllEvenDigits_NoAllEvenNumbers() {
        Vector vector = new Vector();
        vector.addValue(233);
        vector.addValue(22333333);
        vector.addValue(1232);
        vector.addValue(12);
        assertThrows(IllegalStateException.class, vector::getJustNumbersThatHaveAllEvenDigits);
    }

    @Test
    void getJustNumbersThatHaveAllEvenDigits_222And448() {
        Vector vector = new Vector();
        vector.addValue(222);
        vector.addValue(22333333);
        vector.addValue(1232);
        vector.addValue(448);
        Vector expected = new Vector(222);
        expected.addValue(448);
        Vector result = vector.getJustNumbersThatHaveAllEvenDigits();
        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsThatAreAscendingSequences() {
        Vector vector = new Vector();
        vector.addValue(222);
        vector.addValue(22333333);
        vector.addValue(1232);
        vector.addValue(443);
        Vector expected = new Vector(222);
        expected.addValue(22333333);
        Vector result = vector.getElementsThatAreAscendingSequences();
        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getElementsThatAreAscendingSequences_NoSequences() {
        Vector vector = new Vector();
        vector.addValue(252);
        vector.addValue(22313333);
        vector.addValue(1232);
        vector.addValue(443);
        assertThrows(IllegalStateException.class, vector::getElementsThatAreAscendingSequences);
    }

    @Test
    void getAllPalindromes_1() {
        Vector vector = new Vector();
        vector.addValue(252);
        vector.addValue(22313333);
        vector.addValue(1232);
        vector.addValue(443);
        Vector expected = new Vector(252);
        Vector result = vector.getAllPalindromes();
        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getAllPalindromes_2() {
        Vector vector = new Vector();
        vector.addValue(252);
        vector.addValue(22313333);
        vector.addValue(1221);
        vector.addValue(443);
        Vector expected = new Vector(252);
        expected.addValue(1221);
        Vector result = vector.getAllPalindromes();
        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getallPalindromes_NoPalindromes() {
        Vector vector = new Vector();
        vector.addValue(253);
        vector.addValue(22313333);
        vector.addValue(1232);
        vector.addValue(443);
        assertThrows(IllegalStateException.class, vector::getAllPalindromes);
    }

    @Test
    void getAllElementsWithSameDigits_NoElements() {
        Vector vector = new Vector();
        vector.addValue(253);
        vector.addValue(22313333);
        vector.addValue(1232);
        vector.addValue(443);
        assertThrows(IllegalStateException.class, vector::getAllElementsWithSameDigits);
    }

    @Test
    void getAllElementsWithSameDigits() {
        Vector vector = new Vector();
        vector.addValue(222);
        vector.addValue(22313333);
        vector.addValue(1);
        vector.addValue(443);
        Vector expected = new Vector(222);
        expected.addValue(1);
        Vector result = vector.getAllElementsWithSameDigits();
        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void getAllNonArmstrongNumbers_EmptyArray() {
        Vector vector = new Vector();
        vector.addValue(1);
        vector.addValue(0);
        vector.addValue(153);
        vector.addValue(407);
        assertThrows(IllegalStateException.class, vector::getAllNonArmstrongNumbers);
    }

    @Test
    void getAllNonArmstrongNumbers_370_407() {
        Vector vector = new Vector(407);
        vector.addValue(253);
        vector.addValue(22313333);
        vector.addValue(370);
        Vector expected = new Vector(253);
        expected.addValue(22313333);
        Vector result = vector.getAllNonArmstrongNumbers();
        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void testGetElementsThatAreAscendingSequences() {
        int limit = 3;
        Vector vector = new Vector(123);
        vector.addValue(253);
        vector.addValue(123221);
        vector.addValue(379);
        Vector expected = new Vector(123);
        expected.addValue(123221);
        expected.addValue(379);
        Vector result = vector.getElementsThatAreAscendingSequences(limit);
        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    void isEquals_True() {
        Vector vector = new Vector(123);
        vector.addValue(253);
        vector.addValue(123221);
        vector.addValue(379);
        Vector vector2 = new Vector(123);
        vector2.addValue(253);
        vector2.addValue(123221);
        vector2.addValue(379);
        assertTrue(vector.isEquals(vector2));
    }

    @Test
    void isEquals_FalseDifferentLengths() {
        Vector vector = new Vector(123);
        vector.addValue(253);
        vector.addValue(123221);
        vector.addValue(379);
        Vector vector2 = new Vector();
        vector2.addValue(253);
        vector2.addValue(123221);
        vector2.addValue(379);
        assertFalse(vector.isEquals(vector2));
    }

    @Test
    void isEquals_FalseSameLengths() {
        Vector vector = new Vector(123);
        vector.addValue(253);
        vector.addValue(123221);
        vector.addValue(379);
        Vector vector2 = new Vector(123);
        vector2.addValue(251);
        vector2.addValue(123221);
        vector2.addValue(379);
        assertFalse(vector.isEquals(vector2));
    }


}