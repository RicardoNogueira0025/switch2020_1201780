package Previous_Exercises;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EdificioTest {



    @Test
    void criarEdificioInvalido(){
        double alturaSombra = -3;
        assertThrows(IllegalArgumentException.class, () -> {
            Edificio edificio = new Edificio(alturaSombra);
        });
    }
}