package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class Exercicio_TrezeTest {

    @Test
    void matrizEQuadradaTrue() {
        float[][] matriz = new float[4][4];
        boolean result = Exercicio_Treze.matrizEQuadrada(matriz);
        assertTrue(result);
    }

    @Test
    void matrizEQuadradaFalseRetangulo() {
        float[][] matriz = new float[4][5];
        boolean result = Exercicio_Treze.matrizEQuadrada(matriz);
        assertFalse(result);
    }

    @Test
    void matrizEQuadradaFalse() {
        float[][] matriz = new float[5][];
        matriz[0] = new float[5];
        matriz[1] = new float[3];
        matriz[2] = new float[3];
        boolean result = Exercicio_Treze.matrizEQuadrada(matriz);
        assertFalse(result);
    }
}