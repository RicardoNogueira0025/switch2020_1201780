package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio_DezanoveTest {

    @Test
    void verificarSudokuEstaCerto_true() {
        int[][] matrizSudoku = {
                {7, 2, 4, 8, 6, 5, 3, 1, 9}, {5, 8, 6, 3, 1, 9, 2, 7, 4}, {3, 1, 9, 7, 4, 2, 5, 8, 6},
                {1, 7, 8, 6, 2, 3, 4, 9, 5}, {2, 9, 5, 4, 7, 1, 8, 6, 3}, {4, 6, 3, 9, 5, 8, 7, 2, 1},
                {6, 5, 1, 2, 3, 7, 9, 4, 8}, {8, 3, 2, 1, 9, 4, 6, 5, 7}, {9, 4, 7, 5, 8, 6, 1, 3, 2}};
        assertTrue(Exercicio_Dezanove.verificarSudokuEstaCerto(matrizSudoku));
    }

    @Test
    void verificarSudokuEstaCerto_false_emptySpace() {
        int[][] matrizSudoku = {
                {7, 2, 4, 8, 6, 5, 3, 1, 9}, {5, 8, 6, 3, 1, 9, 2, 7, 4}, {3, 1, 9, 7, 4, 2, 5, 8, 6},
                {1, 7, 8, 6, 2, 3, 0, 9, 5}, {2, 9, 5, 4, 7, 1, 8, 6, 3}, {4, 6, 3, 9, 5, 8, 7, 2, 1},
                {6, 5, 1, 2, 3, 7, 9, 4, 8}, {8, 3, 2, 1, 9, 4, 6, 5, 7}, {9, 4, 7, 5, 8, 6, 1, 3, 2}};
        assertFalse(Exercicio_Dezanove.verificarSudokuEstaCerto(matrizSudoku));
    }

    @Test
    void verificarSudokuEstaCerto_falseErrorIn_6_1() {
        int[][] matrizSudoku = {
                {7, 2, 4, 8, 6, 5, 3, 1, 9}, {5, 8, 6, 3, 1, 9, 2, 7, 4}, {3, 1, 9, 7, 4, 2, 5, 8, 6},
                {1, 7, 8, 6, 2, 3, 4, 9, 5}, {2, 9, 5, 4, 7, 1, 8, 6, 3}, {4, 6, 3, 9, 5, 8, 7, 2, 1},
                {5, 5, 1, 2, 3, 7, 9, 4, 8}, {8, 3, 2, 1, 9, 4, 6, 5, 7}, {9, 4, 7, 5, 8, 6, 1, 3, 2}};
        assertFalse(Exercicio_Dezanove.verificarSudokuEstaCerto(matrizSudoku));
    }

    @Test
    void verificarSudokuEstaCerto_falseErrorIn_4_4() {
        int[][] matrizSudoku = {
                {7, 2, 4, 8, 6, 5, 3, 1, 9}, {5, 8, 6, 3, 1, 9, 2, 7, 4}, {3, 1, 9, 7, 4, 2, 5, 8, 6},
                {1, 7, 8, 6, 2, 3, 4, 9, 5}, {2, 9, 5, 4, 3, 1, 8, 6, 3}, {4, 6, 3, 9, 5, 8, 7, 2, 1},
                {6, 5, 1, 2, 3, 7, 9, 4, 8}, {8, 3, 2, 1, 9, 4, 6, 5, 7}, {9, 4, 7, 5, 8, 6, 1, 3, 2}};
        assertFalse(Exercicio_Dezanove.verificarSudokuEstaCerto(matrizSudoku));
    }

    @Test
    void criarMatrizNumerosFixos_1() {
        int[][] matrizSudoku = {
                {0, 0, 4, 0, 0, 5, 3, 0, 9}, {0, 0, 0, 0, 0, 0, 2, 0, 4}, {0, 0, 0, 7, 0, 0, 0, 0, 0},
                {0, 7, 0, 6, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 7, 0, 0, 6, 0}, {0, 0, 3, 9, 0, 0, 0, 0, 0},
                {6, 5, 0, 0, 3, 0, 9, 0, 0}, {8, 0, 2, 0, 0, 4, 0, 5, 7}, {0, 0, 0, 0, 0, 0, 0, 0, 2}};
        int[][] expected = {
                {0, 0, 1, 0, 0, 1, 1, 0, 1}, {0, 0, 0, 0, 0, 0, 1, 0, 1}, {0, 0, 0, 1, 0, 0, 0, 0, 0},
                {0, 1, 0, 1, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 1, 0, 0, 1, 0}, {0, 0, 1, 1, 0, 0, 0, 0, 0},
                {1, 1, 0, 0, 1, 0, 1, 0, 0}, {1, 0, 1, 0, 0, 1, 0, 1, 1}, {0, 0, 0, 0, 0, 0, 0, 0, 1}};
        int[][] result = Exercicio_Dezanove.criarMatrizMapa(matrizSudoku);
        assertArrayEquals(expected, result);
    }

    @Test
    void criarMatrizNumerosFixos_2() {
        int[][] matrizSudoku = {
                {0, 0, 4, 0, 0, 5, 0, 3, 9}, {0, 0, 0, 0, 0, 0, 2, 0, 4}, {0, 0, 0, 7, 0, 0, 0, 0, 0},
                {0, 7, 0, 6, 0, 0, 0, 0, 0}, {0, 3, 0, 0, 7, 0, 0, 6, 0}, {0, 0, 3, 9, 0, 0, 0, 0, 0},
                {6, 5, 0, 0, 3, 0, 9, 0, 0}, {8, 0, 2, 0, 0, 4, 0, 5, 7}, {0, 0, 0, 0, 0, 0, 0, 0, 2}};
        int[][] expected = {
                {0, 0, 1, 0, 0, 1, 0, 1, 1}, {0, 0, 0, 0, 0, 0, 1, 0, 1}, {0, 0, 0, 1, 0, 0, 0, 0, 0},
                {0, 1, 0, 1, 0, 0, 0, 0, 0}, {0, 1, 0, 0, 1, 0, 0, 1, 0}, {0, 0, 1, 1, 0, 0, 0, 0, 0},
                {1, 1, 0, 0, 1, 0, 1, 0, 0}, {1, 0, 1, 0, 0, 1, 0, 1, 1}, {0, 0, 0, 0, 0, 0, 0, 0, 1}};
        int[][] result = Exercicio_Dezanove.criarMatrizMapa(matrizSudoku);
        assertArrayEquals(expected, result);
    }

    @Test
    void verificarSePodeIntroduzirNumero_true() {
        int[][] matrizMapa = {
                {0, 0, 1, 0, 0, 1, 0, 1, 1}, {0, 0, 0, 0, 0, 0, 1, 0, 1}, {0, 0, 0, 1, 0, 0, 0, 0, 0},
                {0, 1, 0, 1, 0, 0, 0, 0, 0}, {0, 1, 0, 0, 1, 0, 0, 1, 0}, {0, 0, 1, 1, 0, 0, 0, 0, 0},
                {1, 1, 0, 0, 1, 0, 1, 0, 0}, {1, 0, 1, 0, 0, 1, 0, 1, 1}, {0, 0, 0, 0, 0, 0, 0, 0, 1}};
        int cord1 = 0;
        int cord2 = 0;
        assertTrue(Exercicio_Dezanove.verificarSePodeIntroduzirNumero(cord1, cord2, matrizMapa));
    }

    @Test
    void verificarSePodeIntroduzirNumero_false() {
        int[][] matrizMapa = {
                {0, 0, 1, 0, 0, 1, 0, 1, 1}, {0, 0, 0, 0, 0, 0, 1, 0, 1}, {0, 0, 0, 1, 0, 0, 0, 0, 0},
                {0, 1, 0, 1, 0, 0, 0, 0, 0}, {0, 1, 0, 0, 1, 0, 0, 1, 0}, {0, 0, 1, 1, 0, 0, 0, 0, 0},
                {1, 1, 0, 0, 1, 0, 1, 0, 0}, {1, 0, 1, 0, 0, 1, 0, 1, 1}, {0, 0, 0, 0, 0, 0, 0, 0, 1}};
        int cord1 = 0;
        int cord2 = 2;
        assertFalse(Exercicio_Dezanove.verificarSePodeIntroduzirNumero(cord1, cord2, matrizMapa));

    }
}