package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class Exercicio_QuatroTest {

    @Test
    void separarPares_923874_284() {
        int[] arrayInicial = {9, 2, 3, 8, 7, 4};
        Object[] expected = {2, 8, 4};
        Object[] result = Exercicio_Quatro.separarPares(arrayInicial);
        assertArrayEquals(expected, result);
    }

    @Test
    void separarPares_29837_28() {
        int[] arrayInicial = {2, 9, 8, 3, 7};
        Object[] expected = {2, 8};
        Object[] result = Exercicio_Quatro.separarPares(arrayInicial);
        assertArrayEquals(expected, result);
    }

    @Test
    void separarImpares_923874_937() {
        int[] arrayInicial = {9, 2, 3, 8, 7, 4};
        Object[] expected = {9, 3, 7};
        Object[] result = Exercicio_Quatro.separarImpares(arrayInicial);
        assertArrayEquals(expected, result);
    }

    @Test
    void separarImpares_29837_937() {
        int[] arrayInicial = {2, 9, 8, 3, 7};
        Object[] expected = {9, 3, 7};
        Object[] result = Exercicio_Quatro.separarImpares(arrayInicial);
        assertArrayEquals(expected, result);
    }
}