package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio_TresTest {

    @Test
    void somaDeElementos_36781_25() {
        int[] array = {3, 6, 7, 8, 1};
        int expected = 25;
        int result = Exercicio_Tres.somaDeElementos(array);
        assertEquals(expected, result);
    }

    @Test
    void somaDeElementos_238746_25() {
        int[] array = {2, 3, 8, 7, 4, 6};
        int expected = 30;
        int result = Exercicio_Tres.somaDeElementos(array);
        assertEquals(expected, result);
    }
}