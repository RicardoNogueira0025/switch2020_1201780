package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio_UmTest {

    @Test
    void numeroDeDigitos_2314_4() {
        int numero = 2314;
        int expected = 4;
        int result = Exercicio_Um.numeroDeDigitos(numero);
        assertEquals(expected,result);
    }
    @Test
    void numeroDeDigitos_23862_5() {
        int numero = 23862;
        int expected = 5;
        int result = Exercicio_Um.numeroDeDigitos(numero);
        assertEquals(expected,result);
    }
}