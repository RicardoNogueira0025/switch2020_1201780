package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio_CatorzeTest {

    @Test
    void matrizERetangularFalse() {
        float[][] matriz = new float[5][];
        matriz[0] = new float[5];
        matriz[1] = new float[3];
        matriz[2] = new float[3];
        boolean result = Exercicio_Catorze.matrizERetangular(matriz);
        assertFalse(result);
    }

    @Test
    void matrizERetangularTrue() {
        float[][] matriz = new float[4][5];
        boolean result = Exercicio_Catorze.matrizERetangular(matriz);
        assertTrue(result);

    }

    @Test
    void matrizERetangularFalseQuadrado() {
        float[][] matriz = new float[4][4];
        boolean result = Exercicio_Catorze.matrizERetangular(matriz);
        assertFalse(result);
    }
}