package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio_OnzeTest {

    @Test
    void produtoEscalar_155() {
        int[] vetor1 = {8, 3, 2, 6, 4};
        int[] vetor2 = {9, 3, 7, 4, 9};
        int expected = 155;
        int result = Exercicio_Onze.produtoEscalar(vetor1, vetor2);
        assertEquals(expected, result);
    }

    @Test
    void produtoEscalar_95() {
        int[] vetor1 = {1, 2, 3, 4, 5};
        int[] vetor2 = {9, 8, 7, 6, 5};
        int expected = 95;
        int result = Exercicio_Onze.produtoEscalar(vetor1, vetor2);
        assertEquals(expected, result);
    }
}