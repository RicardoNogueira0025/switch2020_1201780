package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class Exercicio_DezaseteTest {

    @Test
    void produtoMatrizConstante_2_3() {
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}};
        int constante = 2;
        int[][] expected = {{2, 4, 6}, {8, 10, 12}};
        int[][] result = Exercicio_Dezasete.produtoMatrizConstante(matriz, constante);
        assertArrayEquals(expected, result);
    }

    @Test
    void produtoMatrizConstante_4_4() {
        int[][] matriz = {{1, 2, 3, 4}, {5, 6, 7, 8}, {2, 2, 2, 2}, {3, 3, 3, 3}};
        int constante = 3;
        int[][] expected = {{3, 6, 9, 12}, {15, 18, 21, 24}, {6, 6, 6, 6}, {9, 9, 9, 9}};
        int[][] result = Exercicio_Dezasete.produtoMatrizConstante(matriz, constante);
        assertArrayEquals(expected, result);
    }

    @Test
    void somaMatrizes_2_3() {
        int[][] matriz1 = {{1, 2, 3}, {4, 5, 6}};
        int[][] matriz2 = {{2, 4, 6}, {8, 10, 12}};
        int[][] expected = {{3, 6, 9}, {12, 15, 18}};
        int[][] result = Exercicio_Dezasete.somaMatrizes(matriz1, matriz2);
        assertArrayEquals(expected, result);
    }

    @Test
    void somaMatrizes_3_3() {
        int[][] matriz1 = {{1, 2, 3}, {4, 5, 6}, {7, 4, 5}};
        int[][] matriz2 = {{2, 4, 6}, {8, 10, 12}, {2, 4, 3,}};
        int[][] expected = {{3, 6, 9}, {12, 15, 18}, {9, 8, 8}};
        int[][] result = Exercicio_Dezasete.somaMatrizes(matriz1, matriz2);
        assertArrayEquals(expected, result);
    }

    @Test
    void produtoMatrizes_1() {
        int[][] matriz1 = {{1, 2, 3}, {-2, 0, 4}};
        int[][] matriz2 = {{2, 3}, {0, 1}, {-1, 4}};
        int[][] expected = {{-1, 17}, {-8, 10}};
        int[][] result = Exercicio_Dezasete.produtoMatrizes(matriz1, matriz2);
        assertArrayEquals(expected, result);
    }

    void produtoMatrizes_2() {
        int[][] matriz2 = {{1, 2, 3}, {-2, 0, 4}};
        int[][] matriz1 = {{2, 3}, {0, 1}, {-1, 4}};
        int[][] expected = {{-4, 4, 18}, {-2, 0, 4}, {-9, -2, 13}};
        int[][] result = Exercicio_Dezasete.produtoMatrizes(matriz1, matriz2);
        assertArrayEquals(expected, result);
    }
}