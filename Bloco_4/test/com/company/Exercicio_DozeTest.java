package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio_DozeTest {

    @Test
    void mesmoNumeroColunas_colunasDesiguais() {
        float[][] matriz = new float[5][];
        matriz[0] = new float[5];
        matriz[1] = new float[3];
        matriz[2] = new float[3];
        float expected = -1;
        float result = Exercicio_Doze.mesmoNumeroColunas(matriz);
        assertEquals(expected, result);
    }

    @Test
    void mesmoNumeroColunas_colunasIguais() {
        float[][] matriz = new float[5][3];
        float expected = 3;
        float result = Exercicio_Doze.mesmoNumeroColunas(matriz);
        assertEquals(expected, result);
    }
}