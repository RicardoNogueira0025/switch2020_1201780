package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio_QuinzeTest {

    @Test
    void elementoMenorValorMatriz_1() {
        float[][] matriz = new float[3][5];
        matriz[0] = new float[]{5, 3, 2, 6, 8};
        matriz[1] = new float[]{2, 6, 7, 2, 9};
        matriz[2] = new float[]{22, 12, 33, 1, 34};
        float expected = 1;
        float result = Exercicio_Quinze.elementoMenorValorMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    void elementoMenorValorMatriz_Negativo() {
        float[][] matriz = new float[3][5];
        matriz[0] = new float[]{5, 3, 2, 6, 8};
        matriz[1] = new float[]{2, 6, -7, 2, 9};
        matriz[2] = new float[]{22, 12, 33, 1, 34};
        float expected = -7;
        float result = Exercicio_Quinze.elementoMenorValorMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    void elementoMenorValorMatriz_0() {
        float[][] matriz = new float[3][5];
        matriz[0] = new float[]{5, 3, 2, 6, 8};
        matriz[1] = new float[]{2, 6, 7, 0, 9};
        matriz[2] = new float[]{22, 12, 33, 1, 34};
        float expected = 0;
        float result = Exercicio_Quinze.elementoMenorValorMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    void elementoMenorValorMatriz_EmptyTwo() {
        float[][] matriz = new float[3][5];
        matriz[0] = new float[]{5, 3, 2, 6};
        matriz[1] = new float[]{2, 6, 7, 2, 9};
        matriz[2] = new float[]{22, 12, 33, 54, 34};
        float expected = 2;
        float result = Exercicio_Quinze.elementoMenorValorMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    void elementoMaiorValorMatriz_34() {
        float[][] matriz = new float[3][5];
        matriz[0] = new float[]{5, 3, 2, 6, 8};
        matriz[1] = new float[]{2, 6, 7, 2, 9};
        matriz[2] = new float[]{22, 12, 33, 1, 34};
        float expected = 34;
        float result = Exercicio_Quinze.elementoMaiorValorMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    void elementoMaiorValorMatriz_333() {
        float[][] matriz = new float[3][5];
        matriz[0] = new float[]{5, 3, 2, 6, 8};
        matriz[1] = new float[]{2, 6, 7, 2, 9};
        matriz[2] = new float[]{22, 12, 333, 1, 34};
        float expected = 333;
        float result = Exercicio_Quinze.elementoMaiorValorMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    void mediaElementosMatriz_TrintaPontoTreze() {
        float[][] matriz = new float[3][5];
        matriz[0] = new float[]{5, 3, 2, 6, 8};
        matriz[1] = new float[]{2, 6, 7, 2, 9};
        matriz[2] = new float[]{22, 12, 333, 1, 34};
        float expected = 30.13f;
        float result = Exercicio_Quinze.mediaElementosMatriz(matriz);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void mediaElementosMatrizNaoComlpeta_TrintaQuatroPontoZeroSete() {
        float[][] matriz = new float[3][5];
        matriz[0] = new float[]{5, 3, 2, 6, 8};
        matriz[1] = new float[]{2, 6, 9};
        matriz[2] = new float[]{22, 12, 333, 1, 34};
        float expected = 34.07f;
        float result = Exercicio_Quinze.mediaElementosMatriz(matriz);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void produtoElementosMatriz_83980800() {
        int[][] matriz = new int[3][5];
        matriz[0] = new int[]{5, 3, 2, 6, 8};
        matriz[1] = new int[]{2, 6, 9, 1, 3};
        matriz[2] = new int[]{5, 3, 3, 1, 4};
        long expected = 83980800L;
        long result = Exercicio_Quinze.produtoElementosMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    void produtoElementosMatriz_Zero() {
        int[][] matriz = new int[3][5];
        matriz[0] = new int[]{5, 3, 2, 6, 8};
        matriz[1] = new int[]{2, 6, 0, 1, 3};
        matriz[2] = new int[]{5, 3, 3, 1, 4};
        long expected = 0L;
        long result = Exercicio_Quinze.produtoElementosMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    void produtoElementosMatriz_IncompletaNegativa() {
        int[][] matriz = new int[3][5];
        matriz[0] = new int[]{5, 3, 2, 6, 8};
        matriz[1] = new int[]{1, 3};
        matriz[2] = new int[]{-1, 4};
        long expected = -17280L;
        long result = Exercicio_Quinze.produtoElementosMatriz(matriz);
        assertEquals(expected, result);
    }

    @Test
    void elementosNaoRepetidosMatriz_1_2_3_4_5_6_8() {
        int[][] matriz = new int[3][5];
        matriz[0] = new int[]{5, 3, 2, 6, 8};
        matriz[1] = new int[]{1, 3};
        matriz[2] = new int[]{1, 4};
        int[] expected = {1, 2, 3, 4, 5, 6, 8};
        int[] result = Exercicio_Quinze.elementosNaoRepetidosMatriz(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosNaoRepetidosMatriz_Menos1_1_2_3_4_5_6_8_22_32() {
        int[][] matriz = new int[3][5];
        matriz[0] = new int[]{5, 3, 2, 6, 8};
        matriz[1] = new int[]{1, 3, 22, 32, 32};
        matriz[2] = new int[]{-1, 4, 22, 8};
        int[] expected = {-1, 1, 2, 3, 4, 5, 6, 8, 22, 32};
        int[] result = Exercicio_Quinze.elementosNaoRepetidosMatriz(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosPrimosNumaMatriz_2_3_5_7() {
        int[][] matriz = new int[3][5];
        matriz[0] = new int[]{5, 3, 2, 6, 8};
        matriz[1] = new int[]{1, 3, 22, 32, 32};
        matriz[2] = new int[]{1, 7, 22, 8};
        int[] expected = {2, 3, 5, 7};
        int[] result = Exercicio_Quinze.elementosPrimosNumaMatriz(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosPrimosNumaMatriz_2_3_5_11_13_17() {
        int[][] matriz = new int[3][5];
        matriz[0] = new int[]{5, 3, 2, 6, 8};
        matriz[1] = new int[]{11, 13, 22, 32, 32};
        matriz[2] = new int[]{1, 17, 22, 8};
        int[] expected = {2, 3, 5, 11, 13, 17};
        int[] result = Exercicio_Quinze.elementosPrimosNumaMatriz(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void diagonalPrincipalMatrizNull() {
        float[][] matriz = new float[3][5];
        matriz[0] = new float[]{5, 3, 2, 6, 8};
        matriz[1] = new float[]{11, 13, 22, 32, 32};
        matriz[2] = new float[]{1, 17, 22, 8};
        float[] result = Exercicio_Quinze.diagonalPrincipalMatriz(matriz);
        assertArrayEquals(null, result);
    }

    @Test
    void diagonalPrincipalMatrizQuadrada() {
        float[][] matriz = new float[3][3];
        matriz[0] = new float[]{5, 3, 2};
        matriz[1] = new float[]{11, 13, 22};
        matriz[2] = new float[]{1, 17, 22};
        float[] expected = {5, 13, 22};
        float[] result = Exercicio_Quinze.diagonalPrincipalMatriz(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void diagonalPrincipalMatrizRetangular() {
        float[][] matriz = new float[4][5];
        matriz[0] = new float[]{5, 3, 2, 6, 8};
        matriz[1] = new float[]{11, 13, 22, 32, 32};
        matriz[2] = new float[]{1, 17, 22, 8, 6};
        matriz[3] = new float[]{12, 34, 55, 14, 77};
        float[] expected = {5, 13, 22, 14};
        float[] result = Exercicio_Quinze.diagonalPrincipalMatriz(matriz);
        assertArrayEquals(expected, result);
    }

    ////////
    @Test
    void diagonalSecundariaMatrizNull() {
        float[][] matriz = new float[3][5];
        matriz[0] = new float[]{5, 3, 2, 6, 8};
        matriz[1] = new float[]{11, 13, 22, 32, 32};
        matriz[2] = new float[]{1, 17, 22, 8};
        float[] result = Exercicio_Quinze.diagonalSecundariaMatriz(matriz);
        assertArrayEquals(null, result);
    }

    @Test
    void diagonalSecundariaMatrizQuadrada() {
        float[][] matriz = new float[3][3];
        matriz[0] = new float[]{5, 3, 2};
        matriz[1] = new float[]{11, 13, 22};
        matriz[2] = new float[]{1, 17, 22};
        float[] expected = {2, 13, 1};
        float[] result = Exercicio_Quinze.diagonalSecundariaMatriz(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void diagonalSecundariaMatrizRetangular() {
        float[][] matriz = new float[4][5];
        matriz[0] = new float[]{5, 3, 2, 6, 8};
        matriz[1] = new float[]{11, 13, 22, 32, 32};
        matriz[2] = new float[]{1, 17, 22, 8, 6};
        matriz[3] = new float[]{12, 34, 55, 14, 77};
        float[] expected = {8, 32, 22, 34};
        float[] result = Exercicio_Quinze.diagonalSecundariaMatriz(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void matrizIdentidadeFalse() {
        float[][] matriz = new float[3][4];
        matriz[0] = new float[]{5, 3, 2, 8};
        matriz[1] = new float[]{11, 22, 32, 32};
        matriz[2] = new float[]{1, 22, 8, 6};
        boolean result = Exercicio_Quinze.matrizIdentidade(matriz);
        assertFalse(result);
    }

    @Test
    void matrizIdentidadeFalseNaoQuadrada() {
        float[][] matriz = new float[3][5];
        matriz[0] = new float[]{5, 3, 2, 8, 5};
        matriz[1] = new float[]{11, 22, 32, 32};
        matriz[2] = new float[]{1, 22, 8};
        boolean result = Exercicio_Quinze.matrizIdentidade(matriz);
        assertFalse(result);
    }

    @Test
    void matrizIdentidadeTrue() {
        float[][] matriz = new float[4][4];
        matriz[0] = new float[]{1, 0, 0, 0};
        matriz[1] = new float[]{0, 1, 0, 0};
        matriz[2] = new float[]{0, 0, 1, 0};
        matriz[3] = new float[]{0, 0, 0, 1};
        boolean result = Exercicio_Quinze.matrizIdentidade(matriz);
        assertTrue(result);
    }

    @Test
    void matrizIdentidadeFalseNonZeroOne() {
        float[][] matriz = new float[4][4];
        matriz[0] = new float[]{1, 0, 0, 0};
        matriz[1] = new float[]{0, 1, 0, 0};
        matriz[2] = new float[]{0, 0, 1, 0};
        matriz[3] = new float[]{0, 0, 1, 1};
        boolean result = Exercicio_Quinze.matrizIdentidade(matriz);
        assertFalse(result);
    }

    @Test
    void matrizIdentidadeFalseNonZeroTwo() {
        float[][] matriz = new float[4][4];
        matriz[0] = new float[]{1, 0, 0, 0};
        matriz[1] = new float[]{0, 1, 0, 0};
        matriz[2] = new float[]{0, 0, 1, 0};
        matriz[3] = new float[]{0, 0, 2, 1};
        boolean result = Exercicio_Quinze.matrizIdentidade(matriz);
        assertFalse(result);
    }

    @Test
    void inversoMatriz2_2() {
        float[][] matriz = {{4f, 7f}, {2f, 6f}};
        float[][] expected = {{0.6f, -0.7f}, {-0.2f, 0.4f}};
        float[][] result = Exercicio_Quinze.inversoMatriz(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void inversoMatriz2_2NaoQuadrada() {
        float[][] matriz = {{4f, 7f, 3f}, {2f, 6f, 5f}};
        float[][] result = Exercicio_Quinze.inversoMatriz(matriz);
        assertNull(result);
    }

    @Test
    void inversoMatriz2_2Determinante0() {
        float[][] matriz = {{3f, 4f}, {6f, 8f}};
        float[][] result = Exercicio_Quinze.inversoMatriz(matriz);
        assertNull(result);
    }

    @Test
    void originarIdentidade() {
        float[][] matriz = new float[4][4];
        float[][] expected = new float[4][4];
        expected[0] = new float[]{1, 0, 0, 0};
        expected[1] = new float[]{0, 1, 0, 0};
        expected[2] = new float[]{0, 0, 1, 0};
        expected[3] = new float[]{0, 0, 0, 1};
        float[][] result = Exercicio_Quinze.originarIdentidade(matriz);
        assertArrayEquals(expected, result);
    }
}