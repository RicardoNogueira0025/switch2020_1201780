package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio_SeteTest {

    @Test
    void multiplosNumIntervalo_4a10_3() {
        int minimo = 4;
        int maximo = 10;
        int multiplo = 3;
        int[] expected = {6,9};
        int[] result = Exercicio_Sete.multiplosNumIntervalo(minimo,maximo,multiplo);
        assertArrayEquals(expected,result);
    }

    @Test
    void multiplosNumIntervalo_1a15_5() {
        int minimo = 1;
        int maximo = 15;
        int multiplo = 5;
        int[] expected = {5,10,15};
        int[] result = Exercicio_Sete.multiplosNumIntervalo(minimo,maximo,multiplo);
        assertArrayEquals(expected,result);
    }

    @Test
    void verificarMultiplo_true() {
        int numero = 12;
        int multiplo = 6;
        boolean expected = true;
        boolean result = Exercicio_Sete.verificarMultiplo(numero,multiplo);
        assertEquals(expected,result);
    }

    @Test
    void verificarMultiplo_false() {
        int numero = 12;
        int multiplo = 5;
        boolean expected = false;
        boolean result = Exercicio_Sete.verificarMultiplo(numero,multiplo);
        assertEquals(expected,result);
    }
}