package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio_DezTest {

    @Test
    void menorValorDeArray_6() {
        int[] array = {63, 85, 8, 6, 23};
        int expected = 6;
        int result = Exercicio_Dez.menorValorDeArray(array);
        assertEquals(expected, result);
    }

    @Test
    void menorValorDeArray_353() {
        int[] array = {387, 353, 897, 379};
        int expected = 353;
        int result = Exercicio_Dez.menorValorDeArray(array);
        assertEquals(expected, result);
    }

    @Test
    void maiorValorDeArray_85() {
        int[] array = {63, 85, 8, 6, 23};
        int expected = 85;
        int result = Exercicio_Dez.maiorValorDeArray(array);
        assertEquals(expected, result);
    }

    @Test
    void maiorValorDeArray_4856() {
        int[] array = {4856, 3, 85};
        int expected = 4856;
        int result = Exercicio_Dez.maiorValorDeArray(array);
        assertEquals(expected, result);
    }

    @Test
    void mediaElementosArray_QuatroPontoOito() {
        int[] array = {2, 8, 3, 7, 4};
        double expected = 4.8;
        double result = Exercicio_Dez.mediaElementosArray(array);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void mediaElementosArray_SessentaPontoSeisSeisSeis() {
        int[] array = {92, 83, 7};
        double expected = 60.666;
        double result = Exercicio_Dez.mediaElementosArray(array);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void produtoElementosArray_2268() {
        int[] array = {18, 6, 21};
        int expected = 2268;
        int result = Exercicio_Dez.produtoElementosArray(array);
        assertEquals(expected, result);
    }

    @Test
    void produtoElementosArray_49715190() {
        int[] array = {73, 5, 23, 7, 846};
        int expected = 49715190;
        int result = Exercicio_Dez.produtoElementosArray(array);
        assertEquals(expected, result);
    }

    @Test
    void elementosNaoRepetidos_3_4_5_22() {
        int[] array = {22, 3, 4, 4, 5};
        int[] expected = {3, 4, 5, 22};
        int[] result = Exercicio_Dez.elementosNaoRepetidos(array);
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosNaoRepetidos_2_3_4_7_8() {
        int[] array = {7, 8, 3, 2, 2, 3, 4, 8, 7, 3, 2};
        int[] expected = {2, 3, 4, 7, 8};
        int[] result = Exercicio_Dez.elementosNaoRepetidos(array);
        assertArrayEquals(expected, result);
    }

    @Test
    void elementosNaoRepetidos_1_3_5() {
        int[] array = {1, 4, 3, 3, 4, 5, 5, 4, 5, 5, 3, 5, 5, 5, 5};
        int[] expected = {1,3,4,5};
        int[] result = Exercicio_Dez.elementosNaoRepetidos(array);
        assertArrayEquals(expected, result);
    }

    @Test
    void vetorInverso_5_4_4_3_22() {
        int[] array = {22, 3, 4, 4, 5};
        int[] expected = {5, 4, 4, 3, 22};
        int[] result = Exercicio_Dez.vetorInverso(array);
        assertArrayEquals(expected, result);
    }

    @Test
    void vetorInverso_4_4_3_1_2() {
        int[] array = {2, 1, 3, 4, 4};
        int[] expected = {4, 4, 3, 1, 2};
        int[] result = Exercicio_Dez.vetorInverso(array);
        assertArrayEquals(expected, result);
    }


    @Test
    void arrayDePrimos_2_3() {
        int[] array = {2, 1, 3, 4, 4};
        int[] expected = {2, 3};
        int[] result = Exercicio_Dez.arrayDePrimos(array);
        assertArrayEquals(expected, result);
    }

    @Test
    void arrayDePrimos_7_13_11() {
        int[] array = {4, 4, 6, 7, 13, 10, 11};
        int[] expected = {7, 13, 11};
        int[] result = Exercicio_Dez.arrayDePrimos(array);
        assertArrayEquals(expected, result);
    }


    @Test
    void ePrimo_1() {
        int numero = 1;
        boolean expected = false;
        boolean result = Exercicio_Dez.ePrimo(numero);
        assertEquals(expected, result);
    }

    @Test
    void ePrimo_2() {
        int numero = 2;
        boolean expected = true;
        boolean result = Exercicio_Dez.ePrimo(numero);
        assertEquals(expected, result);
    }

    @Test
    void ePrimo_3() {
        int numero = 3;
        boolean expected = true;
        boolean result = Exercicio_Dez.ePrimo(numero);
        assertEquals(expected, result);
    }

    @Test
    void ePrimo_4() {
        int numero = 4;
        boolean expected = false;
        boolean result = Exercicio_Dez.ePrimo(numero);
        assertEquals(expected, result);
    }

    @Test
    void ePrimo_8() {
        int numero = 8;
        boolean expected = false;
        boolean result = Exercicio_Dez.ePrimo(numero);
        assertEquals(expected, result);
    }
}