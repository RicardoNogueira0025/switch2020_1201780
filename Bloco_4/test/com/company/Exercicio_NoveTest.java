package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio_NoveTest {

    @Test
    void verificarCapicua_12345_false() {
        int numero = 12345;
        boolean result = Exercicio_Nove.verificarCapicua(numero);
        assertFalse(result);
    }

    @Test
    void verificarCapicua_1331_true() {
        int numero = 1331;
        boolean result = Exercicio_Nove.verificarCapicua(numero);
        assertTrue(result);
    }

    @Test
    void verificarCapicua_13531_true() {
        int numero = 13531;
        boolean result = Exercicio_Nove.verificarCapicua(numero);
        assertTrue(result);
    }
}