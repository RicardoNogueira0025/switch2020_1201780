package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class Exercicio_SeisTest {

    @Test
    void darArrayLimitado_6_123_2_123() {
        int[] arrayInicial = {6, 123, 2, 123, 44, 5, 2};
        int limite = 4;
        int[] expected = {6, 123, 2, 123};
        int[] result = Exercicio_Seis.darArrayLimitado(arrayInicial, limite);
        assertArrayEquals(expected, result);
    }

    @Test
    void darArrayLimitado_1_2_3_4_5_6_7() {
        int[] arrayInicial = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int limite = 7;
        int[] expected = {1,2,3,4,5,6,7};
        int[] result = Exercicio_Seis.darArrayLimitado(arrayInicial, limite);
        assertArrayEquals(expected, result);
    }

}