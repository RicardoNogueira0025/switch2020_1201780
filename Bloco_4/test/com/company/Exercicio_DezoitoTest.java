package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio_DezoitoTest {

    @Test
    void gerarMatrizMascara_3_3() {
        char[][] matriz = {{'c', 'o', 'd'}, {'s', 'd', 'f'}, {'s', 'e', 'r'}};
        int[][] expected = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
        int[][] result = Exercicio_Dezoito.gerarMatrizMascara(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void gerarMatrizMascara_10_10() {
        char[][] matriz = {{'c', 'o', 'd', 'c', 'o', 'd', 'c', 'o', 'd', 'c'}, {'s', 'd', 'f', 's', 'd', 'f', 's', 'd', 'f', 'g'}, {'s', 'e', 'r', 's', 'e', 'r', 's', 'e', 'r', 'j'}, {'c', 'o', 'd', 'c', 'o', 'd', 'c', 'o', 'd', 'c'}, {'s', 'd', 'f', 's', 'd', 'f', 's', 'd', 'f', 'g'}, {'s', 'e', 'r', 's', 'e', 'r', 's', 'e', 'r', 'j'}, {'c', 'o', 'd', 'c', 'o', 'd', 'c', 'o', 'd', 'c'}, {'s', 'd', 'f', 's', 'd', 'f', 's', 'd', 'f', 'g'}, {'s', 'e', 'r', 's', 'e', 'r', 's', 'e', 'r', 'j'}, {'s', 'e', 'r', 's', 'e', 'r', 's', 'e', 'r', 'j'}};
        int[][] expected = new int[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                expected[i][j] = 0;
            }
        }
        int[][] result = Exercicio_Dezoito.gerarMatrizMascara(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void obterCoordenadasDoUm_1_2() {
        int[][] matriz = {{0, 0, 0}, {0, 0, 1}, {0, 0, 0}};
        int[] expected = {1, 2};
        int[] result = Exercicio_Dezoito.obterCoordenadasDoUm(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void obterCoordenadasDoUm_2_1() {
        int[][] matriz = {{0, 0, 0}, {0, 0, 0}, {0, 1, 0}};
        int[] expected = {2, 1};
        int[] result = Exercicio_Dezoito.obterCoordenadasDoUm(matriz);
        assertArrayEquals(expected, result);
    }

    @Test
    void verificarSePalavraExiste_True() {
        String palavraEscolhida = "QUEIJO";
        String[] listaDePalavras = {"FRAMBOESA", "PICANHA", "FONDUE", "QUEIJO"};
        boolean result = Exercicio_Dezoito.verificarSePalavraExiste(palavraEscolhida, listaDePalavras);
        assertTrue(result);

    }

    @Test
    void verificarSePalavraExiste_False() {
        String palavraEscolhida = "PUDIM";
        String[] listaDePalavras = {"FRAMBOESA", "PICANHA", "FONDUE", "QUEIJO"};
        boolean result = Exercicio_Dezoito.verificarSePalavraExiste(palavraEscolhida, listaDePalavras);
        assertFalse(result);

    }

    @Test
    void obterPalavra() {
        char[][] sopa = {{'P', 'E', 'I', 'T', 'O', 'F', 'U', 'O'}, {'R', 'O', 'T', 'U', 'L', 'I', 'R', 'E'},
                {'E', 'S', 'O', 'G', 'R', 'A', 'L', 'O'}, {'F', 'U', 'A', 'R', 'I', 'N', 'E', 'I'},
                {'A', 'G', 'E', 'P', 'A', 'L', 'O', 'R'}, {'D', 'M', 'S', 'T', 'O', 'N', 'I', 'A'}, {'O', 'T', 'A', 'C', 'O', 'B', 'A', 'R'},
                {'V', 'I', 'F', 'E', 'U', 'T', 'P', 'X'}};
        //Esquerda-Direita
        int[] coordenada1 = {0, 0};
        int[] coordenada2 = {0, 4};
        String palavraEscolhida = "PEITO";
        String result = Exercicio_Dezoito.obterPalavra(sopa, coordenada1, coordenada2);
        assertTrue(result.equalsIgnoreCase(palavraEscolhida));
        // Direita-Esquerda
        coordenada1 = new int[]{0, 4};
        coordenada2 = new int[]{0, 0};
        palavraEscolhida = "OTIEP";
        result = Exercicio_Dezoito.obterPalavra(sopa, coordenada1, coordenada2);
        assertTrue(result.equalsIgnoreCase(palavraEscolhida));
        //Cima-Baixo
        coordenada1 = new int[]{3, 0};
        coordenada2 = new int[]{6, 0};
        palavraEscolhida = "FADO";
        result = Exercicio_Dezoito.obterPalavra(sopa, coordenada1, coordenada2);
        assertTrue(result.equalsIgnoreCase(palavraEscolhida));
        //Baixo-Cima
        coordenada1 = new int[]{6, 0};
        coordenada2 = new int[]{3, 0};
        palavraEscolhida = "ODAF";
        result = Exercicio_Dezoito.obterPalavra(sopa, coordenada1, coordenada2);
        assertTrue(result.equalsIgnoreCase(palavraEscolhida));
        //Diagonal Ascendente Direita
        coordenada1 = new int[]{2, 5};
        coordenada2 = new int[]{0, 7};
        palavraEscolhida = "ARO";
        result = Exercicio_Dezoito.obterPalavra(sopa, coordenada1, coordenada2);
        assertTrue(result.equalsIgnoreCase(palavraEscolhida));
        //Diagonal Descendente Esquerda
        coordenada1 = new int[]{0, 7};
        coordenada2 = new int[]{2, 5};
        palavraEscolhida = "ORA";
        result = Exercicio_Dezoito.obterPalavra(sopa, coordenada1, coordenada2);
        assertTrue(result.equalsIgnoreCase(palavraEscolhida));
        //Diagonal Descendente Direita
        coordenada1 = new int[]{2, 1};
        coordenada2 = new int[]{5, 4};
        palavraEscolhida = "SAPO";
        result = Exercicio_Dezoito.obterPalavra(sopa, coordenada1, coordenada2);
        assertTrue(result.equalsIgnoreCase(palavraEscolhida));
        //Diagonal Ascendente Esquerda
        coordenada1 = new int[]{5, 4};
        coordenada2 = new int[]{2, 1};
        palavraEscolhida = "OPAS";
        result = Exercicio_Dezoito.obterPalavra(sopa, coordenada1, coordenada2);
        assertTrue(result.equalsIgnoreCase(palavraEscolhida));
    }

    @Test
    void verificarDirecao_Esquerda_Direita() {
        int[] coordenada1 = {0, 0};
        int[] coordenada2 = {0, 4};
        int expected = 1;
        int result = Exercicio_Dezoito.verificarDirecao(coordenada1, coordenada2);
        assertEquals(expected, result);
    }

    @Test
    void verificarDirecao_Direita_Esquerda() {
        int[] coordenada1 = {0, 4};
        int[] coordenada2 = {0, 0};
        int expected = 2;
        int result = Exercicio_Dezoito.verificarDirecao(coordenada1, coordenada2);
        assertEquals(expected, result);
    }

    @Test
    void verificarDirecao_Cima_Baixo() {
        int[] coordenada1 = {0, 0};
        int[] coordenada2 = {5, 0};
        int expected = 3;
        int result = Exercicio_Dezoito.verificarDirecao(coordenada1, coordenada2);
        assertEquals(expected, result);
    }

    @Test
    void verificarDirecao_Baixo_Cima() {
        int[] coordenada1 = {5, 0};
        int[] coordenada2 = {0, 0};
        int expected = 4;
        int result = Exercicio_Dezoito.verificarDirecao(coordenada1, coordenada2);
        assertEquals(expected, result);
    }

    @Test
    void verificarDirecao_Diagonal_Descendente_Direito() {
        int[] coordenada1 = {0, 0};
        int[] coordenada2 = {5, 5};
        int expected = 5;
        int result = Exercicio_Dezoito.verificarDirecao(coordenada1, coordenada2);
        assertEquals(expected, result);
    }

    @Test
    void verificarDirecao_Diagonal_Ascendente_Direito() {
        int[] coordenada1 = {5, 5};
        int[] coordenada2 = {3, 7};
        int expected = 6;
        int result = Exercicio_Dezoito.verificarDirecao(coordenada1, coordenada2);
        assertEquals(expected, result);
    }

    @Test
    void verificarDirecao_Diagonal_Descendente_Esquerda() {
        int[] coordenada1 = {0, 7};
        int[] coordenada2 = {5, 4};
        int expected = 7;
        int result = Exercicio_Dezoito.verificarDirecao(coordenada1, coordenada2);
        assertEquals(expected, result);
    }

    @Test
    void verificarDirecao_Diagonal_Ascendente_Esquerda() {
        int[] coordenada1 = {5, 5};
        int[] coordenada2 = {3, 3};
        int expected = 8;
        int result = Exercicio_Dezoito.verificarDirecao(coordenada1, coordenada2);
        assertEquals(expected, result);
    }


    @Test
    void removerPalavraDaLista_Primeira() {
        String palavraEscolhida = "FRAMBOESA";
        String[] listaDePalavras = {"FRAMBOESA", "PICANHA", "FONDUE", "QUEIJO"};
        String[] expected = {"PICANHA", "FONDUE", "QUEIJO"};
        String[] result = Exercicio_Dezoito.removerPalavraDaLista(palavraEscolhida, listaDePalavras);
        assertArrayEquals(expected, result);
    }

    @Test
    void removerPalavraDaLista_Meio() {
        String palavraEscolhida = "FONDUE";
        String[] listaDePalavras = {"FRAMBOESA", "PICANHA", "FONDUE", "QUEIJO"};
        String[] expected = {"FRAMBOESA", "PICANHA", "QUEIJO"};
        String[] result = Exercicio_Dezoito.removerPalavraDaLista(palavraEscolhida, listaDePalavras);
        assertArrayEquals(expected, result);
    }

    @Test
    void removerPalavraDaLista_Ultima() {
        String palavraEscolhida = "QUEIJO";
        String[] listaDePalavras = {"FRAMBOESA", "PICANHA", "FONDUE", "QUEIJO"};
        String[] expected = {"FRAMBOESA", "PICANHA", "FONDUE"};
        String[] result = Exercicio_Dezoito.removerPalavraDaLista(palavraEscolhida, listaDePalavras);
        assertArrayEquals(expected, result);
    }
}