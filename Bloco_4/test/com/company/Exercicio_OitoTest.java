package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class Exercicio_OitoTest {

    @Test
    void arrayDeMultiplosComuns_4a12_6e12() {
        int minimo = 4;
        int maximo = 12;
        int[] multiplos = {2, 3};
        int[] expected = {6, 12};
        int[] result = Exercicio_Oito.arrayDeMultiplosComuns(minimo, maximo, multiplos);
        assertArrayEquals(expected, result);
    }

    @Test
    void arrayDeMultiplosComuns_1a10_15() {
        int minimo = 1;
        int maximo = 15;
        int[] multiplos = {3, 5};
        int[] expected = {15};
        int[] result = Exercicio_Oito.arrayDeMultiplosComuns(minimo, maximo, multiplos);
        assertArrayEquals(expected, result);
    }
}