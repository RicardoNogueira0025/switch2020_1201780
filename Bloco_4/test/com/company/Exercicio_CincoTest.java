package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Exercicio_CincoTest {

    @Test
    void somaDePares_947334_8() {
        int numero = 947334;
        int expected = 8;
        int result = Exercicio_Cinco.somaDePares(numero);
        assertEquals(expected, result);
    }

    @Test
    void somaDePares_827536_16() {
        int numero = 8275036;
        int expected = 16;
        int result = Exercicio_Cinco.somaDePares(numero);
        assertEquals(expected, result);
    }

    @Test
    void somaDeImpares_947334_24() {
        int numero = 947334;
        int expected = 22;
        int result = Exercicio_Cinco.somaDeImpares(numero);
        assertEquals(expected, result);
    }

    @Test
    void somaDeImpares_827536_15() {
        int numero = 8275036;
        int expected = 15;
        int result = Exercicio_Cinco.somaDeImpares(numero);
        assertEquals(expected, result);
    }
}