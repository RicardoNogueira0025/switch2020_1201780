package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Exercicio_DoisTest {

    @Test
    void arrayDeDigitos_33781() {
        int numero = 36781;
        int[] expected = {3,6,7,8,1};
        int[] result = Exercicio_Dois.arrayDeDigitos(numero);
        assertArrayEquals(expected,result);
    }

    @Test
    void arrayDeDigitos_222() {
        int numero = 222;
        int[] expected = {2,2,2};
        int[] result = Exercicio_Dois.arrayDeDigitos(numero);
        assertArrayEquals(expected,result);
    }
}