package Previous_Exercises;

public class Cilindro {
    private double raio = -1;
    private double altura = -1;

    private Cilindro() {

    }

   /* public double getRaio(){
        return raio;
    }*/

    public Cilindro(double raio, double altura) {
        if (raio < 0 || altura < 0) {
            throw new IllegalArgumentException("Não pode ter valores negativos");
        }
        this.raio = raio;
        this.altura = altura;
    }

    public double obterVolume() {
        //double areaBase = obterAreaBase(); exatamente igual à seguinte
        double areaBase = this.obterAreaBase();
        double volume = areaBase * altura;
        return volume;
    }

    private double obterAreaBase() {
        return Math.PI * (raio * raio);
    }
}
