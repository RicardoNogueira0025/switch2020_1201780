package Previous_Exercises;//import java.util.Scanner;

public class BlocoUm {

    public static void main(String[] args) {

        Pessoa bettencourt = new Pessoa(1.72, 2);
        Edificio empireState = new Edificio(200);
        System.out.println(empireState.calcularAltura(bettencourt));

        /*
        Scanner ler = new Scanner(System.in);
        boolean choice;

        while (choice = true) {
            System.out.println("Que exercicio correr?");
            int opcao = ler.nextInt();
            if (opcao == 3) {
                exercicio3();
            } else if (opcao == 4) {
                exercicio4();
            } else if (opcao == 5) {
                exercicio5();
            } else if (opcao == 6) {
                exercicio6();
            } else if (opcao == 7) {
                exercicio7();
            } else if (opcao == 8) {
                exercicio8();
            } else if (opcao == 9) {
                exercicio9();
            } else if (opcao == 10) {
                exercicio10();
            } else if (opcao == 11) {
                exercicio11();
            } else if (opcao == 12) {
                exercicio12();
            } else {
                exercicio13();
            }
            System.out.println("---------------");
            System.out.println("Correr mais um exercício? Y/N");
            String opcao2 = ler.next();

            if (opcao2.equalsIgnoreCase("y")) {

            } else {
                choice = false;
                break;
            }
        }
    */
        /*
        exercicio3processamento(2,3);
        exercicio4processamento(2);
        exercicio5processamento(2);
        exercicio6processamento(2,2,4);
        exercicio7processamento(23,222,22);
        exercicio8processamento(23,21,21);
        exercicio9processamento(2,3);
        exercicio10processamento(2,2);
        exercicio11processamento(3);
        exercicio12processamento(21);
        exercicio13processamento(12,22);
        */
    }

    /*
    public static void exercicio3() {
        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 3");
        //Preparação para leitura
        Scanner ler = new Scanner(System.in);
        //Leitura de Dados
        System.out.println("Insira o raio da base:");
        double base = ler.nextDouble();
        System.out.println("Insira a altura do cilindro:");
        double altura = ler.nextDouble();
        //Processamento
        double capacidade = exercicio3processamento(base, altura);
        //Apresentação de dados
        System.out.println("A capacidade do cilindro é de: " + String.format("%.2f", capacidade) + " litros.");
    }
    */
    public static double exercicio3processamento(double raioBase, double alturaCilindro) {
            /*
            ALGORITMO
            Conjunto de Dados:
            double: raio da base, altura, volumecilindro
            Entrada de dados:
            ler(raio da base, altura)
            Processamento:
            - area da base = pi * (raio da base * raio da base)
            - volume do cilindro = area da base * altura
            - capacidadeLitros = volume do cilindro * 1000
            Saída de dados:
            - Escrever("O cilindro tem capacidade para" + capacidadeLitros + "litros")
             */
        double areaBase = Math.PI * (raioBase * raioBase);
        double volumeCilindro = areaBase * alturaCilindro;
        return volumeCilindro * 1000;
    }
    /*
    public static void exercicio4() {
        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 4");
        //Preparação para leitura
        Scanner ler = new Scanner(System.in);
        //Leitura de Dados
        System.out.println("Insira o tempo em segundos entre o relampago e o trovão:");
        double tempo = ler.nextDouble();
        //Processamento
        double distancia = exercicio4processamento(tempo);
        System.out.println("A trovoada está a " + String.format("%.2f", distancia) + " Km de distância");
    }
    */

    public static double exercicio4processamento(double segundosLuzSom) {
        /*
        ALGORITMO
        Conjunto de dados:
        double: segundosentreLuzeSom, velocidadeSom, distanciaTrovoada
        Entrada de dados:
        ler(segundos)
        Processamento:
        distancioTrovoada = velociade do som (340 m/s) * segundos
        Saída de dados:
        Escrever("A trovoada está a "+ distanciaTrovoada *" metros de distância")
         */
        final int VELOCIDADE_DO_SOM = 340;
        return segundosLuzSom * VELOCIDADE_DO_SOM / 1000;
    }

    /*
    public static void exercicio5() {
        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 5");
        //Preparação para leitura
        Scanner ler = new Scanner(System.in);
        //Leitura de Dados
        System.out.println("Insira o tempo em segundos da queda da pedra:");
        double tempo = ler.nextDouble();
        //Processamento
        double altura = exercicio5processamento(tempo);
        //Apresentação de Dados
        System.out.println("O prédio tem " + String.format("%.2f", altura) + " metros de altura.");
    }
    */
    public static double exercicio5processamento(double segundosQueda) {
        /*
        ALGORITMO
        Conjunto de Dados:
        double: segundos da queda, alturaPredio
        Entrada de dados:
        ler(segundos)
        Processamento:
        alturaPredio = [9,8*(segundos*segundos)]/2
        Saída de Dados:
        Escrever("A altura do prédio é de "+ alturPredio +" metros")
         */
        final double ACELARACAO_GRAVITICA = 9.8;
        return ACELARACAO_GRAVITICA * (segundosQueda * segundosQueda) / 2;
    }

    /*
    public static void exercicio6() {
        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 6");
        //Preparação para leitura
        Scanner ler = new Scanner(System.in);
        //Leitura de Dados
        System.out.println("Insira a altura da pessoa:");
        double alturaPessoa = ler.nextDouble();
        System.out.println("Insira o comprimento da sombra da pessoa:");
        double sombraPessoa = ler.nextDouble();
        System.out.println("Insira o comprimento da sombra do edifício:");
        double sombraEdificio = ler.nextDouble();
        //Processamento
        double alturaEdificio = exercicio6processamento(alturaPessoa, sombraPessoa, sombraEdificio);
        //Apresentação de dados
        System.out.println("O edifício tem " + String.format("%.2f", alturaEdificio) + "metros de altura.");
    }
    */
    public static double exercicio6processamento(double alturaPessoa, double sombraPessoa, double sombraEdificio) {
        /*
        ALGORITMO
        Conjunto de Dados:
        double: alturaPessoa, sombraEdificio, sombraPessoa, alturaEdificio
        Entrada de dados:
        ler(sombraEdificio)
        ler(sombraPessoa)
        ler8alturaPessoa)
        Processamento:
        alturaEdificio = sombraEdifico*alturaPessoa/sombraPessoa
        Saída de Dados:
        Escrever("O prédio tem " +alturaPredio+" metros de altura")
         */
        return sombraEdificio * alturaPessoa / sombraPessoa;
    }

    /*
    public static void exercicio7() {
        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 7");
        //Preparação para leitura
        Scanner ler = new Scanner(System.in);
        //Leitura de Dados
        System.out.println("Insira quanto o Manel andou, em metros:");
        double deslocamentoManel = ler.nextDouble();
        System.out.println("Insira quanto tempo, em segundos, andou o Manel:");
        double tempoManel = ler.nextDouble();
        System.out.println("Insira quanto tempo, em segundos, andou o Zé:");
        double tempoZe = ler.nextDouble();
        //Processamento
        double deslocamentoZe = exercicio7processamento(deslocamentoManel, tempoManel, tempoZe);
        //Apresentação de dados
        System.out.println("O Zé andou " + String.format("%.2f", deslocamentoZe) + " Km.");

    }
    */
    public static double exercicio7processamento(double deslocamentoManel, double tempoManel, double tempoZe) {
        /*
        ALGORITMO
        Conjunto de Dados:
        double: deslocamentoManel, tempoManel, tempoZe, deslocamentoZe, VelocidadeZe
        Entrada de dados:
        ler(deslocamentoManuel)
        ler(tempoManuel)
        ler(tempoZe)
        Processamento:
        velocidadeManelZe = deslocamentoManel/tempoManel
        deslocamentoZe = velocidadeManelZe*tempoZe
        Saída de Dados:
        Escrever("O Ze percorreu "+distanciaZe+" metros")
         */
        return deslocamentoManel / tempoManel * tempoZe / 1000;
    }

    /*
    public static void exercicio8() {
        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 8");
        //Preparação para leitura
        Scanner ler = new Scanner(System.in);
        //Leitura de Dados
        System.out.println("Qual o comprimento, em metros, do primeiro cabo?");
        double compCabo1 = ler.nextDouble();
        System.out.println("Qual o comprimento, em metros, do segundo cabo?");
        double compCabo2 = ler.nextDouble();
        System.out.println("Qual a amplitude do ângulo formado pelos dois cabos?");
        double angulo = ler.nextDouble();
        //Processamento
        double distanciaPessoas = exercicio8processamento(compCabo1, compCabo2, angulo);
        //Apresentação de dados
        System.out.println("Os dois homens estão a " + String.format("%.2f", distanciaPessoas) + " metros de distância um do outro.");
    }
    */
    public static double exercicio8processamento(double comprimentoCabo1, double comprimentoCabo2, double angulo) {
        /*
        ALGORITMO
        Entrada de dados:
        double: comprimetoCabo1, comprimentoCabo2, angulo
        Processamento:
        quadradoDaDistancia = (comprimentoCabo1*comprimentoCabo1)+(comprimentoCabo2*comprimentoCaboo2) - (2*comprimentoCabo1*comprimentoCabo2*Coseno(angulo)
        distancia = √quadradoDaDistancia
         */

        double quadradoDistancia = comprimentoCabo1 * comprimentoCabo1 + comprimentoCabo2 * comprimentoCabo2 - 2 * comprimentoCabo1 * comprimentoCabo2 * Math.cos(Math.toRadians(angulo));
        return Math.sqrt(quadradoDistancia);

    }

    /*
    public static void exercicio9() {

        ALGORITMO
        Entrada de dados:
        double: comprimento, largura,
        Processamento:
        Perimetro = (comprimento*2) + (largura*2)
        Saida de dados:
        mostrar(O perimetro do retângulo é + perimetro)

        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 9");
        //Preparação para leitura
        Scanner ler = new Scanner(System.in);
        //Leitura de Dados
        System.out.println("Qual o comprimento do retângulo?");
        double comprimento = ler.nextDouble();
        System.out.println("Qual a largura do retângulo?");
        double largura = ler.nextDouble();
        //Processamento
        double perimetro = exercicio9processamento(comprimento, largura);
        //Saída de dados
        System.out.println("O perímetro do retângulo é " + perimetro);

    }
    */
    public static double exercicio9processamento(double comprimento, double largura) {

        return (2 * comprimento) + (2 * largura);
    }

    /*
    public static void exercicio10() {

        ALGORITMO
        Entrada de dados:
        double: cateto1, cateto2,
        Processamento:
        hipotenusa= Raiz quadrada(cateto1^2 + cateto2^2)
        Saida de dados:
        mostrar(a hipotenusa tem o comprimento de + hipotenusa)

        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 10");
        //Preparação para leitura
        Scanner ler = new Scanner(System.in);
        //Leitura de Dados
        System.out.println("Qual o comprimento do primeiro cateto?");
        double cateto1 = ler.nextDouble();
        System.out.println("Qual o comprimento do segundo cateto?");
        double cateto2 = ler.nextDouble();
        //Processamento
        double hipotenusa = exercicio10processamento(cateto1, cateto2);
        //Saída de dados
        System.out.println("A hipotenusa mede " + hipotenusa);
    }
    */
    public static double exercicio10processamento(double cateto1, double cateto2) {

        return Math.sqrt(Math.pow(cateto1, 2) + Math.pow(cateto2, 2));
    }

    /*
    public static void exercicio11() {

        ALGORITMO
        Entrada de dados:
        double: x,
        Processamento:
        resultado= x^2-3x+1
        Saida de dados:
        mostrar(o resultado é + resultado)

        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 11");
        //Preparação para leitura
        Scanner ler = new Scanner(System.in);
        //Leitura de Dados
        System.out.println("Insere o valor de x");
        double valorX = ler.nextDouble();
        //Processamento
        double resultado = exercicio11processamento(valorX);
        //Saída de dados
        System.out.println("O resultado é " + resultado);
    }
    */
    public static double exercicio11processamento(double valorX) {

        return Math.pow(valorX, 2) - (3 * valorX) + 1;
    }

    /*
    public static void exercicio12() {

        ALGORITMO
        Entrada de dados:
        double: grausCelsius,
        Processamento:
        grausFahrenheit = 32 + (9/5)*grausCelsius
        Saida de dados:
        mostrar(A temperatura em Celsius é "+grausFahrenheit)

        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 12");
        //Preparação para leitura
        Scanner ler = new Scanner(System.in);
        //Leitura de Dados
        System.out.println("Insere o valor dos graus em Celsius");
        double grausCelsius = ler.nextDouble();
        //Processamento
        double grausFahrenheit = exercicio12processamento(grausCelsius);
        //Saidad de dados
        System.out.println(grausCelsius + " graus Celsius equivalem a " + grausFahrenheit + " graus Fahrenheit");


    }
    */
    public static double exercicio12processamento(double grausCelsius) {
        return 32 + 1.8 * grausCelsius;
    }

    /*
    public static void exercicio13() {

        ALGORITMO
        Entrada de dados:
        int: horas, minutos
        Processamento:
        tempoDecorrido= horas*60 + minutos
        Saida de dados:
        mostrar(passaram +tempoDecorrido+ minutos desde o inicio)

        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 13");
        //Preparação para leitura
        Scanner ler = new Scanner(System.in);
        //Leitura de Dados
        System.out.println("Quantas horas passaram?");
        int horas = ler.nextInt();
        System.out.println("Quantos minutos passaram)");
        int minutos = ler.nextInt();
        //Processamento
        int tempoDecorrido = exercicio13processamento(horas, minutos);
        //Saida de dados
        System.out.println("Passaram " + tempoDecorrido + " minutos desde a Hora 0.");
    }
    */
    public static int exercicio13processamento(int horas, int minutos) {

        return horas * 60 + minutos;
    }

}
