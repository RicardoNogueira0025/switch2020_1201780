package Previous_Exercises;

public class Pessoa {
    private double altura;
    private double comprimentoSombra;

    public Pessoa(double altura, double comprimentoSombra) {
        if (altura < 0 || comprimentoSombra < 0) {
            throw new IllegalArgumentException("Não pode ter valores negativos");
        }
        this.altura = altura;
        this.comprimentoSombra = comprimentoSombra;
    }

    public double getAltura() {
        return altura;
    }

    public double getComprimentoSombra() {
        return comprimentoSombra;
    }
}
