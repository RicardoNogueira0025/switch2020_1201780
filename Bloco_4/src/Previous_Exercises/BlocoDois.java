package Previous_Exercises;//import java.util.Scanner;

public class BlocoDois {
    //Preparação para leitura


    public static void main(String[] args) {
        /*
        getCubeVolume(26);
        exercicio1processamento(12,30,13,30,18,40);
        getDigito1(233);
        getDigito2(233);
        getDigito3(233);
        getDistanciaPlanos(3,4,4,6);
        exercicio4(0);
        exercicio4(-3);
        exercicio4(2);
        getCubeSize(39);
        getSegundosLeft(2344);
        getMinutos(24445);
        getHoras(23345);
        exercicio7custototal(23,3,44,32);
        getPintores(24);
        exercicio7customaoobra(21,44);
        exercicio7custotinta(33,34,12);
        exercicio8XmultiplodeY(4,3);
        exercicio8YmultiploX(4,8);
        exercicio9(222);
        exercicio10(23);
        getAprovados(12,2,4,5,7);
        exercicio12(0.4);
        exercicio13Preco(2,3,4);
        exercicio13Tempo(2,4,5);
        exercicio14(23,2.6,2,4.5,3.2);
        exercicio15(2,4,2);
        exercicio16(2,78,100);
        exercicio18(12,23,12,222);
        exercicio17(12,22,11,22);
        exercicio19(2);
        exercicio20(true,'a',2);
    */
    }
    /*
    public static void exercicio1() {

        a) O algoritmo permite calcular a média de 3 notas, tendo em consideração o peso que cada uma tem para a média.
        b) No caderno

        Scanner ler = new Scanner(System.in);
        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 1");
        //Leitura de Dados
        System.out.println("Insira a primeira nota:");
        double nota1 = ler.nextDouble();
        System.out.println("Insira o peso da primeira nota:");
        double peso1 = ler.nextDouble();
        System.out.println("Insira a segunda nota:");
        double nota2 = ler.nextDouble();
        System.out.println("Insira o peso da segunda nota:");
        double peso2 = ler.nextDouble();
        System.out.println("Insira a terceira nota:");
        double nota3 = ler.nextDouble();
        System.out.println("Insira o peso da terceira nota:");
        double peso3 = ler.nextDouble();
        //Processamento
        double media = exercicio1processamento(nota1, peso1, nota2, peso2, nota3, peso3);
        //Saida de dados
        if (media >= 8) {
            System.out.println("O aluno cumpre a média requerida.");
        } else {
            System.out.println("O aluno não cumpre a média requerida.");
        }

    }
    */


    public static double exercicio1processamento(double nota1, double peso1, double nota2, double peso2, double nota3, double peso3) {

        return ((nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3));
    }
    /*
    public static void exercicio2() {

        a) O algoritmo identifica se o numero inserido tem 3 digitos. Caso tenha, ele separa os 3 digitos,
        calculando o resto  de 10 da divisao do primeiro por 100, o resto de 10 da divisao do segundo por 10, e o resto de 10 do terceiro
        b) No caderno

        Scanner ler = new Scanner(System.in);
        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 2");
        //Leitura de Dados
        System.out.println("Insira um número de 3 digitos:");
        int numero = ler.nextInt();
        if (numero % 2 == 0) {
            System.out.println("O número " + numero + " é par.");
        } else {
            System.out.println("O número " + numero + " é impar.");
        }
        if (numero >= 100 || numero <= 999) {
            //Processamento
            int digito3 = getDigito3(numero);
            int digito2 = getDigito2(numero);
            int digito1 = getDigito1(numero);
            //Saida de dados
            System.out.println(digito1 + " " + digito2 + " " + digito3);

        } else
            System.out.println("Esse numero não tem 3 digitos.");
    }
    */

    public static int getDigito1(int numero) {

        return (numero / 100) % 10;
    }

    public static int getDigito2(int numero) {
        return (numero / 10) % 10;
    }

    public static int getDigito3(int numero) {
        return numero % 10;
    }
/*
    public static void exercicio3() {
        Scanner ler = new Scanner(System.in);
        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 3");
        //Leitura de Dados
        System.out.println("Insira a coordenada x do plano 1");
        int x1 = ler.nextInt();
        System.out.println("Insira a coordenada y do plano 1");
        int y1 = ler.nextInt();
        System.out.println("Insira a coordenada x do plano 2");
        int x2 = ler.nextInt();
        System.out.println("Insira a coordenada y do plano 2");
        int y2 = ler.nextInt();
        //Processamento
        double distanciaPlanos = getDistanciaPlanos(x1, y1, x2, y2);
        //Saida de dados
        System.out.println("A distância entre os dois pontos é de " + distanciaPlanos);
    }
    */

    public static double getDistanciaPlanos(int x1, int y1, int x2, int y2) {

        return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }

    public static int exercicio4(int valorx) {
        if (valorx == 0) {
            return 0;
        } else if (valorx > 0) {
            return (valorx * valorx) - (2 * valorx);
        } else
            return valorx;

    }
    /*
    public static void exercicio5() {
        Scanner ler = new Scanner(System.in);
        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 5");
        //Leitura de Dados
        System.out.println("Insira o valor da área do cubo, em cm2:");
        double area = ler.nextDouble();
        if (area <= 0) {
            System.out.println("Valor da área incorreto.");
        } else {
            double volume = getCubeVolume(area);
            System.out.println("Volume do cubo = " + volume);
            System.out.println(getCubeSize(area));
        }
    }
    */
    public static String getCubeSize(double area) {
        double volume = getCubeVolume(area);
        if (volume <= 1) {
            return "O cubo é Pequeno.";
        } else if (volume > 2) {
            return "O cubo é Grande.";
        } else {
            return "O cubo é Médio.";
        }
    }

    public static double getCubeVolume(double area) {
        double aresta = Math.sqrt(area / 6);

        return Math.pow(aresta, 3);
    }
/*
    public static void exercicio6() {
        Scanner ler = new Scanner(System.in);
        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 6");
        //Leitura de Dados
        System.out.println("Insira quantos segundos passaram desde o inicio do dia:");
        int segundos = ler.nextInt();
        int segundosiniciais = segundos;
        //Processamento
        while (segundos > 86400) {
            segundos = segundos - 86400;
            segundosiniciais = segundos;
        }
        int horas = getHoras(segundos);
        int minutos = getMinutos(segundos);
        segundos = getSegundosLeft(segundos);
        //Apresentação de dados
        System.out.println("São " + horas + ":" + minutos + ":" + segundos);
        if (21599 < segundosiniciais && segundosiniciais < 43201) {
            System.out.println("Bom dia!");
        } else if (43200 < segundosiniciais && segundosiniciais < 72001) {
            System.out.println("Boa tarde!");
        } else {
            System.out.println("Boa noite!");
        }


    }
*/
    public static int getSegundosLeft(int segundos) {
        int modulosegundos = segundos % 3600;
        modulosegundos = modulosegundos % 60;
        return modulosegundos;
    }

    public static int getMinutos(int segundos) {
        int modulosegundos = segundos % 3600;

        return modulosegundos / 60;
    }

    public static int getHoras(int segundos) {

        return segundos / 3600;
    }

    public static double exercicio7custototal(float area, float custoLitro, double rendimentoLitro, double salarioPintores) {

        return exercicio7customaoobra(area, salarioPintores) + exercicio7custotinta(area, custoLitro, (float) rendimentoLitro);

    }

    private static int getPintores(double area) {
        int pintores = 0;
        if (0 < area && area < 100) {
            pintores = 1;
        } else if (100 <= area && area < 300) {
            pintores = 2;
        } else if (300 <= area && area < 1000) {
            pintores = 3;
        } else if (area >= 1000) {
            pintores = 4;
        }
        return pintores;
    }

    public static double exercicio7customaoobra(double area, double salarioPintores) {


        int pintores = getPintores(area);
        return (area / (16 * pintores)) * salarioPintores;
        //double custo = (pintores*salarioPintores);
        //return custo;

    }

    public static double exercicio7custotinta(float area, float custoLitro, float rendimentoLitro) {

        return Math.ceil(area / rendimentoLitro) * custoLitro;

    }

    public static boolean exercicio8XmultiplodeY(int x, int y) {
        return x % y == 0;
    }

    public static boolean exercicio8YmultiploX(int x, int y) {
        return y % x == 0;

    }
/*
    public static void exercicio8() {
        Scanner ler = new Scanner(System.in);
        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 8");
        //Leitura de Dados
        System.out.println("Insira o primeiro numero:");
        int x = ler.nextInt();
        System.out.println("Insira o segundo numero:");
        int y = ler.nextInt();
        if (exercicio8XmultiplodeY(x, y) == true && exercicio8YmultiploX(x, y) == true) {
            System.out.println("Ambos os números são multiplos um do outro.");
        } else if (exercicio8XmultiplodeY(x, y) == true) {
            System.out.println(x + " é multiplo de " + y);
        } else if (exercicio8YmultiploX(x, y) == true) {
            System.out.println(y + " é multiplo de " + y);
        } else {
            System.out.println(x + " não é multiplo nem divisor de " + y);
        }
    }
*/

    public static boolean exercicio9(int numero) {
        int digito1 = (numero / 100) % 10;
        int digito2 = (numero / 10) % 10;
        int digito3 = numero % 10;

        return digito1 < digito2 && digito2 < digito3;
    }

    public static double exercicio10(double preco) {
        if (preco <= 50) {
            preco = preco - preco * 0.20;
        } else if (preco > 50 && preco <= 100) {
            preco = preco - preco * 0.30;
        } else if (preco > 100 && preco <= 200) {
            preco = preco - preco * 0.40;
        } else if (preco > 200) {
            preco = preco - preco * 0.60;
        }
        return preco;
    }
/*
    public static void exercicio11() {
        Scanner ler = new Scanner(System.in);
        //Cabeçalho
        System.out.println("---------------");
        System.out.println("Exercício 11");
        //Leitura de Dados
        System.out.println("Insira a percentagem de aprovados:");
        double aprovados = ler.nextDouble();
        System.out.println("Qual a percentagem limite para 'Turma Má'?");
        double turmaMa = ler.nextDouble();
        System.out.println("Qual a percentagem limite para 'Turma Fraca'?");
        double turmaFraca = ler.nextDouble();
        System.out.println("Qual a percentagem limite para 'Turma Razoável'?");
        double turmaRazoável = ler.nextDouble();
        System.out.println("Qual a percentagem limite para 'Turma Boa'?");
        double turmaBoa = ler.nextDouble();
        System.out.println(getAprovados(aprovados, turmaMa, turmaFraca, turmaRazoável, turmaBoa));
    }
*/
    public static String getAprovados(double aprovados, double turmaMa, double turmaFraca,
                                      double turmaRazoavel, double turmaBoa) {
        String mensagem;
        if (aprovados < 0 || aprovados > 1) {
            mensagem = "Valor inválido.";
        } else if (aprovados < turmaMa) {
            mensagem = "Turma Má.";
        } else if (aprovados < turmaFraca) {
            mensagem = "Turma Fraca.";
        } else if (aprovados < turmaRazoavel) {
            mensagem = "Turma Razoável.";
        } else if (aprovados < turmaBoa) {
            mensagem = "Turma Boa.";
        } else {
            mensagem = "Turma Excelente.";
        }
        return mensagem;
    }

    public static String exercicio12(double indicePoluicao) {
        if (indicePoluicao <= 0.3) {
            return "O índíce de poluição está aceitável.";
        } else if (indicePoluicao > 0.5) {
            return "Indíce de poluição elevado! Os 3 grupos devem paralisar as atividades.";
        } else if (indicePoluicao > 0.4) {
            return "Indíce de poluição alto! Indústrias do grupo 1 e 2 devem paralisar as atividades.";
        } else {
            return "Indíce de poluição médio! Indústrias de grupo 1 devem paralisar as atividades.";
        }
    }

    public static double exercicio13Tempo(int grama, int arvores, int arbustos) {
        double segundos = (grama * 300) + (arvores * 600) + (arbustos * 400);
        return segundos / 3600;
    }

    public static double exercicio13Preco(int grama, int arvores, int arbustos) {
        double segundos = (grama * 300) + (arvores * 600) + (arbustos * 400);
        double horas = segundos / 3600;
        double precoTempo = horas * 10;
        double precoProdutos = (grama * 10) + (arvores * 20) + (arbustos * 15);
        return precoProdutos + precoTempo;

    }

    public static double exercicio14(double distancia1, double distancia2, double distancia3, double distancia4, double distancia5) {
        double distanciaTotalMilhas = distancia1 + distancia2 + distancia3 + distancia4 + distancia5;
        double distanciaTotalKm = distanciaTotalMilhas * 1609;
        return distanciaTotalKm / 1000 / 5;
    }

    public static String exercicio15(double lado1, double lado2, double lado3) {
        if (lado1 <= 0 || lado2 <= 0 || lado3 <= 0) {
            return "Este triângulo é impossível: Nenhum lado pode ser 0 ou negativo.";
        } else if (lado1 > lado2 + lado3 || lado2 > lado1 + lado3 || lado3 > lado1 + lado2) {
            return "Este triângulo é impossível: Nenhum lado pode ser maior que a soma dos outros dois.";
        } else if (lado1 == lado2 && lado1 == lado3) {
            return "Este triângulo é Equilátero.";
        } else if (lado1 == lado2 || lado1 == lado3 || lado2 == lado3) {
            return "Este triângulo é Isósceles.";
        } else {
            return "Este triângulo é Escaleno.";
        }

    }

    public static String exercicio16(double angulo1, double angulo2, double angulo3) {
        if (angulo1 <= 0 || angulo2 <= 0 || angulo3 <= 0) {
            return "Este triângulo é impossível: Nenhum angulo pode ser 0 ou negativo.";
        } else if (angulo1 + angulo2 + angulo3 != 180) {
            return "Este triângulo é impossível: A soma dos angulos tem de ser 180.";
        } else if (angulo1 == 90 || angulo2 == 90 || angulo3 == 90) {
            return "Este triângulo é Retângulo.";
        } else if (angulo1 < 90 && angulo2 < 90 && angulo3 < 90) {
            return "Este triângulo é Acutângulo.";
        } else {
            return "Este triângulo é Obtuso.";
        }

    }

    public static String exercicio18(int horaInicio, int minutoinicio, int segundosInicio, int tempoprocess) {

        int horas = getHoras(tempoprocess);
        int minutos = getMinutos(tempoprocess);
        int segundos = getSegundosLeft(tempoprocess);

        int horaFinal = horaInicio + horas;
        int segundosFinal = segundosInicio + segundos;
        int minutosFinal = minutoinicio + minutos;
       /*
        while (segundosFinal > 60) {
            segundosFinal = segundosFinal - 60;
            minutosFinal = minutosFinal + 1;
        }
        while (minutosFinal > 60) {
            minutosFinal = minutosFinal - 60;
            horaFinal = horaFinal + 1;
        }

    */
        while (horaFinal > 24) {
            horaFinal = horaFinal - 24;
        }
        return "O processamento acabará às " + String.format("%02d", horaFinal) + ":" + String.format("%02d", minutosFinal) + ":" + String.format("%02d", segundosFinal);
    }

    public static String exercicio17(int horasPartida, int minutosPartida, int horasDuracao, int minutosDuracao) {
        int horaChegada = horasPartida + horasDuracao;
        boolean diaSeguinte = false;
        if (horaChegada >= 24) {
            horaChegada = horaChegada - 24;
            diaSeguinte = true;
        }
        int minutosChegada = minutosPartida + minutosDuracao;
        if (minutosChegada >= 60) {
            minutosChegada = minutosChegada - 60;
            horaChegada = horaChegada + 1;
        }
        if (diaSeguinte) {
            return "O comboio chegará às " + String.format("%02d", horaChegada) + ":" + String.format("%02d", minutosChegada) + " do dia seguinte.";
        } else {
            return "O comboio chegará às " + String.format("%02d", horaChegada) + ":" + String.format("%02d", minutosChegada) + " do próprio dia.";
        }
    }

    public static double exercicio19(int horas) {
        if (horas <= 36) {
            return horas * 7.5;
        } else if (horas <= 41) {
            int horasExtra = horas - 36;
            return 270 + horasExtra * 10;
        } else {
            int horasExtra = horas - 41;
            return 320 + horasExtra * 15;
        }
    }

    public static double exercicio20(boolean diaUtil, char kit, double kmEntrega) {
        if (diaUtil) {
            if (kit == 'a' || kit == 'A') {
                return 30 + kmEntrega * 2;
            } else if (kit == 'b' || kit == 'B') {
                return 50 + kmEntrega * 2;
            } else {
                return 100 + kmEntrega * 2;
            }
        } else {
            if (kit == 'a' || kit == 'A') {
                return 40 + kmEntrega * 2;
            } else if (kit == 'b' || kit == 'B') {
                return 70 + kmEntrega * 2;
            } else {
                return 140 + kmEntrega * 2;
            }
        }
    }


}
