package Previous_Exercises;

public class Edificio {
    private double alturaSombra;

    public Edificio(double alturaSombra) {
        if (alturaSombra < 0) {
            throw new IllegalArgumentException("Sombra não pode ter altura negativa");
        }
        this.alturaSombra = alturaSombra;
    }

    public double calcularAltura(Pessoa pessoa) {
        return alturaSombra * pessoa.getAltura() / pessoa.getComprimentoSombra();
    }
}
