package com.company;

public class Exercicio_Dezasete {
    /**
     * Method to multiply all the cells of an Integer matrix with a constant number.
     *
     * @param matriz    a 2-dimensional Array of the type Integer.
     * @param constante number for which the matrix will be multiplied, of type Integer.
     * @return a 2-dimensional Array, of the type Integer.
     */
    public static int[][] produtoMatrizConstante(int[][] matriz, int constante) {
        int[][] novaMatriz = matriz.clone();
        int indexL, indexC;
        for (indexL = 0; indexL < matriz.length; indexL++) {
            for (indexC = 0; indexC < matriz[indexL].length; indexC++) {
                novaMatriz[indexL][indexC] = matriz[indexL][indexC] * constante;
            }
        }
        return novaMatriz;
    }

    /**
     * Method to add each corresponding value of two Integer matrices.
     *
     * @param matriz1 2-dimensional Array of type Integer.
     * @param matriz2 2-dimensional Array of type Integer.
     * @return 2-dimensional Array, of type Integer, containing the added values of the passed matrices.
     */
    public static int[][] somaMatrizes(int[][] matriz1, int[][] matriz2) {
        int[][] novaMatriz = matriz1.clone();
        int indexL, indexC;
        for (indexL = 0; indexL < matriz1.length; indexL++) {
            for (indexC = 0; indexC < matriz1[indexL].length; indexC++) {
                novaMatriz[indexL][indexC] = matriz1[indexL][indexC] + matriz2[indexL][indexC];
            }
        }
        return novaMatriz;
    }

    /**
     * Method to return a Matrix that is the product of the multiplication of two other matrices.
     *
     * @param matriz1 2-dimensional Array of type Integer.
     * @param matriz2 2-dimensional Array of type Integer.
     * @return 2-dimensional Array of type Integer.
     */
    public static int[][] produtoMatrizes(int[][] matriz1, int[][] matriz2) {
        if (matriz1[0].length != matriz2.length) {
            return null;
        }
        int[][] matrizProduto = new int[matriz1.length][matriz2[0].length];
        for (int indexF = 0; indexF < matrizProduto.length; indexF++) {
            for (int indexC = 0; indexC < matrizProduto[indexF].length; indexC++) {
                matrizProduto[indexF][indexC] = produtoDeIndice(matriz1, matriz2, indexF, indexC);
            }
        }

        return matrizProduto;
    }

    public static int produtoDeIndice(int[][] matriz1, int[][] matriz2, int indiceF, int indiceC) {
        int indice = 0;
        for (int i = 0; i < matriz2.length; i++) {
            indice += matriz1[indiceF][i] * matriz2[i][indiceC];
        }
        return indice;
    }

}
