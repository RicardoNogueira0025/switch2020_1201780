package com.company;

import java.util.Arrays;

public class Exercicio_Dez {
    /**
     * Method to return the smallest integer on an array
     *
     * @param array of type Integer.
     * @return the smallest value of type Integer.
     */
    public static int menorValorDeArray(int[] array) {
        Arrays.sort(array);
        return array[0];
    }

    /**
     * Method to return the larger integer on an array
     *
     * @param array of type Integer.
     * @return the larger value of type Integer.
     */
    public static int maiorValorDeArray(int[] array) {
        Arrays.sort(array);
        return array[array.length - 1];
    }

    /**
     * Method to return the average of all the values in an array.
     *
     * @param array of the type Integer.
     * @return the average of the array values, of the type Double.
     */
    public static double mediaElementosArray(int[] array) {
        return (double) Exercicio_Tres.somaDeElementos(array) / array.length;
    }

    /**
     * Method to return the product of the multiplication of all the values in an Array.
     *
     * @param array of the type Integer.
     * @return the product of the array values, of the type Integer.
     */
    public static int produtoElementosArray(int[] array) {
        int produtoDeDigitos = 1;
        for (int digito : array) {
            produtoDeDigitos *= digito;
        }
        return produtoDeDigitos;
    }

    /**
     * Method to return an array, of the type Integer, without any repetitions of the values of the passed Array.
     *
     * @param array of the type Integer.
     * @return copy of the passed Array without any repeated values.
     */
    public static int[] elementosNaoRepetidos(int[] array) {
        Arrays.sort(array);
        int[] novoArray = new int[array.length];
        int indiceNovo = 0;
        for (int i = 0; i < array.length; i++) {
            if (i == array.length - 1) {
                novoArray[indiceNovo] = array[i];
                indiceNovo++;
            } else {
                if (array[i] != array[i + 1]) {
                    novoArray[indiceNovo] = array[i];
                    indiceNovo++;
                }
            }
        }
        return Exercicio_Seis.darArrayLimitado(novoArray, indiceNovo);
    }

    /**
     * Method to return an Array which is the inverse of the passed Array.
     *
     * @param array of the type Integer.
     * @return a copy of the passed Array, with the values in reverse order.
     */
    public static int[] vetorInverso(int[] array) {
        int[] novoArray = new int[array.length];
        int indiceContrario = 0;
        for (int numero : array) {
            novoArray[array.length - 1 - indiceContrario] = numero;
            indiceContrario++;
        }
        return novoArray;
    }

    /**
     * Method to return an Array with only the prime values contained in the passed Array.
     *
     * @param array of the type Integer.
     * @return an array with only the prime numbers of the passed Array.
     */
    public static int[] arrayDePrimos(int[] array) {
        int[] novoArray = new int[array.length];
        int indice = 0;
        for (int numero : array) {
            if (ePrimo(numero)) {
                novoArray[indice] = numero;
                indice++;
            }
        }
        return Exercicio_Seis.darArrayLimitado(novoArray, indice);
    }

    /**
     * Method to check if a given number is prime.
     *
     * @param numero of the type Integer.
     * @return boolean, True if the number is prime.
     */
    public static boolean ePrimo(int numero) {
        if (numero == 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(numero); i++) {
            if (numero % i == 0) {
                return false;
            }
        }
        return true;
    }


}
