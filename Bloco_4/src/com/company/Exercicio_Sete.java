package com.company;

import Previous_Exercises.BlocoTres;

public class Exercicio_Sete {
    /**
     * Method that returns an Array containing all the multiples of a given number present in a given interval.
     *
     * @param minimo   Integer representing the start of the interval.
     * @param maximo   Integer representing the end of the interval.
     * @param multiplo Number for which to check the multiples.
     * @return An array of type Integer containing the multiples of the given number.
     */
    public static int[] multiplosNumIntervalo(int minimo, int maximo, int multiplo) {
        int[] arrayFinal = new int[(int) BlocoTres.numeroMultiplosNumIntervalo(minimo, maximo, multiplo)];
        int indiceArray = 0;
        for (int i = minimo; i <= maximo; i++) {
            if (verificarMultiplo(i, multiplo)) {
                arrayFinal[indiceArray] = i;
                indiceArray++;
            }
        }
        return arrayFinal;
    }

    /**
     * Method to check if a number is a multiple of another.
     *
     * @param numero   Integer to check if it's a multiple.
     * @param multiplo Integer from which to check if the first one is a multiple of.
     * @return boolean True if the numbers are multiples.
     */
    public static boolean verificarMultiplo(int numero, int multiplo) {
        return numero % multiplo == 0;
    }
}
