package com.company;

import Previous_Exercises.BlocoTres;

public class Exercicio_Cinco {
    public static int somaDePares(int numero) {
        return (int) BlocoTres.ex6SomaDePares(numero);
    }

    public static int somaDeImpares(int numero) {
        return (int) BlocoTres.ex6SomaDeImpares(numero);
    }


}
