package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Exercicio_Dezoito {

    public static void main(String[] args) {

        String[] palavras = {"PEITO", "SOGRA", "FADO", "SAPO", "RIO", "RABO", "ARO", "ROLA", "RIA", "RALO", "OLEO", "RIO", "PIA"};
        char[][] sopa = {{'P', 'E', 'I', 'T', 'O', 'F', 'U', 'O'}, {'R', 'O', 'T', 'U', 'L', 'I', 'R', 'E'},
                {'E', 'S', 'O', 'G', 'R', 'A', 'L', 'O'}, {'F', 'U', 'A', 'R', 'I', 'N', 'E', 'I'},
                {'A', 'G', 'E', 'P', 'A', 'L', 'O', 'R'}, {'D', 'M', 'S', 'T', 'O', 'N', 'I', 'A'}, {'O', 'T', 'A', 'C', 'O', 'B', 'A', 'R'},
                {'V', 'I', 'F', 'E', 'U', 'T', 'P', 'X'}};
        sopaDeLetras(palavras, sopa);

    }

    /**
     * A method to simulate a game of word salad in the console.
     *
     * @param listaDePalavras A 1-dimensional array containing the solutions to the word salad, of type String.
     * @param sopaDeLetras    A 2-dimensional square array containing various letters, of type Character.
     */
    public static void sopaDeLetras(String[] listaDePalavras, char[][] sopaDeLetras) {
        Scanner ler = new Scanner(System.in);
        String palavraEscolhida;
        String[] listaOriginal = listaDePalavras.clone();
        for (int numeroPalavras = listaDePalavras.length; numeroPalavras > 0; numeroPalavras += 0) {
            mostrarMatriz(sopaDeLetras);
            int[][] matrizMascara1 = gerarMatrizMascara(sopaDeLetras);
            int[][] matrizMascara2 = gerarMatrizMascara(sopaDeLetras);
            System.out.println("\n--------");
            System.out.println("Inserir Coordenadas da primeira letra:");
            int cord1 = ler.nextInt();
            int cord2 = ler.nextInt();
            matrizMascara1[cord1][cord2] = 1;
            int[] coordenada1 = obterCoordenadasDoUm(matrizMascara1);
            mostrarMatriz(sopaDeLetras);
            System.out.println("\n--------");
            System.out.println("Inserir Coordenadas da última letra:");
            cord1 = ler.nextInt();
            cord2 = ler.nextInt();
            matrizMascara2[cord1][cord2] = 1;
            int[] coordenada2 = obterCoordenadasDoUm(matrizMascara2);
            palavraEscolhida = obterPalavra(sopaDeLetras, coordenada1, coordenada2);
            if (verificarSePalavraExiste(palavraEscolhida, listaDePalavras)) {
                listaDePalavras = removerPalavraDaLista(palavraEscolhida, listaDePalavras);
                numeroPalavras--;
                System.out.println("Boa! Encontraste a palavra " + palavraEscolhida + "! Palavras restantes: " + numeroPalavras);
                premirEnterParaContinuar();
            } else {
                if (verificarSePalavraExiste(palavraEscolhida, listaOriginal)) {
                    System.out.println("Já encontraste essa palavra!");
                    premirEnterParaContinuar();
                } else {
                    System.out.println("Essa palavra não está na sopa!");
                    premirEnterParaContinuar();
                }

            }
        }
        System.out.println("Parabéns! Encontraste todas as palavras!");

    }

    /**
     * A method to pause the runtime until the Return key is pressed.
     */
    public static void premirEnterParaContinuar() {
        System.out.println("Prime ENTER para continuar");
        try {
            System.in.read();
        } catch (Exception ignored) {
        }
    }

    /**
     * A method to remove a correctly guessed word from the solution Array.
     *
     * @param palavraEscolhida The word to be removed, of type String.
     * @param listaDePalavras  A 1-dimensional array containing the solutions to the word salad, of type String.
     * @return A 1-dimensional array containg the remaining words to be found in the word salad, of type String.
     */
    public static String[] removerPalavraDaLista(String palavraEscolhida, String[] listaDePalavras) {
        int contador = 0;
        ArrayList<String> novaLista = new ArrayList<>();
        for (String palavra : listaDePalavras) {
            if (!palavraEscolhida.equalsIgnoreCase(palavra)) {
                novaLista.add(palavra);
                contador++;
            }
        }
        return novaLista.toArray(new String[contador]);
    }

    /**
     * A method to verify if the chosen word is on the solution list.
     *
     * @param palavraEscolhida The word to be verified, of type String.
     * @param listaDePalavras  A 1-dimensional array containing the solutions to the word salad, of type String.
     * @return boolean True if the chosen word is on the list.
     */
    public static boolean verificarSePalavraExiste(String palavraEscolhida, String[] listaDePalavras) {
        boolean palavraPresente = false;
        for (String palavra : listaDePalavras) {
            if (!palavraEscolhida.equalsIgnoreCase(palavra)) continue;
            palavraPresente = true;
        }
        return palavraPresente;
    }

    /**
     * A method to parse the path of two given coordinates and read the word formed by the letters.
     *
     * @param sopa        A 2-dimensional square array containing various letters, of type Character.
     * @param coordenada1 A 1-dimensional array containing the coordinates of the first letter, of type Integer. [0] contains the row index, [1] contains the column index.
     * @param coordenada2 A 1-dimensional array containing the coordinates of the second letter, of type Integer. [0] contains the row index, [1] contains the column index.
     * @return String containin the word formed by the letters between the coordinates.
     */
    public static String obterPalavra(char[][] sopa, int[] coordenada1, int[] coordenada2) {
        String palavraEscolhida = "";
        switch (verificarDirecao(coordenada1, coordenada2)) {
            case 1:
                for (int i = coordenada1[1]; i <= coordenada2[1]; i++) {
                    palavraEscolhida += sopa[coordenada1[0]][i];
                }
                break;
            case 2:
                for (int i = coordenada1[1]; i >= coordenada2[1]; i--) {
                    palavraEscolhida += sopa[coordenada1[0]][i];
                }
                break;
            case 3:
                for (int i = coordenada1[0]; i <= coordenada2[0]; i++) {
                    palavraEscolhida += sopa[i][coordenada1[1]];
                }
                break;
            case 4:
                for (int i = coordenada1[0]; i >= coordenada2[0]; i--) {
                    palavraEscolhida += sopa[i][coordenada1[1]];
                }
                break;
            case 5:
                for (int i = coordenada1[0], j = coordenada1[1]; i <= coordenada2[0]; i++, j++) {
                    palavraEscolhida += sopa[i][j];
                }
                break;
            case 6:
                for (int i = coordenada1[0], j = coordenada1[1]; i >= coordenada2[0]; i--, j++) {
                    palavraEscolhida += sopa[i][j];
                }
                break;
            case 7:
                for (int i = coordenada1[0], j = coordenada1[1]; i <= coordenada2[0]; i++, j--) {
                    palavraEscolhida += sopa[i][j];
                }
                break;
            case 8:
                for (int i = coordenada1[0], j = coordenada1[1]; i >= coordenada2[0]; i--, j--) {
                    palavraEscolhida += sopa[i][j];
                }
                break;
        }
        return palavraEscolhida;
    }

    /**
     * A method to verify the direction of the desired word.
     *
     * @param coordenada1 A 1-dimensional array containing the coordinates of the first letter, of type Integer. [0] contains the row index, [1] contains the column index.
     * @param coordenada2 A 1-dimensional array containing the coordinates of the second letter, of type Integer. [0] contains the row index, [1] contains the column index.
     * @return int A number representing the selected direction.
     * <p>1. Left -> Right</p>
     * <p>2. Right -> Left</p>
     * <p>3. Up -> Down</p>
     * <p>4. Down -> Up</p>
     * <p>5. Diagonal -> Descending Right</p>
     * <p>6. Diagonal -> Ascending Right</p>
     * <p>7. Diagonal -> Descending Left</p>
     * <p>8. Diagonal -> Ascending Left</p>
     */
    public static int verificarDirecao(int[] coordenada1, int[] coordenada2) {
        /*
        1. Esquerda -> Direita
        2. Direita -> Esquerda
        3. Cima -> Baixo
        4. Baixo -> Cima
        5. Diagonal -> Descendente Direita
        6. Diagonal -> Ascendente Direita
        7. Diagonal Descendente Esquerda
        8. Diagonal Ascendente Esquerda
        */

        if (coordenada1[0] == coordenada2[0]) {
            //mesma fila
            if (coordenada1[1] < coordenada2[1]) {
                //Esquerda->Direita
                return 1;
            } else {
                //Direita->Esquerda
                return 2;
            }
        } else if (coordenada1[0] < coordenada2[0]) {
            //descendente
            if (coordenada1[1] == coordenada2[1]) {
                //mesma coluna, Cima->Baixo
                return 3;
            } else if (coordenada1[1] < coordenada2[1]) {
                //colunas diferentes, Diagonal -> Descendente Direita
                return 5;
            } else {
                //Diagonal -> Descendente Esquerda
                return 7;
            }
        } else {
            //ascendente
            if (coordenada1[1] == coordenada2[1]) {
                //mesma coluna, Baixo->Cima
                return 4;
            } else if (coordenada1[1] < coordenada2[1]) {
                //colunas diferentes, Diagonal -> Ascendente Direita
                return 6;
            } else {
                //Diagonal -> Ascendente Esquerda
                return 8;
            }
        }
    }

    /**
     * A method to print a matrix to the console.
     *
     * @param matriz A 2-dimensional array of type Character.
     */
    public static void mostrarMatriz(char[][] matriz) {
        for (char[] chars : matriz) {
            System.out.println();
            for (char aChar : chars) {
                System.out.print("|" + aChar + "|");
            }
        }
    }

    /**
     * A method to generate a mask matrix of the same size as the word salad, filled with 0s.
     *
     * @param matriz A 2-dimensional array of type Character.
     */
    public static int[][] gerarMatrizMascara(char[][] matriz) {
        int[][] matrixMascara = new int[matriz.length][matriz.length];
        for (int[] fila : matrixMascara) {
            for (int celula : fila) {
                celula = 0;
            }
        }
        return matrixMascara;
    }

    /**
     * A method to get the coordinates of the cell containing the number 1 in a matriz.
     *
     * @param matrixMascara A 2-dimensional array of type Integer.
     * @return A 1-dimensional array containing the coordinates of the 1 cell, of type Integer. [0] contains the row index, [1] contains the column index.
     */
    public static int[] obterCoordenadasDoUm(int[][] matrixMascara) {
        for (int i = 0; i < matrixMascara.length; i++) {
            for (int j = 0; j < matrixMascara.length; j++) {
                if (matrixMascara[i][j] == 1) {
                    return new int[]{i, j};
                }
            }
        }
        return null;
    }

}
