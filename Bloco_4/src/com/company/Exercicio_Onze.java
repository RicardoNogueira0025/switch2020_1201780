package com.company;

public class Exercicio_Onze {
    /**
     * Method to return the scalar product of two arrays.
     *
     * @param vetor1 First array of Integer type.
     * @param vetor2 Second array of Integer type.
     * @return Integer representing the scalar product of the two arrays.
     */
    public static int produtoEscalar(int[] vetor1, int[] vetor2) {
        int produto = 0;
        int lenght = vetor1.length;
        for (int i = 0; i < lenght; i++) {
            produto = produto + (vetor1[i] * vetor2[i]);
        }
        return produto;
    }
}
