package com.company;

import Previous_Exercises.BlocoTres;

import java.util.ArrayList;
import java.util.List;

public class Exercicio_Quatro {
    /**
     * Method to return only the even numbers present on an Array.
     *
     * @param arrayInicial An array of type Integer.
     * @return An Array of type Integer containing the even numbers of the passed Array.
     */
    public static Object[] separarPares(int[] arrayInicial) {
        List<Integer> paresList = new ArrayList<>();
        for (int numero : arrayInicial) {
            if (BlocoTres.ePar(numero)) {
                paresList.add(numero);
            }
        }
        return paresList.toArray();
    }

    /**
     * Method to return only the odd numbers present on an Array.
     *
     * @param arrayInicial An array of type Integer.
     * @return An Array of type Integer containing the odd numbers of the passed Array.
     */
    public static Object[] separarImpares(int[] arrayInicial) {
        List<Integer> imparesList = new ArrayList<>();
        for (int numero : arrayInicial) {
            if (BlocoTres.eImpar(numero)) {
                imparesList.add(numero);
            }
        }
        return imparesList.toArray();
    }


}
