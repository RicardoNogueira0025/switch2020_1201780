package com.company;

public class Exercicio_Nove {
    /**
     * Method to check if a given number is a palindrome.
     *
     * @param numero Integer to check.
     * @return boolean True if the number is a palindrome.
     */
    public static boolean verificarCapicua(int numero) {
        int[] digitos = Exercicio_Dois.arrayDeDigitos(numero);
        int indiceArrayReversa = 1;
        boolean resultado = false;
        for (int i = 0; i < digitos.length - indiceArrayReversa; i++) {
            if (digitos[i] == digitos[digitos.length - indiceArrayReversa]) {
                resultado = true;
                indiceArrayReversa++;
            }
        }
        return resultado;
    }
}
