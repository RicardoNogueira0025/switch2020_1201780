package com.company;

public class Exercicio_Seis {
    /**
     * Method to trim an Array to the specified length.
     *
     * @param arrayIncial     Array of type Integer to be trimmed.
     * @param numeroElementos Integer representing the length of the new Array.
     * @return an Array of type Integer trimmed to the specified length.
     */
    public static int[] darArrayLimitado(int[] arrayIncial, int numeroElementos) {
        int[] arrayFinal = new int[numeroElementos];
        if (numeroElementos >= 0) System.arraycopy(arrayIncial, 0, arrayFinal, 0, numeroElementos);
        return arrayFinal;
    }
}
