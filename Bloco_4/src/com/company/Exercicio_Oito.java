package com.company;

import Previous_Exercises.BlocoTres;

public class Exercicio_Oito {
    /**
     * Method to return an Array containing the multiples of two numbers present in a given interval.
     *
     * @param minimo    Integer representing the start of the interval.
     * @param maximo    Integer representing the end of the interval.
     * @param multiplos Array of Integer type containing the numbers to check the multiples of.
     * @return an Array of Integer type with the multiples of the two numbers given, in the given interval.
     */
    public static int[] arrayDeMultiplosComuns(int minimo, int maximo, int[] multiplos) {
        int[] arrayFinal = new int[BlocoTres.numeroDeMultiplosComunsNumIntervalo(multiplos[0], multiplos[1], minimo, maximo)];
        int indiceArray = 0;
        for (int i = minimo; i <= maximo; i++) {
            if (i % multiplos[0] == 0 && i % multiplos[1] == 0) {
                arrayFinal[indiceArray] = i;
                indiceArray++;
            }
        }
        return arrayFinal;
    }


}
