package com.company;

import java.util.ArrayList;
import java.util.List;

public class Exercicio_Quinze {
    /**
     * Method that returns the smallest value of a Matrix.
     *
     * @param matriz 2-dimension Array of Float type.
     * @return Float representing the smallest value of the given matrix.
     */
    public static float elementoMenorValorMatriz(float[][] matriz) {
        float elementoMenor = matriz[0][0];
        for (float[] floats : matriz) {
            for (float elemento : floats) {
                if (elemento < elementoMenor) {
                    elementoMenor = elemento;
                }
            }
        }
        return elementoMenor;
    }

    /**
     * Method that returns the largest value of a Matrix.
     *
     * @param matriz 2-dimension Array of Float type.
     * @return Float representing the largest value of the given matrix.
     */
    public static float elementoMaiorValorMatriz(float[][] matriz) {
        float elementoMenor = matriz[0][0];
        for (float[] floats : matriz) {
            for (float elemento : floats) {
                if (elemento > elementoMenor) {
                    elementoMenor = elemento;
                }
            }
        }
        return elementoMenor;
    }

    /**
     * Method that returns the average value of a Matrix.
     *
     * @param matriz 2-dimension Array of Float type.
     * @return Float representing the average value of the given matrix.
     */
    public static float mediaElementosMatriz(float[][] matriz) {
        int numeroElementos = 0, soma = 0;
        for (float[] floats : matriz) {
            for (float elemento : floats) {
                soma += elemento;
                numeroElementos++;
            }
        }
        return (float) soma / numeroElementos;
    }

    /**
     * Method that returns the product of the multiplication of all the values of a Matrix.
     *
     * @param matriz 2-dimension Array of Integer type.
     * @return Long representing the product of all the values of the given matrix.
     */
    public static long produtoElementosMatriz(int[][] matriz) {
        int produtoElementos = 1;
        for (int[] ints : matriz) {
            for (int elemento : ints) {
                produtoElementos *= elemento;
            }
        }
        return produtoElementos;
    }

    /**
     * Method that returns all the values of a given matrix without repetitions.
     *
     * @param matriz 2-dimensional Array of Integer type.
     * @return Array of Integer type with all the values of the given Matrix without repetitions.
     */
    public static int[] elementosNaoRepetidosMatriz(int[][] matriz) {
        List<Integer> elementosUnicosLista = new ArrayList<>();
        for (int[] ints : matriz) {
            int[] listaTemp = Exercicio_Dez.elementosNaoRepetidos(ints);
            for (int num : listaTemp) {
                elementosUnicosLista.add(num);
            }
        }
        int[] elementosUnicos = elementosUnicosLista.stream().mapToInt(i -> i).toArray();
        return Exercicio_Dez.elementosNaoRepetidos(elementosUnicos);
    }

    /**
     * Method that returns all the prime values of a given matrix.
     *
     * @param matriz 2-dimensional Array of Integer type.
     * @return Array of Integer type with all the prime values of the given Matrix.
     */
    public static int[] elementosPrimosNumaMatriz(int[][] matriz) {
        List<Integer> elementosPrimosLista = new ArrayList<>();
        for (int[] ints : matriz) {
            int[] listaTemp = Exercicio_Dez.arrayDePrimos(ints);
            for (int num : listaTemp) {
                elementosPrimosLista.add(num);
            }
        }
        int[] elementosPrimos = elementosPrimosLista.stream().mapToInt(i -> i).toArray();
        return Exercicio_Dez.elementosNaoRepetidos(elementosPrimos);
    }

    /**
     * Method that returns all the values that represent the primary diagonal of a given matrix.
     *
     * @param matriz 2-dimensional Array of Float type.
     * @return Array of Float type with all the values of the given Matrix's primary diagonal.
     */
    public static float[] diagonalPrincipalMatriz(float[][] matriz) {
        if (Exercicio_Treze.matrizEQuadrada(matriz) || Exercicio_Catorze.matrizERetangular(matriz)) {
            float[] diagonal = new float[matriz.length];
            for (int i = 0; i < matriz.length; i++) {
                diagonal[i] = matriz[i][i];
            }
            return diagonal;
        }
        return null;
    }

    /**
     * Method that returns all the values that represent the secondary diagonal of a given matrix.
     *
     * @param matriz 2-dimensional Array of Float type.
     * @return Array of Float type with all the values of the given Matrix's secondary diagonal.
     */
    public static float[] diagonalSecundariaMatriz(float[][] matriz) {
        if (Exercicio_Treze.matrizEQuadrada(matriz) || Exercicio_Catorze.matrizERetangular(matriz)) {

            int index = 0;
            if (Exercicio_Treze.matrizEQuadrada(matriz)) {
                float[] diagonal = new float[matriz[0].length];
                for (int i = (matriz[0].length) - 1; i >= 0; i--) {
                    diagonal[index] = matriz[index][i];
                    index++;
                }
                return diagonal;
            } else if (Exercicio_Catorze.matrizERetangular(matriz)) {
                float[] diagonal = new float[matriz[0].length - 1];
                for (int i = (matriz[0].length) - 1; i > 0; i--) {
                    diagonal[index] = matriz[index][i];
                    index++;
                }
                return diagonal;
            }


        }
        return null;
    }

    /**
     * Method that checks if a given matrix is an Identity Matrix.
     *
     * @param matriz 2-dimesnional Array of Float type.
     * @return boolean True if the given matrix is an Identity Matrix.
     */
    public static boolean matrizIdentidade(float[][] matriz) {
        if (!Exercicio_Treze.matrizEQuadrada(matriz)) {
            return false;
        }
        if (elementoMenorValorMatriz(matriz) != 0 || elementoMaiorValorMatriz(matriz) != 1) {
            return false;
        }
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                if (i != j) {
                    if (matriz[i][j] != 0) {
                        return false;
                    }
                } else {
                    if (matriz[i][j] != 1) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public static float[][] inversoMatriz(float[][] matriz) {
        if (!Exercicio_Treze.matrizEQuadrada(matriz)) {
            return null;
        }
        float[][] matrizInversa;
        if (matriz.length == 2) {
            if (((matriz[0][0] * matriz[1][1] - matriz[0][1] * matriz[1][0]) == 0)) {
                return null;
            }
            matrizInversa = matrizInversa2_2(matriz);
            return matrizInversa;//alterar para o fim, quando estiver tudo feito.
        } else if (matriz.length == 3) {
            matrizInversa = new float[matriz.length][matriz.length];
            matrizInversa = originarIdentidade(matrizInversa);

        }


        return null;
    }

    public static float[][] originarIdentidade(float[][] matrizInversa) {
        for (int i = 0; i < matrizInversa.length; i++) {
            for (int j = 0; j < matrizInversa.length; j++) {
                matrizInversa[i][j] = 0;
            }

        }
        for (int i = 0; i < matrizInversa.length; i++) {
            for (int j = 0; j < matrizInversa.length; j++) {
                if (i == j) {
                    matrizInversa[i][j] = 1;
                }
            }
        }
        return matrizInversa;
    }

    public static float[][] matrizInversa2_2(float[][] matriz) {
        float determinante = (1f / (matriz[0][0] * matriz[1][1] - matriz[0][1] * matriz[1][0]));
        return new float[][]{{determinante * matriz[1][1], determinante * (-matriz[0][1])}, {determinante * (-matriz[1][0]), determinante * matriz[0][0]}};
    }
    /*
    public static int[][] matrizInversa3_3(int[][] matriz){
        int[][] matrizInversa = new int[3][3];
        matrizInversa[0][0]=;
        matrizInversa[0][1]
    }

    */

    /*
    int[][] matrizInversa = matriz.clone();
    int indexL = 0,indexC=0;
        for (int[] ints : matriz) {
        for (int num : ints) {
            matrizInversa[indexL][indexC]=

        }
        indexL++;
        indexC=0;
    }
    */

}
