package com.company;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Exercicio_Dezanove {
    public static void main(String[] args) {
        int[][] matrizSudoku = {
                {0, 0, 4, 0, 0, 5, 3, 0, 9}, {0, 0, 0, 0, 0, 0, 2, 0, 4}, {0, 0, 0, 7, 0, 0, 0, 0, 0},
                {0, 7, 0, 6, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 7, 0, 0, 6, 0}, {0, 0, 3, 9, 0, 0, 0, 0, 0},
                {6, 5, 0, 0, 3, 0, 9, 0, 0}, {8, 0, 2, 0, 0, 4, 0, 5, 7}, {0, 0, 0, 0, 0, 0, 0, 0, 2}};

        jogoSudoku(matrizSudoku);

    }

    /**
     * A method to run a Sudoku game in the console, given a matrix representing a Sudoku table.
     *
     * @param matrizSudoku A 2-dimensional, lenght 9 array, with numbers ranging from 0 to 9. 0 cells represent empty spaces.
     */
    public static void jogoSudoku(int[][] matrizSudoku) {
        Scanner ler = new Scanner(System.in);
        int[][] matrizMapa = criarMatrizMapa(matrizSudoku);
        char opcao;
        String opcaoString;
        int indiceLinha;
        int indiceColuna;
        int numero;
        mostrarMatriz(matrizSudoku);
        long tempoNoInicioDoJogo = System.currentTimeMillis();
        //remainingNumbers=72;


        while (!verificarSudokuEstaCerto(matrizSudoku)) {
            numero = lerNumeroValido();
            System.out.println("Introduzir coordenadas para inserir um número:");
            indiceLinha = ler.nextInt();
            indiceColuna = ler.nextInt();

            if (!verificarSePodeIntroduzirNumero(indiceLinha, indiceColuna, matrizMapa)) {
                System.out.println("Esse número não pode ser alterado!");
                Exercicio_Dezoito.premirEnterParaContinuar();
            } else if (matrizSudoku[indiceLinha][indiceColuna] != 0) {
                System.out.println("Alterar o número que já está nessa posição? Y/N");
                opcaoString = ler.next();
                opcao = opcaoString.charAt(0);
                if (opcao == 'y' || opcao == 'Y') {
                    matrizSudoku[indiceLinha][indiceColuna] = numero;
                }
            } else {
                matrizSudoku[indiceLinha][indiceColuna] = numero;
            }
            mostrarMatriz(matrizSudoku);
        }
        long tempo2 = System.currentTimeMillis() - tempoNoInicioDoJogo;
        mostrarMatriz(matrizSudoku);
        System.out.println("Parabens! Está concluido com sucesso! Demoraste " + TimeUnit.MILLISECONDS.toMinutes(tempo2) + " minutos!");


    }


    public static int lerNumeroValido() {
        Scanner ler = new Scanner(System.in);
        int numero;
        do {
            System.out.println("Que numero quer introduzir?");
            numero = ler.nextInt();

            if (isNumberInvalid(numero)) {
                System.out.println("O número tem de ser entre 1 e 9!");
            }
        } while (isNumberInvalid(numero));
        return numero;
    }

    public static boolean isNumberInvalid(int numero) {
        return numero <= 0 || numero > 9;
    }

    /**
     * A method to check the product of all lines and columns in order to validate the Sudoku result.
     *
     * @param matrizSudoku A 2-dimensional array of type Integer.
     * @return boolean Returns true if the Sudoku matrix is complete and valid.
     */
    public static boolean verificarSudokuEstaCerto(int[][] matrizSudoku) {
        final int NOVE_FATORIAL = 362880;
        for (int[] ints : matrizSudoku) {
            if (Exercicio_Dez.produtoElementosArray(ints) != NOVE_FATORIAL) {
                return false;
            }
        }
        for (int i = 0; i <= 8; i++) {
            int[] coluna = new int[9];
            for (int j = 0; j <= 8; j++) {
                coluna[j] = matrizSudoku[j][i];
            }
            if (Exercicio_Dez.produtoElementosArray(coluna) != NOVE_FATORIAL) {
                return false;

            }
        }
        return true;
    }

    /**
     * A method to create a copy of the original matrix, with 0 on empty spaces and 1 in numbered spaces. 0 are editable cells, 1 are non.editable cells.
     *
     * @param matrizSudoku A 2-dimensional array with lenght = 9, of type Integer.
     * @return array A 2-dimensional array of type Integer.
     */
    public static int[][] criarMatrizMapa(int[][] matrizSudoku) {
        int[][] matrizNumerosFixos = new int[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (matrizSudoku[i][j] != 0) {
                    matrizNumerosFixos[i][j] = 1;
                }
            }
        }
        return matrizNumerosFixos;
    }

    /**
     * A method to verify if the given cell is editable, comparing it with the Map Matrix. 0 are editable cells, 1 are non.editable cells.
     *
     * @param coordenada1 The 1st-dimension index of the target cell, of type Integer.
     * @param coordenada2 The 2nd-dimension index of the target cell, of type Integer.
     * @param matrizMapa  A 2-dimensional array that's a copy of the original.
     * @return boolean True if the given cell is editable.
     */
    public static boolean verificarSePodeIntroduzirNumero(int coordenada1, int coordenada2, int[][] matrizMapa) {
        return matrizMapa[coordenada1][coordenada2] != 1;

    }

    /**
     * A method to print a matrix to the console.
     *
     * @param matriz A 2-dimensional array of type Integer.
     */
    public static void mostrarMatriz(int[][] matriz) {
        for (int i = 0; i <= 2; i++) {
            System.out.println();
            for (int j = 0; j < 9; j++) {
                if (j == 2 || j == 5 || j == 8) {
                    System.out.print("|" + matriz[i][j] + "|");
                } else
                    System.out.print("|" + matriz[i][j]);
            }
        }
        System.out.println();
        System.out.println("_____________________");
        for (int i = 3; i <= 5; i++) {
            System.out.println();
            for (int j = 0; j < 9; j++) {
                if (j == 2 || j == 5 || j == 8) {
                    System.out.print("|" + matriz[i][j] + "|");
                } else
                    System.out.print("|" + matriz[i][j]);
            }
        }
        System.out.println();
        System.out.println("_____________________");
        for (int i = 6; i <= 8; i++) {
            System.out.println();
            for (int j = 0; j < 9; j++) {
                if (j == 2 || j == 5 || j == 8) {
                    System.out.print("|" + matriz[i][j] + "|");
                } else
                    System.out.print("|" + matriz[i][j]);
            }
        }
        System.out.println();
        System.out.println("_____________________");

        /*
        for (int[] ints : matriz) {
            System.out.println();
            for (int numero : ints) {
                System.out.print("|" + numero + "|");
            }
        }

         */
    }

}
