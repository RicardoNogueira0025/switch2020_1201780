package com.company;

public class Exercicio_Dois {
    /**
     * Method to create an Array containing each individual digit of a passed number.
     *
     * @param numero of the type Integer.
     * @return an array with the individual digits of the passed number.
     */
    public static int[] arrayDeDigitos(int numero) {
        int lenght = Exercicio_Um.numeroDeDigitos(numero);
        int[] arrayComDigitos = new int[lenght];
        for (int i = lenght - 1; i >= 0; i--) {

            arrayComDigitos[i] = numero % 10;
            numero = numero / 10;
        }

        return arrayComDigitos;
    }
}
