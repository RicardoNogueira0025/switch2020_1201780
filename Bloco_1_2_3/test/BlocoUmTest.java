import org.junit.Test;

import static org.junit.Assert.*;

public class BlocoUmTest {

    @Test
    public void exercicio3Teste1() {
        double expected = 549778.714;
        double result = BlocoUm.exercicio3processamento(5,7);
        assertEquals(expected,result, 0.001);

    }

    @Test
    public void exercicio3TesteZero(){
        double expected = 0;
        double result = BlocoUm.exercicio3processamento(5,0);
        assertEquals(expected,result, 0.001);
    }

    @Test
    public void exercicio4Teste1(){
        double expected = 2.040;
        double result = BlocoUm.exercicio4processamento(6);
        assertEquals(expected,result,0.01);
    }
    @Test
    public void exercicio4TesteZero() {
        double expected = 0;
        double result = BlocoUm.exercicio4processamento(0);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio5Teste1(){
        double expected = 19.6;
        double result = BlocoUm.exercicio5processamento(2);
        assertEquals(expected, result, 0.01);
    }
    @Test
    public void exercicio5TesteZero(){
        double expected = 0;
        double result = BlocoUm.exercicio5processamento(0);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio6Teste1(){
        double expected = 20;
        double result = BlocoUm.exercicio6processamento(2,4,40);
        assertEquals(expected, result, 0.01);
    }
    @Test
    public void exercicio6teste2(){
        double expected = 19.493;
        double result = BlocoUm.exercicio6processamento(1.72,3,34);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio7Teste1(){
        double expected = 11.3255677;
        double result = BlocoUm.exercicio7processamento(42195,14530,3900);
        assertEquals(expected, result, 0.01);
    }
    @Test
    public void exercicio7TesteZero() {
        double expected = 0;
        double result = BlocoUm.exercicio7processamento(0, 14530, 3900);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio8teste1(){
        double expected = 52.915;
        double result = BlocoUm.exercicio8processamento(40, 60, 60);
        assertEquals(expected, result, 0.01);
    }
    @Test
    public void exercicio8testeZero() {
        double expected = 60;
        double result = BlocoUm.exercicio8processamento(0, 60, 60);
        assertEquals(expected, result, 0.01);
    }
    @Test
    public void exercicio9teste1(){
        double expected = 12;
        double result = BlocoUm.exercicio9processamento(2,4);
        assertEquals(expected,result,0.01);
    }
    @Test
    public void exercicio9teste2(){
        double expected = 86;
        double result = BlocoUm.exercicio9processamento(16,27);
        assertEquals(expected,result,0.01);
    }
    @Test
    public void exercicio10teste1(){
        double expected = 4.4721;
        double result = BlocoUm.exercicio10processamento(2,4);
        assertEquals(expected,result,0.001);
    }
    @Test
    public void exercicio10teste2() {
        double expected = 40.4969;
        double result = BlocoUm.exercicio10processamento(22, 34);
        assertEquals(expected, result, 0.001);
    }


    @Test
    public void exercicio11teste1() {
        double expected = 419;
        double result = BlocoUm.exercicio11processamento(22);
        assertEquals(expected, result, 0.001);
    }
    @Test
    public void exercicio11teste2() {
        double expected = 755;
        double result = BlocoUm.exercicio11processamento(29);
        assertEquals(expected, result, 0.001);
    }
    @Test
    public void exercicio12teste1() {
        double expected = 89.6;
        double result = BlocoUm.exercicio12processamento(32);
        assertEquals(expected, result, 0.001);
    }
    @Test
    public void exercicio12testeNegativo() {
        double expected = 14;
        double result = BlocoUm.exercicio12processamento(-10);
        assertEquals(expected, result, 0.001);
    }
    @Test
    public void exercicio13teste1() {
        double expected = 1373;
        double result = BlocoUm.exercicio13processamento(22,53);
        assertEquals(expected, result, 0.001);
    }
    @Test
    public void exercicio13teste2() {
        double expected = 980;
        double result = BlocoUm.exercicio13processamento(16,20);
        assertEquals(expected, result, 0.001);
    }

}