import org.junit.Test;

import static org.junit.Assert.*;

public class BlocoDoisTest {

    @Test
    public void exercicio1processamentoteste1() {

        double nota1 = 13;
        double peso1 = 30;
        double nota2 = 12;
        double peso2 = 30;
        double nota3 = 18;
        double peso3 = 40;

        double expected = 14.7;
        double result = BlocoDois.exercicio1processamento(nota1, peso1, nota2, peso2, nota3, peso3);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio1processamentoteste2() {

        double nota1 = 13;
        double peso1 = 25;
        double nota2 = 17;
        double peso2 = 25;
        double nota3 = 18;
        double peso3 = 50;

        double expected = 16.5;
        double result = BlocoDois.exercicio1processamento(nota1, peso1, nota2, peso2, nota3, peso3);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getDigito1() {
        int numero = 256;

        int expected = 2;
        int result = BlocoDois.getDigito1(numero);
        assertEquals(expected, result);
    }

    @Test
    public void getDigito2() {
        int numero = 256;

        int expected = 5;
        int result = BlocoDois.getDigito2(numero);
        assertEquals(expected, result);
    }

    @Test
    public void getDigito3() {
        int numero = 256;

        int expected = 6;
        int result = BlocoDois.getDigito3(numero);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio3teste1() {

        int x1 = 3;
        int x2 = 2;
        int y1 = 6;
        int y2 = 8;

        double expected = 2.236;
        double result = BlocoDois.getDistanciaPlanos(x1, y1, x2, y2);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio3teste2() {

        int x1 = -3;
        int x2 = 4;
        int y1 = 5;
        int y2 = -7;

        double expected = 13.892;
        double result = BlocoDois.getDistanciaPlanos(x1, y1, x2, y2);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio4xZero() {
        int valorx = 0;

        int expected = 0;
        int result = BlocoDois.exercicio4(valorx);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio4xPositivo() {
        int valorx = 3;

        int expected = 3;
        int result = BlocoDois.exercicio4(valorx);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio4xNegativo() {
        int valorx = -5;

        int expected = -5;
        int result = BlocoDois.exercicio4(valorx);
        assertEquals(expected, result);
    }

    @Test
    //public void getVolume() {
    public void ensureVolumeForCubeWithAreaTwentySixIsNinePointZeroSeventeen() {
        //Arrange
        double expected = 9.017;
        double area = 26;
        //double expected = volume; eliminar linhas que causem entropia

        //Act
        double result = BlocoDois.getCubeVolume(area);

        //Assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getCubeSizetestepequeno() {
        double area = 2;
        String expected = "O cubo é Pequeno.";
        String result = BlocoDois.getCubeSize(area);
        assertEquals(expected, result);

    }

    @Test
    public void getCubeSizetestemedio() {
        double area = 7;
        String expected = "O cubo é Médio.";
        String result = BlocoDois.getCubeSize(area);
        assertEquals(expected, result);
    }

    @Test
    public void getCubeSizetestegrande() {
        double area = 10;
        String expected = "O cubo é Grande.";
        String result = BlocoDois.getCubeSize(area);
        assertEquals(expected, result);
    }

    @Test
    public void getSegundosLeft() {
        int segundos = 72001;
        int expected = 1;
        int result = BlocoDois.getSegundosLeft(segundos);
        assertEquals(expected, result);
    }

    @Test
    public void getMinutos() {
        int segundos = 50580;
        int expected = 3;
        int result = BlocoDois.getMinutos(segundos);
        assertEquals(expected, result);
    }

    @Test
    public void getHoras() {
        int segundos = 82800;
        int expected = 23;
        int result = BlocoDois.getHoras(segundos);
    }

    @Test
    public void exercicio7custototalteste() {
        float area = 230F;
        float custoLitro = 2f;
        double rendimentoLitro = 5f;
        double salarioPintores = 5;
        double expected = 127.937;
        double result = BlocoDois.exercicio7custototal(area, custoLitro, rendimentoLitro, salarioPintores);
        assertEquals(expected, result, 0.001);
    }

    @Test
    public void exercicio7custototalteste2() {
        float area = 1000F;
        float custoLitro = 4f;
        double rendimentoLitro = 7.25f;
        double salarioPintores = 20;
        double expected = 864.5;
        double result = BlocoDois.exercicio7custototal(area, custoLitro, rendimentoLitro, salarioPintores);
        assertEquals(expected, result, 0.001);
    }

    @Test
    public void exercicio7customaoobrateste1() {
        float area = 999f;
        double salarioPintores = 23.5;
        float expected = 489.093F;
        double result = BlocoDois.exercicio7customaoobra(area, salarioPintores);
        assertEquals(expected, result, 0.001);
    }

    @Test
    public void exercicio7customaoobrateste2() {
        float area = 34f;
        double salarioPintores = 76.33;
        double expected = 162.201;
        double result = BlocoDois.exercicio7customaoobra(area, salarioPintores);
        assertEquals(expected, result, 0.001);
    }

    @Test
    public void exercicio7custotintateste1() {
        float area = 20f;
        float custoLitro = 6f;
        float rendimentoLitro = 3f;


        double expected = 42;
        double result = BlocoDois.exercicio7custotinta(area, custoLitro, rendimentoLitro);
        assertEquals(expected, result, 0.001);

    }

    @Test
    public void exercicio7custotintateste2() {
        float area = 666f;
        float custoLitro = 4.20f;
        float rendimentoLitro = 69f;

        double expected = 41.999;
        double result = BlocoDois.exercicio7custotinta(area, custoLitro, rendimentoLitro);
        assertEquals(expected, result, 0.001);

    }

    @Test
    public void exercicio8XmultiplodeYteste1() {
        int x = 25;
        int y = 5;

        assertTrue(BlocoDois.exercicio8XmultiplodeY(x, y));
    }

    @Test
    public void exercicio8XmultiplodeYteste2() {
        int x = 25;
        int y = 6;
        assertFalse(BlocoDois.exercicio8XmultiplodeY(x, y));
    }

    @Test
    public void exercicio8ymultiploxteste1() {
        int x = 2;
        int y = 20;
        assertTrue(BlocoDois.exercicio8YmultiploX(x, y));
    }

    @Test
    public void exercicio8ymultiploxteste2() {
        int x = 3;
        int y = 35;
        assertFalse(BlocoDois.exercicio8YmultiploX(x, y));
    }

    @Test
    public void exercicio9testTrue() {
        int numero = 678;
        assertTrue(BlocoDois.exercicio9(numero));
    }

    @Test
    public void exercicio9testFalse() {
        int numero = 231;
        assertFalse(BlocoDois.exercicio9(numero));
    }

    @Test
    public void exercicio10teste1() {
        double preco = 50;
        double expected = 40;
        double result = BlocoDois.exercicio10(preco);
        assertEquals(expected, result, 0.001);
    }

    @Test
    public void exercicio10teste2() {
        double preco = 99;
        double expected = 69.3;
        double result = BlocoDois.exercicio10(preco);
        assertEquals(expected, result, 0.001);
    }

    @Test
    public void exercicio10teste3() {
        double preco = 101;
        double expected = 60.6;
        double result = BlocoDois.exercicio10(preco);
        assertEquals(expected, result, 0.001);
    }

    @Test
    public void exercicio10teste4() {
        double preco = 1254;
        double expected = 501.6;
        double result = BlocoDois.exercicio10(preco);
        assertEquals(expected, result, 0.001);
    }

    @Test
    public void getAprovadosTestInvalido0() {
        int aprovados = -1;
        double turmaMa = 0.2;
        double turmaFraca = 0.5;
        double turmaRazoavel = 0.7;
        double turmaBoa = 0.9;
        String expected = "Valor inválido.";
        String result = BlocoDois.getAprovados(aprovados, turmaMa, turmaFraca, turmaRazoavel, turmaBoa);
        assertEquals(expected, result);
    }

    @Test
    public void getAprovadosTestInvalido1() {
        float aprovados = 1.1f;
        double turmaMa = 0.2;
        double turmaFraca = 0.5;
        double turmaRazoavel = 0.7;
        double turmaBoa = 0.9;
        String expected = "Valor inválido.";
        String result = BlocoDois.getAprovados(aprovados, turmaMa, turmaFraca, turmaRazoavel, turmaBoa);
        assertEquals(expected, result);
    }

    @Test
    public void getAprovadosTestTurmaMa() {
        float aprovados = 0.1f;
        double turmaMa = 0.2;
        double turmaFraca = 0.5;
        double turmaRazoavel = 0.7;
        double turmaBoa = 0.9;
        String expected = "Turma Má.";
        String result = BlocoDois.getAprovados(aprovados, turmaMa, turmaFraca, turmaRazoavel, turmaBoa);
        assertEquals(expected, result);
    }

    @Test
    public void getAprovadosTestTurmaFraca() {
        float aprovados = 0.55f;
        double turmaMa = 0.2;
        double turmaFraca = 0.6;
        double turmaRazoavel = 0.7;
        double turmaBoa = 0.9;
        String expected = "Turma Fraca.";
        String result = BlocoDois.getAprovados(aprovados, turmaMa, turmaFraca, turmaRazoavel, turmaBoa);
        assertEquals(expected, result);
    }

    @Test
    public void getAprovadosTestTurmaRazoavel() {
        float aprovados = 0.7f;
        double turmaMa = 0.2;
        double turmaFraca = 0.5;
        double turmaRazoavel = 0.79;
        double turmaBoa = 0.9;
        String expected = "Turma Razoável.";
        String result = BlocoDois.getAprovados(aprovados, turmaMa, turmaFraca, turmaRazoavel, turmaBoa);
        assertEquals(expected, result);
    }

    @Test
    public void getAprovadosTestTurmaBoa() {
        float aprovados = 0.7f;
        double turmaMa = 0.2;
        double turmaFraca = 0.5;
        double turmaRazoavel = 0.6;
        double turmaBoa = 0.72;
        String expected = "Turma Boa.";
        String result = BlocoDois.getAprovados(0.7, 0.2, 0.5, 0.6, 0.72);
        assertEquals(expected, result);
    }

    @Test
    public void getAprovadosTestTurmaExcelente() {
        float aprovados = 0.8f;
        double turmaMa = 0.2;
        double turmaFraca = 0.5;
        double turmaRazoavel = 0.7;
        double turmaBoa = 0.78;
        String expected = "Turma Excelente.";
        String result = BlocoDois.getAprovados(aprovados, turmaMa, turmaFraca, turmaRazoavel, turmaBoa);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio12testeaceitavel() {
        double indicePoluicao = 0.3;
        String expected = "O índíce de poluição está aceitável.";
        String result = BlocoDois.exercicio12(indicePoluicao);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio12testegrupo1() {
        double indicePoluicao = 0.31;
        String expected = "Indíce de poluição médio! Indústrias de grupo 1 devem paralisar as atividades.";
        String result = BlocoDois.exercicio12(indicePoluicao);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio12testegrupo1e2() {
        double indicePoluicao = 0.5;
        String expected = "Indíce de poluição alto! Indústrias do grupo 1 e 2 devem paralisar as atividades.";
        String result = BlocoDois.exercicio12(indicePoluicao);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio12testegrupo1e2e3() {
        double indicePoluicao = 0.501;
        String expected = "Indíce de poluição elevado! Os 3 grupos devem paralisar as atividades.";
        String result = BlocoDois.exercicio12(indicePoluicao);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio13Tempo() {
        int grama = 23;
        int arvores = 7;
        int arbustos = 10;

        double expected = 4.19;
        double result = BlocoDois.exercicio13Tempo(grama, arvores, arbustos);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio13Preco() {
        int grama = 23;
        int arvores = 7;
        int arbustos = 10;
        double expected = 561.944;
        double result = BlocoDois.exercicio13Preco(grama, arvores, arbustos);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio14teste1() {
        float distancia1 = 1f;
        float distancia2 = 1.3f;
        float distancia3 = 2f;
        float distancia4 = 0.6f;
        float distancia5 = 1.5f;

        double expected = 2.059;
        double result = BlocoDois.exercicio14(distancia1, distancia2, distancia3, distancia4, distancia5);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio14teste2() {
        float distancia1 = 3f;
        float distancia2 = 1.3f;
        float distancia3 = 0.3f;
        float distancia4 = 2f;
        float distancia5 = 2.5f;

        double expected = 2.928;
        double result = BlocoDois.exercicio14(distancia1, distancia2, distancia3, distancia4, distancia5);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio15ladonegativo() {
        int lado1 = 2;
        int lado2 = 4;
        int lado3 = -2;

        String expected = "Este triângulo é impossível: Nenhum lado pode ser 0 ou negativo.";
        String result = BlocoDois.exercicio15(lado1, lado2, lado3);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio15ladoZero() {
        int lado1 = 2;
        int lado2 = 4;
        int lado3 = 0;

        String expected = "Este triângulo é impossível: Nenhum lado pode ser 0 ou negativo.";
        String result = BlocoDois.exercicio15(lado1, lado2, lado3);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio15ladoMaior() {
        int lado1 = 2;
        int lado2 = 2;
        int lado3 = 5;

        String expected = "Este triângulo é impossível: Nenhum lado pode ser maior que a soma dos outros dois.";
        String result = BlocoDois.exercicio15(lado1, lado2, lado3);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio15equilatero() {
        int lado1 = 2;
        int lado2 = 2;
        int lado3 = 2;

        String expected = "Este triângulo é Equilátero.";
        String result = BlocoDois.exercicio15(lado1, lado2, lado3);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio15isosceles() {
        int lado1 = 2;
        int lado2 = 2;
        int lado3 = 1;

        String expected = "Este triângulo é Isósceles.";
        String result = BlocoDois.exercicio15(lado1, lado2, lado3);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio15escaleno() {
        int lado1 = 2;
        int lado2 = 3;
        int lado3 = 1;
        String expected = "Este triângulo é Escaleno.";
        String result = BlocoDois.exercicio15(lado1, lado2, lado3);
        assertEquals(expected, result);
    }


    @Test
    public void exercicio16testezero() {
        int angulo1 = 0;
        int angulo2 = 100;
        int angulo3 = 80;

        String expected = "Este triângulo é impossível: Nenhum angulo pode ser 0 ou negativo.";
        String result = BlocoDois.exercicio16(angulo1, angulo2, angulo3);
        assertEquals(expected, result);

    }

    @Test
    public void exercicio16testenegativo() {
        int angulo1 = 12;
        int angulo2 = -100;
        int angulo3 = 80;
        String expected = "Este triângulo é impossível: Nenhum angulo pode ser 0 ou negativo.";
        String result = BlocoDois.exercicio16(angulo1, angulo2, angulo3);
        assertEquals(expected, result);

    }

    @Test
    public void exercicio16soma180errada() {
        int angulo1 = 50;
        int angulo2 = 50;
        int angulo3 = 81;

        String expected = "Este triângulo é impossível: A soma dos angulos tem de ser 180.";
        String result = BlocoDois.exercicio16(angulo1, angulo2, angulo3);
        assertEquals(expected, result);

    }

    @Test
    public void exercicio16retangulo() {
        int angulo1 = 90;
        int angulo2 = 40;
        int angulo3 = 50;

        String expected = "Este triângulo é Retângulo.";
        String result = BlocoDois.exercicio16(angulo1, angulo2, angulo3);
        assertEquals(expected, result);

    }

    @Test
    public void exercicio16acutangulo() {
        int angulo1 = 85;
        int angulo2 = 45;
        int angulo3 = 50;

        String expected = "Este triângulo é Acutângulo.";
        String result = BlocoDois.exercicio16(angulo1, angulo2, angulo3);
        assertEquals(expected, result);

    }

    @Test
    public void exercicio16obtuso() {
        int angulo1 = 100;
        int angulo2 = 40;
        int angulo3 = 40;

        String expected = "Este triângulo é Obtuso.";
        String result = BlocoDois.exercicio16(angulo1, angulo2, angulo3);
        assertEquals(expected, result);

    }

    @Test
    public void exercicio18teste1() {
        int horaInicio = 14;
        int minutoinicio = 32;
        int segundosInicio = 00;
        int tempoprocess = 123;

        String expected = "O processamento acabará às 14:34:03";
        String result = BlocoDois.exercicio18(horaInicio, minutoinicio, segundosInicio, tempoprocess);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio18teste0() {
        int horaInicio = 14;
        int minutoinicio = 32;
        int segundosInicio = 00;
        int tempoprocess = 0;

        String expected = "O processamento acabará às 14:32:00";
        String result = BlocoDois.exercicio18(horaInicio, minutoinicio, segundosInicio, tempoprocess);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio18teste1dia() {
        int horaInicio = 14;
        int minutoinicio = 32;
        int segundosInicio = 00;
        int tempoprocess = 86400;

        String expected = "O processamento acabará às 14:32:00";
        String result = BlocoDois.exercicio18(horaInicio, minutoinicio, segundosInicio, tempoprocess);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio17testepropriodia() {
        int horaPartida = 15;
        int minutoPartida = 30;
        int horasDuracao = 2;
        int minutosDuracao = 30;

        String expected = "O comboio chegará às 18:00 do próprio dia.";
        String result = BlocoDois.exercicio17(horaPartida, minutoPartida, horasDuracao, minutosDuracao);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio17testediaseguinte() {
        int horaPartida = 20;
        int minutoPartida = 00;
        int horasDuracao = 6;
        int minutosDuracao = 00;

        String expected = "O comboio chegará às 02:00 do dia seguinte.";
        String result = BlocoDois.exercicio17(horaPartida, minutoPartida, horasDuracao, minutosDuracao);
        assertEquals(expected, result);
    }

    @Test
    public void exercicio19horas36() {
        int horas = 36;

        double expected = 270;
        double result = BlocoDois.exercicio19(horas);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio19horas39() {
        int horas = 39;

        double expected = 300;
        double result = BlocoDois.exercicio19(horas);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio19horas44() {
        int horas = 44;

        double expected = 365;
        double result = BlocoDois.exercicio19(horas);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio20diautilA() {
        boolean diaUtil = true;
        char kit = 'a';
        int kmEntreha = 23;

        double expected = 76;
        double result = BlocoDois.exercicio20(diaUtil, kit, kmEntreha);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio20diautilb() {
        boolean diaUtil = true;
        char kit = 'B';
        int kmEntreha = 60;

        double expected = 170;
        double result = BlocoDois.exercicio20(diaUtil, kit, kmEntreha);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio20diautilc() {
        boolean diaUtil = true;
        char kit = 'C';
        int kmEntreha = 15;

        double expected = 130;
        double result = BlocoDois.exercicio20(diaUtil, kit, kmEntreha);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio20feriadoA() {
        boolean diaUtil = false;
        char kit = 'A';
        int kmEntreha = 12;

        double expected = 64;
        double result = BlocoDois.exercicio20(diaUtil, kit, kmEntreha);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio20feriadob() {
        boolean diaUtil = false;
        char kit = 'b';
        int kmEntrega = 5;

        double expected = 80;
        double result = BlocoDois.exercicio20(diaUtil, kit, kmEntrega);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void exercicio20feriadoc() {
        boolean diaUtil = false;
        char kit = 'c';
        int kmEntrega = 34;

        double expected = 208;
        double result = BlocoDois.exercicio20(diaUtil, kit, kmEntrega);
        assertEquals(expected, result, 0.01);
    }


}
