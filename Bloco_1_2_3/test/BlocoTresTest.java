

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class BlocoTresTest {

    @Test
    void exercicio1ProcessTest1() {
        int expected = 3628800;
        int num = 10;

        int result = BlocoTres.exercicio1Process(num);

        assertEquals(expected, result);
    }

    @Test
    void exercicio1ProcessTest2() {
        int expected = 24;
        int num = 4;

        int result = BlocoTres.exercicio1Process(num);

        assertEquals(expected, result);
    }

    @Test
    void exercicio2Test() {
        int alunos = 10;
        double[] notas = {12, 13, 4, 17, 13, 12, 5, 7, 2, 1};
        String expected = "Positivos 50 Negativos 3,8";
        String result = BlocoTres.exercicio2Logic(alunos, notas);
        assertEquals(expected, result);
    }

    @Test
    void exercicio2TestBoundaries() {
        int alunos = 10;
        double[] notas = {10, 9.9, 9, 10.001, 13, 12, 15, 9, 9, 9.1};
        String expected = "Positivos 50 Negativos 9,2";
        String result = BlocoTres.exercicio2Logic(alunos, notas);
        assertEquals(expected, result);
    }

    @Test
    void exercicio3test1() {
        float[] numeros = {8f, 2f, 2f, 3f, 1f, 2f, 5f, 4f, -3f, 2f, 3f};
        String expected = "Pares:62.5 Impares:3.0";
        String result = BlocoTres.exercicio3(numeros);
        assertEquals(expected, result);

    }

    @Test
    void exercicio3test2() {
        float[] numeros = {8f, 1f, 1f, 2f, 0f, 2f, 5f, 4f, 3f, 2f, 3f};
        String expected = "Pares:50.0 Impares:1.0";
        String result = BlocoTres.exercicio3(numeros);
        assertEquals(expected, result);

    }

    @Test
    void exercicio4Atest() {
        int numMin = 1;
        int numMax = 30;
        int expected = 10;
        int result = BlocoTres.exercicio4A(numMin, numMax);
        assertEquals(expected, result);
    }

    @Test
    void exercicio4AtestZero() {
        int numMin = 31;
        int numMax = 32;
        int expected = 0;
        int result = BlocoTres.exercicio4A(numMin, numMax);
        assertEquals(expected, result);
    }

    @Test
    void exercicio4BtestZero() {
        int numMin = 31;
        int numMax = 35;
        int num = 6;
        int expected = 0;
        int result = BlocoTres.exercicio4B(numMin, numMax, num);
        assertEquals(expected, result);
    }

    @Test
    void exercicio4BtestThree() {
        int numMin = 3;
        int numMax = 10;
        int num = 3;
        int expected = 3;
        int result = BlocoTres.exercicio4B(numMin, numMax, num);
        assertEquals(expected, result);
    }

    @Test
    void exercicio4CtestZero() {
        int numMin = 20;
        int numMax = 29;
        int expected = 0;
        int result = BlocoTres.exercicio4C(numMin, numMax);
        assertEquals(expected, result);
    }


    @Test
    void exercicio4CtestTwoMultiplesOfFiveAndThree() {
        int numMin = 1;
        int numMax = 30;
        int expected = 2;
        int result = BlocoTres.exercicio4C(numMin, numMax);
        assertEquals(expected, result);
    }

    @Test
    void exercicio4DtestZero() {
        int num1 = 4;
        int num2 = 6;
        int numMin = 1;
        int numMax = 10;
        int expected = 0;
        int result = BlocoTres.exercicio4D(num1, num2, numMin, numMax);
        assertEquals(expected, result);
    }

    @Test
    void exercicio4DtestOneMultipleofFourandSix() {
        int num1 = 4;
        int num2 = 6;
        int numMin = 1;
        int numMax = 12;
        int expected = 1;
        int result = BlocoTres.exercicio4D(num1, num2, numMin, numMax);
        assertEquals(expected, result);
    }

    @Test
    void exercicio4EZero() {
        int num1 = 4;
        int num2 = 6;
        int numMin = 1;
        int numMax = 11;
        int expected = 0;
        int result = BlocoTres.exercicio4E(num1, num2, numMin, numMax);
        assertEquals(expected, result);
    }

    @Test
    void exercicio4EThirtySix() {
        int num1 = 4;
        int num2 = 6;
        int numMin = 1;
        int numMax = 24;
        int expected = 36;
        int result = BlocoTres.exercicio4E(num1, num2, numMin, numMax);
        assertEquals(expected, result);
    }

    @Test
    void exercicio5AThirty() {
        int numMin = 0;
        int numMax = 10;
        int expected = 30;
        int result = BlocoTres.exercicio5A(numMin, numMax);
        assertEquals(expected, result);
    }

    @Test
    void exercicio5ASeventyTwo() {
        int numMin = 0;
        int numMax = 17;
        int expected = 72;
        int result = BlocoTres.exercicio5A(numMin, numMax);
        assertEquals(expected, result);
    }

    @Test
    void exercicio5BFive() {
        int numMin = 1;
        int numMax = 10;
        int expected = 5;
        int result = BlocoTres.exercicio5B(numMin, numMax);
        assertEquals(expected, result);
    }

    @Test
    void exercicio5BTen() {
        int numMin = 1;
        int numMax = 20;
        int expected = 10;
        int result = BlocoTres.exercicio5B(numMin, numMax);
        assertEquals(expected, result);
    }

    @Test
    void exercicio5CThirtySix() {
        int numMin = 0;
        int numMax = 12;
        int expected = 36;
        int result = BlocoTres.exercicio5C(numMin, numMax);
        assertEquals(expected, result);
    }

    @Test
    void exercicio5CTwentyFive() {
        int numMin = 0;
        int numMax = 10;
        int expected = 25;
        int result = BlocoTres.exercicio5C(numMin, numMax);
        assertEquals(expected, result);
    }

    @Test
    void exercicio5DFive() {
        int numMin = 0;
        int numMax = 10;
        int expected = 5;
        int result = BlocoTres.exercicio5D(numMin, numMax);
        assertEquals(expected, result);
    }

    @Test
    void exercicio5DSeven() {
        int numMin = 0;
        int numMax = 14;
        int expected = 7;
        int result = BlocoTres.exercicio5D(numMin, numMax);
        assertEquals(expected, result);
    }

    @Test
    void exercicio5ECrescente() {
        int num1 = 1;
        int num2 = 9;
        int numMultiplo = 3;
        int expected = 18;
        int result = BlocoTres.exercicio5E(num1, num2, numMultiplo);
        assertEquals(expected, result);
    }

    @Test
    void exercicio5EDecrescente() {
        int num1 = 9;
        int num2 = 1;
        int numMultiplo = 3;
        int expected = 18;
        int result = BlocoTres.exercicio5E(num1, num2, numMultiplo);
        assertEquals(expected, result);
    }

    @Test
    void exercicio5EIgual() {
        int num1 = 9;
        int num2 = 9;
        int numMultiplo = 3;
        int expected = 0;
        int result = BlocoTres.exercicio5E(num1, num2, numMultiplo);
        assertEquals(expected, result);
    }

    @Test
    void exercicio5FHundredSixtyTwo() {
        int num1 = 1;
        int num2 = 10;
        int numMultiplo = 3;
        int expected = 162;
        int result = BlocoTres.exercicio5F(num1, num2, numMultiplo);
        assertEquals(expected, result);
    }

    @Test
    void exercicio5FZero() {
        int num1 = 0;
        int num2 = 10;
        int numMultiplo = 3;
        int expected = 0;
        int result = BlocoTres.exercicio5F(num1, num2, numMultiplo);
        assertEquals(expected, result);
    }

    @Test
    void exercicio5GTestSix() {
        float num1 = 1f;
        float num2 = 10f;
        float numMultiplo = 3f;
        float expected = 6f;
        float result = BlocoTres.exercicio5G(num1, num2, numMultiplo);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio5GTestFifteen() {
        float num1 = 20f;
        float num2 = 10f;
        float numMultiplo = 3f;
        float expected = 15f;
        float result = BlocoTres.exercicio5G(num1, num2, numMultiplo);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio5HSixPointSix() {
        float num1 = 10f;
        float num2 = 1f;
        float numMultiplo1 = 3f;
        float numMultiplo2 = 5f;
        float expected = 6.60f;
        float result = BlocoTres.exercicio5H(num1, num2, numMultiplo1, numMultiplo2);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio5HTwelvePointThiryThree() {
        float num1 = 10f;
        float num2 = 15f;
        float numMultiplo1 = 3f;
        float numMultiplo2 = 5f;
        float expected = 12.33f;
        float result = BlocoTres.exercicio5H(num1, num2, numMultiplo1, numMultiplo2);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio6AtestFive() {
        int num = 27738;
        int expected = 5;
        int result = BlocoTres.ex6NumeroDigitos(num);
        assertEquals(expected, result);
    }

    @Test
    void exercicio6AtestThree() {
        int num = 277;
        int expected = 3;
        int result = BlocoTres.ex6NumeroDigitos(num);
        assertEquals(expected, result);
    }

    @Test
    void exercicio6BtestThree() {
        int num = 24563;
        double expected = 3;
        double result = BlocoTres.ex6NumeroDePares(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio6BtestZero() {
        int num = 3579;
        double expected = 0;
        double result = BlocoTres.ex6NumeroDePares(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio6BtestFive() {
        int num = 24680;
        double expected = 5;
        double result = BlocoTres.ex6NumeroDePares(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio6CtestFive() {
        int num = 13579;
        double expected = 5;
        double result = BlocoTres.ex6NumeroDeImpares(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio6CtestZero() {
        int num = 24680;
        double expected = 0;
        double result = BlocoTres.ex6NumeroDeImpares(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio6CtestThree() {
        int num = 13470;
        double expected = 3;
        double result = BlocoTres.ex6NumeroDeImpares(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio6DtestFifteen() {
        int num = 13470;
        double expected = 15;
        double result = BlocoTres.ex6SomaDigitos(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio6DtestFour() {
        int num = 11011;
        double expected = 4;
        double result = BlocoTres.ex6SomaDigitos(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio6EtestZero() {
        int num = 11011;
        double expected = 0;
        double result = BlocoTres.ex6SomaDePares(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio6EtestFourteen() {
        int num = 27438;
        double expected = 14;
        double result = BlocoTres.ex6SomaDePares(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio6FtestFourteen() {
        int num = 11111;
        double expected = 5;
        double result = BlocoTres.ex6SomaDeImpares(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio6FtestTen() {
        int num = 87423;
        double expected = 10;
        double result = BlocoTres.ex6SomaDeImpares(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ex6MediaDigitosFivePointTwentyFive() {
        int num = 7734;
        double expected = 5.25;
        double result = BlocoTres.ex6MediaDigitos(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ex6MediaDigitosFivePointTwo() {
        int num = 87326;
        double expected = 5.2;
        double result = BlocoTres.ex6MediaDigitos(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ex6MediaParesThree() {
        int num = 77342;
        double expected = 3;
        double result = BlocoTres.ex6MediaPares(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ex6MediaParesFour() {
        int num = 54623;
        double expected = 4;
        double result = BlocoTres.ex6MediaPares(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ex6MediaImparesFivePointSixtySix() {
        int num = 77342;
        double expected = 5.66;
        double result = BlocoTres.ex6MediaImpares(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ex6MediaImparesTwo() {
        int num = 4405265;
        double expected = 5;
        double result = BlocoTres.ex6MediaImpares(num);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ex6InverterNumero54321() {
        int num = 12345;
        int expected = 54321;
        int result = BlocoTres.ex6InverterNumero(num);
        assertEquals(expected, result);
    }

    @Test
    void ex6InverterNumero13267() {
        int num = 76231;
        int expected = 13267;
        int result = BlocoTres.ex6InverterNumero(num);
        assertEquals(expected, result);
    }

    @Test
    void ex7CapicuaTrue() {
        int num = 1331;
        assertTrue(BlocoTres.ex7ACapicua(num));
    }

    @Test
    void ex7CapicuaFalse() {
        int num = 133241;
        assertFalse(BlocoTres.ex7ACapicua(num));
    }

    @Test
    void ex7BAmstrongNumberFalse() {
        int num = 333;
        assertFalse(BlocoTres.ex7BArmstrongNumber(num));
    }

    @Test
    void ex7BAmstrongNumberTrue() {
        int num = 370;
        assertTrue(BlocoTres.ex7BArmstrongNumber(num));
    }

    @Test
    void ex7CPrimeiraCapicua101() {
        int num1 = 100;
        int num2 = 120;
        int expected = 101;
        int result = BlocoTres.ex7CPrimeiraCapicua(num1, num2);
        assertEquals(expected, result);
    }

    @Test
    void ex7CPrimeiraCapicua1() {
        int num1 = 1;
        int num2 = 120;
        int expected = 1;
        int result = BlocoTres.ex7CPrimeiraCapicua(num1, num2);
        assertEquals(expected, result);
    }

    @Test
    void ex7DmaiorCapicua111() {
        int num1 = 100;
        int num2 = 120;
        int expected = 111;
        int result = BlocoTres.ex7DMaiorCapicua(num1, num2);
        assertEquals(expected, result);
    }

    @Test
    void ex7DmaiorCapicua121() {
        int num1 = 1;
        int num2 = 123;
        int expected = 121;
        int result = BlocoTres.ex7DMaiorCapicua(num1, num2);
        assertEquals(expected, result);
    }

    @Test
    void ex7ENumeroCapicuas9() {
        int num1 = 1;
        int num2 = 10;
        int expected = 9;
        int result = BlocoTres.ex7ENumeroCapicuas(num1, num2);
        assertEquals(expected, result);
    }

    @Test
    void ex7ENumeroCapicuas10() {
        int num1 = 1;
        int num2 = 12;
        int expected = 10;
        int result = BlocoTres.ex7ENumeroCapicuas(num1, num2);
        assertEquals(expected, result);
    }

    @Test
    void ex7FPrimeiroArmstrong153() {
        int num1 = 150;
        int num2 = 371;
        int expected = 153;
        int result = BlocoTres.ex7FPrimeiroArmstrong(num1, num2);
        assertEquals(expected, result);
    }

    @Test
    void ex7FPrimeiroArmstrong370() {
        int num1 = 369;
        int num2 = 372;
        int expected = 370;
        int result = BlocoTres.ex7FPrimeiroArmstrong(num1, num2);
        assertEquals(expected, result);
    }

    @Test
    void ex7GNumeroArmstrong1() {
        int num1 = 150;
        int num2 = 155;
        int expected = 1;
        int result = BlocoTres.ex7GNumeroArmstrong(num1, num2);
        assertEquals(expected, result);
    }

    @Test
    void ex7GNumeroArmstrong2() {
        int num1 = 369;
        int num2 = 373;
        int expected = 2;
        int result = BlocoTres.ex7GNumeroArmstrong(num1, num2);
        assertEquals(expected, result);
    }

    @Test
    void exercicio8teste1() {
        int[] set = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int alvo = 15;
        int expected = 1;
        int result = BlocoTres.exercicio8(set, alvo);
        assertEquals(expected, result);
    }

    @Test
    void exercicio8teste2() {
        int[] set = {23, 45, 12, 33, 16, 11, 245};
        int alvo = 100;
        int expected = 12;
        int result = BlocoTres.exercicio8(set, alvo);
        assertEquals(expected, result);
    }

    @Test
    void exercicio9teste1() {
        double[] horasExtra = {4, 5, 2, 2, 3, -1, 10000};
        double[] salarioBase = {1223, 211, 2344, 2134, 1234};
        String expected = "A média dos salários pagos é " + 1503.62;
        String result = BlocoTres.exercicio9(horasExtra, salarioBase);
        assertEquals(expected, result);
    }

    @Test
    void exercicio9teste2() {
        double[] horasExtra = {10, 5, -1, 2, 3, -1, 10000};
        double[] salarioBase = {600, 400};
        String expected = "A média dos salários pagos é " + 580.00;
        String result = BlocoTres.exercicio9(horasExtra, salarioBase);
        assertEquals(expected, result);
    }

    @Test
    void exercicio10teste1() {
        int numeroLimite = 200;
        int[] numeros = {23, 11, 12, 24, 1223};
        int expected = 23;
        int result = BlocoTres.exercicio10(numeroLimite, numeros);
        assertEquals(expected, result);

    }

    @Test
    void exercicio10teste2() {
        int numeroLimite = 72863;
        int[] numeros = {23, 11, 12, 24, 1223};
        int expected = 24;
        int result = BlocoTres.exercicio10(numeroLimite, numeros);
        assertEquals(expected, result);

    }

    @Test
    void ex11altDoze() {
        int numero = 12;
        String expected = "O numero 12 tem 5 combinações possíveis: 2+10;3+9;4+8;5+7;6+6;";
        String result = BlocoTres.ex11VerificarCombinacoesSoma1a10(numero);
        assertEquals(expected, result);
    }

    @Test
    void ex11altDez() {
        int numero = 10;
        String expected = "O numero 10 tem 6 combinações possíveis: 0+10;1+9;2+8;3+7;4+6;5+5;";
        String result = BlocoTres.ex11VerificarCombinacoesSoma1a10(numero);
        assertEquals(expected, result);
    }

    @Test
    void exercicio12testeRaizImaginaria() {
        float a = 1;
        float b = 2;
        float c = 3;

        List<Double> result = BlocoTres.exercicio12(a, b, c);
        assertTrue(result.isEmpty());
    }

    @Test
    void exercicio12testeDuasRaizes() {
        float a = 1;
        float b = 2;
        float c = -15;

        List<Double> expected = new ArrayList<>();
        expected.add(3.0);
        expected.add(-5.0);
        Collections.sort(expected);
        List<Double> result = BlocoTres.exercicio12(a, b, c);
        assertEquals(result, expected);
    }

    @Test
    void exercicio12testeRaizUnica() {
        float a = -3;
        float b = -24;
        float c = -48;
        //boolean test = false;
        List<Double> expected = new ArrayList<>();
        expected.add(-4.0);

        Collections.sort(expected);
        List<Double> result = BlocoTres.exercicio12(a, b, c);
        assertEquals(result, expected);
    }

    @Test
    void exercicio14testeinvalido() {
        char moeda = 'k';
        float valor = 12f;
        String expected = "12.0€ equivale a 0.0 =Moeda Introduzida Inválida!=";
        String result = BlocoTres.ex14process(moeda, valor);
        assertEquals(expected, result);

    }

    @Test
    void exercicio14testeIenes() {
        char moeda = 'I';
        float valor = 12f;
        String expected = "12.0€ equivale a 1937.76 Ienes";
        String result = BlocoTres.ex14process(moeda, valor);
        assertEquals(expected, result);

    }

    @Test
    void exercicio14testeCoroas() {
        char moeda = 'c';
        float valor = 15.30f;
        String expected = "15.3€ equivale a 146.7729 Coroas";
        String result = BlocoTres.ex14process(moeda, valor);
        assertEquals(expected, result);

    }


    @Test
    void ex15processZeroMau() {
        int nota = 0;
        String expected = "Mau";
        String result = BlocoTres.ex15process(nota);
        assertEquals(expected, result);
    }

    @Test
    void ex15processVinteUmInvalido() {
        int nota = 21;
        String expected = "Nota inválida. Insira notas entre 0 e 20. Nota negativa termina o programa.";
        String result = BlocoTres.ex15process(nota);
        assertEquals(expected, result);
    }

    @Test
    void ex15processCincoMediocre() {
        int nota = 5;
        String expected = "Medíocre";
        String result = BlocoTres.ex15process(nota);
        assertEquals(expected, result);
    }

    @Test
    void ex15processTrezeSuficiente() {
        int nota = 13;
        String expected = "Suficiente";
        String result = BlocoTres.ex15process(nota);
        assertEquals(expected, result);
    }

    @Test
    void ex15processCatorzeBom() {
        int nota = 14;
        String expected = "Bom";
        String result = BlocoTres.ex15process(nota);
        assertEquals(expected, result);
    }

    @Test
    void ex15processVinteMuitoBom() {
        int nota = 20;
        String expected = "Muito Bom";
        String result = BlocoTres.ex15process(nota);
        assertEquals(expected, result);
    }

    @Test
    void ex15processMenosUmNull() {
        int nota = -1;
        String expected = null;
        String result = BlocoTres.ex15process(nota);
        assertEquals(expected, result);
    }

    @Test
    void ex16_449() {
        float salarioBruto = 499f;
        float expected = 449.1f;
        float result = BlocoTres.ex16(salarioBruto);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ex16_501() {
        float salarioBruto = 501f;
        float expected = 425.85f;
        float result = BlocoTres.ex16(salarioBruto);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ex16_1000() {
        float salarioBruto = 1000f;
        float expected = 800f;
        float result = BlocoTres.ex16(salarioBruto);
        assertEquals(expected, result, 0.01);
    }

    @Test
    void ex17_100() {
        int[] peso = {10};
        int[] racao = {100};
        String expected = "A quantidade de ração é adequada.";
        String result = BlocoTres.ex17(peso, racao);
        assertEquals(expected, result);
    }

    @Test
    void ex17_26_100() {
        int[] peso = {26};
        int[] racao = {100};
        String expected = "A quantidade é desadequada! Um cão grande deve comer 300g/dia!";
        String result = BlocoTres.ex17(peso, racao);
        assertEquals(expected, result);

    }

    @Test
    void ex18_numero7digitos() {
        int numCC = 1234567;
        int numeroVerificacao = 2;
        boolean expected = false;
        boolean result = BlocoTres.ex18(numCC, numeroVerificacao);
        assertEquals(expected, result);
    }

    @Test
    void ex18_numeroVerificacaoErrado() {
        int numCC = 12345678;
        int numeroVerificacao = 22;
        boolean expected = false;
        boolean result = BlocoTres.ex18(numCC, numeroVerificacao);
        assertEquals(expected, result);
    }

    @Test
    void ex18_numeroInvalido() {
        int numCC = 12345678;
        int numeroVerificacao = 2;
        boolean expected = false;
        boolean result = BlocoTres.ex18(numCC, numeroVerificacao);
        assertEquals(expected, result);
    }

    @Test
    void ex18_numeroValido() {
        int numCC = 12345678;
        int numeroVerificacao = 5;
        boolean expected = true;
        boolean result = BlocoTres.ex18(numCC, numeroVerificacao);
        assertEquals(expected, result);
    }

    @Test
    void ex19_1234567890() {
        int numero = 1234567890;
        int expected = 1357924680;
        int result = BlocoTres.ex19(numero);
        assertEquals(expected, result);
    }

    @Test
    void ex19_82374458() {
        int numero = 82374458;
        int expected = 37582448;
        int result = BlocoTres.ex19(numero);
        assertEquals(expected, result);
    }

    @Test
    void ex20SeisPerfeito() {
        int numero = 6;
        String expected = "6 é um número Perfeito.";
        String result = BlocoTres.ex20(numero);
        assertEquals(expected, result);
    }

    @Test
    void ex20DozeAbundante() {
        int numero = 12;
        String expected = "12 é um número Abundante.";
        String result = BlocoTres.ex20(numero);
        assertEquals(expected, result);
    }

    @Test
    void ex20NoveReduzido() {
        int numero = 9;
        String expected = "9 é um número Reduzido.";
        String result = BlocoTres.ex20(numero);
        assertEquals(expected, result);
    }


}
