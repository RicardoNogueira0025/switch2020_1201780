package com.company;

public class Student {
    private double grade = -1;
    private String name;
    private int number;


    public Student(int number, String name) {
        setName(name);
        setNumber(number);

    }

    public int compareByNumber(Student student) {
        return this.number - student.number;
    }

    public void setNumber(int number) {
        if (!isNumberValid(number)) {
            throw new IllegalArgumentException("O número não pode ser negativo");
        }
        this.number = number;
    }

    private boolean isNumberValid(int number) {
        if (Math.abs(number) != number) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isNameValid(String name) {
        if (name == null || name.length() < 5) {
            return false;
        }
        return true;
    }

    public boolean isApproved() {
        return (this.grade >= 10);
    }

    private boolean isGradeValid(double grade) {
        if (grade < 0.0 || grade > 20.0) {
            return false;
        }
        return true;
    }

    public double getGrade() {
        return grade;
    }

    public void doEvaluation(double grade) {
        if (isGradeValid(grade)) {
            this.grade = grade;
        } else {
            throw new IllegalArgumentException("A nota tem de ser entre 0 e 20.");
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (isNameValid(name)) {
            this.name = name;
        } else {
            throw new IllegalArgumentException("Nome não pode ter menos de 5 caracteres.");
        }
    }
}
