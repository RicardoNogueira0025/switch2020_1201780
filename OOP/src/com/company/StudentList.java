package com.company;

import java.util.ArrayList;

public class StudentList extends ArrayList {

    private ArrayList<Student> studentList = new ArrayList();

    public StudentList(Student student) {
        studentList.add(student);
    }
    public StudentList(){}
    public StudentList(Student[] studentArray) {
        for (Student student : studentArray) {
            this.studentList.add(student);
        }
    }

    public Student[] orderStudentsByNumber() {
        Student tempStudent = new Student(222, "Temporary");
        for (int i = 0; i < this.studentList.size(); i++) {
            for (int j = i + 1; j < this.studentList.size(); j++) {
                if (this.studentList.get(i).compareByNumber(studentList.get(j)) > 0) {
                    tempStudent = this.studentList.get(i);
                    this.studentList.set(i, this.studentList.get(j));
                    this.studentList.set(j, tempStudent);

                }

            }

        }
        return this.studentList.toArray(new Student[studentList.size()]);
    }

}
