package com.company;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {

    @Test
    void setGrade_Valid0Grade() {
        Student s1 = new Student(1234, "Alberto");
        double grade = 0;
        double expected = 0;
        s1.doEvaluation(grade);
        assertEquals(expected, s1.getGrade());
    }

    @Test
    void setGrade_Valid20Grade() {
        Student s1 = new Student(1234, "Alberto");
        double grade = 20;
        double expected = 20;
        s1.doEvaluation(grade);
        assertEquals(expected, s1.getGrade());
    }

    @Test
    void setGrade_InValidMinus1Grade() {
        Student s1 = new Student(1234, "Alberto");
        double grade = -1;
        assertThrows(IllegalArgumentException.class, () -> {
            s1.doEvaluation(grade);
        });


    }

    @Test
    void setGrade_InValid21Grade() {
        Student s1 = new Student(1234, "Alberto");
        double grade = 21;
        assertThrows(IllegalArgumentException.class, () -> {
            s1.doEvaluation(grade);
        });


    }

    @Test
    void setNameValidName() {
        String name = "Ricardo";
        Student s1 = new Student(12345, name);
        assertEquals(name, s1.getName());

    }

    @Test
    void setNameInvalidName() {
        String name = "Rui";
        assertThrows(IllegalArgumentException.class, () -> {
            Student s1 = new Student(12345, name);
        });

    }
    @Test
    void setNameInvalidNameNull() {
        String name = null;
        assertThrows(IllegalArgumentException.class, () -> {
            Student s1 = new Student(12345, name);
        });

    }

    @Test
    void isApproved_Approved() {
        Student s1 = new Student(1234, "Alberto");
        double grade = 18;
        boolean expected = true;
        s1.doEvaluation(grade);
        assertTrue(s1.isApproved());
    }

    @Test
    void isApproved_NotApproved() {
        Student s1 = new Student(1234, "Alberto");
        double grade = 9;
        boolean expected = true;
        s1.doEvaluation(grade);
        assertFalse(s1.isApproved());
    }


    @Test
    void setNumberValidNumber() {
        int number = 1233;
        Student s1 = new Student(1111, "Ricardo");
        Student s2 = new Student(number, "Diana F");
        s1.setNumber(number);
        assertEquals(0,s1.compareByNumber(s2));

    }

    @Test
    void setNumberInValidNumber() {
        int number = -1233;
        assertThrows(IllegalArgumentException.class, () -> {
            Student s1 = new Student(number, "Ricardo");
        });

    }
}