package com.company;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class StudentListTest {
    @Test
    public void sortStudentsByNumberAscWithSeveralElementsIncorrectlyOrdered() {

        Student student1 = new Student(1200145, "Sampaio");
        Student student2 = new Student(1200054, "Moreira");
        Student student3 = new Student(1200086, "Silva");
        Student[] students = {student1, student2, student3};
        StudentList studentList = new StudentList(students);
        Student[] expected = {student2, student3, student1};
        students = studentList.orderStudentsByNumber();

        Assert.assertArrayEquals(expected, students);
    }

    @Test
    public void sortStudentsByNumberAscWithJustTwoElementsIncorrectlyOrdered() {

        Student student1 = new Student(1200145, "Sampaio");
        Student student2 = new Student(1200054, "Moreira");
        //Student student3 = new Student(1200086, "Silva");
        Student[] students = {student1, student2};
        StudentList studentList = new StudentList(students);
        Student[] expected = {student2, student1};
        students = studentList.orderStudentsByNumber();

        Assert.assertArrayEquals(expected, students);
    }

    @Test
    public void sortStudentsByNumberAscWithThreeElementsCorrectlyOrdered() {

        Student student1 = new Student(1200145, "Sampaio");
        Student student2 = new Student(1200054, "Moreira");
        Student student3 = new Student(1200086, "Silva");
        Student[] students = {student2, student3, student1};
        StudentList studentList = new StudentList(students);
        Student[] expected = {student2, student3, student1};
        students = studentList.orderStudentsByNumber();

        Assert.assertArrayEquals(expected, students);
    }

    @Test
    void AddStudentExtendTest() {
        Student student1 = new Student(1200145, "Sampaio");
        StudentList studentList = new StudentList();
        studentList.add(student1);
        Assert.assertEquals(student1, studentList.get(0));


    }
}